\documentclass[11pt,a4paper]{article}

\usepackage[letterpaper, margin=1.25in]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{natbib}
\usepackage{times}
\usepackage{titling}
\usepackage[protrusion=true,expansion=true]{microtype} % Better typography
\usepackage{bm}
\usepackage[dvipsnames]{xcolor}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage[labelfont=bf,skip=5pt]{caption}
\usepackage{subcaption}
\usepackage[mathscr]{euscript}
\usepackage{wrapfig}
\usepackage{hyperref}

\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{aqua}{HTML}{4A7D7E}
\definecolor{purple}{HTML}{75549C} %{46325E}

\hypersetup{
  linkcolor  = purple,  % internal links
  citecolor  = purple,  % internal links
  urlcolor   = aqua,  % external links
  colorlinks = true,
}



\newcommand{\ensuretext}[1]{#1}
\newcommand{\marker}[2]{\ensuremath{^{\textsc{#1}}_{\textsc{#2}}}}
\newcommand{\arkcomment}[3]{\ensuretext{\textcolor{#3}{[#1 #2]}}}
\renewcommand{\arkcomment}[3]{}
\newcommand{\sam}[1]{\arkcomment{\marker{S}{T}}{#1}{orange}}
\newcommand{\nas}[1]{\arkcomment{\marker{N}{S}}{#1}{blue}}

\newcommand{\NEWCS}{NEWCS}

\newcommand{\interalia}[1]{\citep[\textit{e.g.}][]{#1}}
\newcommand{\term}[1]{\textbf{#1}} % term being defined
\newcommand{\seq}[1]{\bm{#1}}
\newcommand{\tensor}[1]{\mathbf{#1}}
\newcommand{\argmax}[1]{\underset{#1}{\operatorname{arg}\,\operatorname{max}}\;}
\DeclareMathOperator{\maxmul}{maxmul}
\newcommand{\R}{\mathbb{R}}

\pretitle{\begin{flushright} \LARGE \sffamily \vspace{-1.25in}}
\title{
  \textbf{Encoding and Decoding Graph Representations of Natural Language}
}
\posttitle{\\ \end{flushright}}

\preauthor{\begin{flushright} \small \sffamily \vspace{0.1cm}}
\author{
  Sam Thomson \\
  Language Technologies Institute \\
  School of Computer Science \\
  Carnegie Mellon University \\
  \url{sthomson@cs.cmu.edu} \\
}
\postauthor{
  \end{flushright}
  \vspace{-1.5cm}
}
\date{}

\begin{document}


\maketitle

\begin{wrapfigure}{R}{0.5\textwidth} %[.5\textwidth]
  \centering
  \includegraphics[width=.48\textwidth]{fig/formalisms/multigraph.pdf}
%   \includegraphics[width=.48\textwidth]{fig/formalisms/pas.pdf}
%   \includegraphics[width=.48\textwidth]{fig/formalisms/psd.pdf}
  \caption{
    An example sentence annotated with semantic frames and roles from \term{FrameNet} \citep[][\textit{top/purple}]{baker_98},  semantic dependencies from \term{DM} \citep[][\textit{middle/red}]{oepen2014sdp} and syntax from \term{Universal Dependencies} \citep[][\textit{bottom/green}]{silveira14gold}.
    Nodes corresponding to token spans have solid blue fill, and nodes added by a formalism are hollow.
    Token nodes are shown both above and below the sentence for clarity.
    Gray arcs labeled ``+'' are added between adjacent token nodes to encode the linear order of the sentence (as graphs are considered to be unordered).
  }
  \label{fig:sdp-formalisms}
%   \sam{TODO:
%     Better example (can be a toy) that doesn't require prior knowledge of the formalism.
%     Something that a 1st year grad student could grok, and see why it needs to be a graph, and why it's useful.
%   }
\end{wrapfigure}


% \nas{make bold claim that the graph-based approaches are best (most natural? flexible? interpretable?).}
% We study the direct, \term{graph-based} approach to semantic parsing, wherein we learn a statistical model that scores all possible semantic graphs $y$ given a sentence $\seq{x}$, and we predict the graph with the highest score, $\argmax{y \in \mathscr{Y}}{\text{score}(y, \seq{x})}$.
% In general, there are an exponential number of possible graphs, so finding this argmax (i.e. \term{decoding}) can be difficult.
% Moreover, in structured learning decoding is done inside the inner loop of training, which makes computational efficiency an even more important consideration.
% Decoding algorithms are well studied for syntactic trees but less so for semantic graphs.
% This thesis will contribute to the theoretical and empirical understanding of algorithms for decoding semantic graphs.
% \nas{
% % Generally this seems to be on track.
% Suggest that you switch to a single column format and basically ``yes'' to all your notes-to-self.
% Pictures of the graphs you intend to decode into, from different formalisms and levels of representation (semantics, discourse, whatever) will go a long way, both in this document and the longer proposal.
% }


% \nas{
% When you make the case in the intro, I suggest that you distinguish (in separate paragraphs?) scientific arguments about representation (e.g., from linguistics) from application-oriented arguments (talking about symbolic manipulations, NLG, etc.).
% }

% \nas{
% ``relate the problem'' -- seems weak.  Do you reduce the problem to the other problem?
% } \st{fixed.}
% \nas{
% Generally, tweak language around citations to your own papers so that it's obvious to the reader that it is *your* work you are talking about.  E.g.,
% ``First, we show that ILP formulation used'' -- change to ``First, we show that ILP formulation *we* used''
% }


\paragraph{Introduction.}

When using natural language text as input to a machine learned system, it is often helpful as an intermediate step to process the original (linearly structured) text into richer structures that represent aspects of its linguistic content.
Multi-word expressions, coreference, syntax, semantics, discourse, and many more types of structured analyses have been found useful in applications such as summarization, question answering, machine translation, natural language inference, etc. % \sam{cite}
Each of these linguistic structures is either annotated directly as a labeled directed graph \interalia{copestake_minimal_2005,hajic2012psd,banarescu_abstract_2013,silveira14gold}, or is easily convertible to one \interalia{baker2007framenet,surdeanu2008conll}.
Non-binary relations, for example, can be converted to binary relations by the introduction of new nodes, as in neo-Davidsonian semantics \citep{parsons_events_1990}.
The nodes in the graph are usually spans from the sentence labeled with the concept, entity, or event they refer to, and arcs between nodes are labeled with a linguistic relationship that holds between the nodes.
See Figure~\ref{fig:sdp-formalisms} for an example sentence with several kinds of annotations.

% Various formalisms have been used to represent natural language semantics, including lambda calculus logical forms \sam{cite}, forests of shallow tree stumps (as in semantic role labeling \sam{cite}), trees (\sam{as in Percy Liang stuff}), and labeled directed graphs (as in AMR, SDP \sam{cite}. see Figure~\ref{fig:sdp-formalisms}).
% Of these, labeled directed graphs are the most flexible --- other formalisms being either a special case of a graph, or convertible to a graph without loss of information.%

This thesis focuses on such graphical representations of text.
We develop novel algorithms and models for
\emph{decoding}---automatically extracting graphs from raw text; and
\emph{encoding}---reducing arbitrarily sized graphs to fixed-size tensors so that they can be conveniently used as input to a downstream system.
We primarily focus on \emph{semantic} graphs---graphs that represent sentence meanings.
Semantic graphs are generally not trees, so they require methods that can handle arbitrary graphs.
We make three main contributions:
\begin{itemize}
  \item We show that the problem of decoding connected semantic graphs is an extension of a classic,
  well-studied problem in network optimization called the
  \term{prize-collecting Steiner tree (PCST)} problem
  \citep{held_traveling-salesman_1970}.
  This allows us to prove theoretical properties about decoding under certain
  graph constraints, and to adapt and extend existing PCST solving techniques
  to decoding semantic graphs. % (\S\ref{sec:pcsg}).
  \item We show that graph-based parsing is an effective approach for semantic
  dependency parsing, and in particular multitask semantic dependency parsing,
  where we get state-of-the-art results in three SDP formalisms.
  \item
  As our proposed future work, we introduce new \emph{lawful} neural
  encoders %~(\S\ref{sec:lawful})
  whose constrained forms allow for efficient exact
  inference by dynamic programming.
  We propose two models in particular:
    \term{Soft Patterns} (\term{SoPa}) and
    the \term{Star Semiring Graph Encoder} (\term{SSGE}).
\end{itemize}



\paragraph{Steiner Graphs.}
\label{sec:pcsg}

Our first contribution is a theoretical one---we show that decoding graphs
under a \emph{connected} constraint but without a \emph{spanning} constraint
results in an NP-complete problem, by reduction from the prize-collecting Steiner
tree (PCST) problem \citep{goemans_general_1992}. % \citep{held_traveling-salesman_1970}.
We introduce the \term{node- and edge-weighted connected subgraph (NEWCS)} problem---an extension
of PCST where both node and edge weights may be positive or negative---which
naturally arises when jointly predicting the nodes and edges of a semantic
graph.

We adapt two known techniques from PCST solving to \NEWCS\ solving.
First, we show that the integer linear program we used in \citet{liu-EtAl-2015-NAACL} can be
extended to \NEWCS\ solving.
Second, we combine our \term{maximum spanning connected graph (MSCG)} algorithm
\citep{flanigan2014amr} with a relaxation due to \citet{beasley_sst_1989} to produce
a novel approximate \NEWCS\ solver.
We also introduce a novel set of \emph{tests} which can transform a \NEWCS\ instance
into an instance with fewer nodes.
An optimal solution to the original problem instance can be recovered from a
solution for the reduced instance.


% \subsection{Characteristics of Semantic Graphs}
%
% Semantic graphs are closely related to syntactic dependency trees (which are
% labeled \term{arborescences}), but differ in key ways that affect the
% complexity of decoding.
% They may be:
% \begin{itemize}
%   \item non-spanning (i.e. the number of nodes may be different than the number of tokens),
%   \item reentrant (a node may have multiple parents), and
%   \item cyclic (when ignoring edge direction).
% \end{itemize}
% Of particular note is the fact that there is no fixed number of nodes nor
% edges that must be predicted.
% On the other hand, they do tend to be:
% \begin{itemize}
%   \item connected, and
%   \item deterministic (with respect to certain labels).
% \end{itemize}



% \paragraph{Factorization and Constraints}
%
% The factorization of the scoring function and the constraints on the set
% $\mathscr{Y}$ of allowed graphs both affect the complexity of decoding.
% Semantic graphs typically satisfy certain constraints, and enforcing them
% can improve performance.
% They include:
% \begin{itemize}
%   \item connected,
%   \item deterministic (with respect to certain labels).
% \end{itemize}
% On the other hand, semantic graphs do not satisfy the constraints that hold for
% in dependency syntax parses, which are \term{arborescences}, i.e. every token
% has exactly one parent and there are no cycles.
% Semantic graphs may be:
% \begin{itemize}
%   \item non-spanning,
%   \item reentrant (multiple parents),
%   \item cyclic (when ignoring edge direction).
% \end{itemize}
% this set of constraints on the set $\mathscr{Y}$ of allowed graphs,

% In this thesis, we relate the problem of decoding under these constraints to classic well studied problems in network optimization.
% We extend existing algorithms to the unique challenges and constraints in
% decoding semantic graphs.
% And we show that improvements in decoding lead to improvements in final semantic parsing performance.




% \subsection{Conditioning on Connectedness / Edge Density}
%
% We show that you can include features based on the number of connected components and/or the number of edges, without any increase in asymptotic runtime, by peeking along the way.


\paragraph{Semantic Dependency Parsing.}
\label{sec:sdp}

Semantic dependencies are a form of semantic graph representation where
the tokens of a sentence themselves are the nodes of the graph.
The arcs represent semantic relations between all content-bearing words in a
sentence \citep{oepen2014sdp,oepen2015sdp}.
We contribute two accurate graph-based SDP systems.

We first explore how varying sets of features and constraints affect parsing
performance in a linear, arc-factored model \citep{thomson-EtAl:2014:SemEval}.
Decoding is done using MSCG (when enforcing connectedness), and additional
constraints are enforced using Lagrangian relaxation.

We then show that multiple semantic dependency formalisms can be decoded
\emph{jointly}, with higher-order neural factors that model the
formalisms' relationships to each other \citep{Peng-EtAl:2017:ACL}.
We decode using an approximate solver
\citep[\term{AD$^3$;}][]{Martins2011DualDW}.
This multitask learning approach gives us state-of-the art performance on
all three formalisms in the SDP shared task.

% Already done:
% \begin{itemize}
%   \item Semantic Dependency Parsing
%   \citep[\textbf{SDP;}][]{oepen2014sdp,oepen2015sdp}.
%   Done in \citet{thomson-EtAl:2014:SemEval,Peng-EtAl:2017:ACL}.
% \end{itemize}
% Proposed:
% \begin{itemize}
%   \item Minimal Recursion Semantics \citep[\textbf{MRS;}]{copestake_minimal_2005}.
%   \item Abstractive summarization using the Abstract Meaning Representation (AMR).
%   Extend \cite{liu-EtAl-2015-NAACL}?
%   \item Multi-agent discourse parsing (SDRT?)
% \end{itemize}
%
% \paragraph{Multi-agent Discourse Parsing}
% \label{sec:discourse}
%
% Our third contribution is an application to multi-agent discourse parsing.

\paragraph{Lawful Networks.}
\label{sec:lawful}

In our third (future) contribution %is a novel approach to deep learning in which
we compose neural representations using functions that satisfy algebraic laws
% These lawful networks 
which permit exact inference via dynamic
programming.
% Our composition functions follow the semiring and star semiring laws respectively.

We introduce \term{Soft Patterns}~(\term{SoPa}), a type of finite state
automaton with neural weights, whose transitions are constrained
so that SoPa and its decisions are highly interpretable.
SoPa's composition functions follow the semiring laws and we use it as a
sequence encoder, experimenting on a suite of document
classification tasks \textit{(in submission)}.
We will explain how SoPa can be seen as a more powerful extension
of the Convolutional Neural Network \citep[CNN;][]{lecun_gradient-based_1998}, but more efficient and
interpretable (and less powerful) than recurrent neural networks like LSTMs \citep{hochreiter_1997}.

We also propose the \term{Star Semiring Graph Encoder}~(\term{SSGE}), an
analogous extension of the
Graph Convolutional Network~\citep[GCN;][]{kipf_semi-supervised_2016}
that can be used to contextualize graphs into fixed-size tensors.
% We learn composition functions that satisfy the star semiring laws by
% construction.
In our experiments, we will learn real square matrix representations for arcs, then use the
following operation to compose sequences of arc matrices into path matrices:
\begin{equation}
\maxmul(A, B)_{i,j} =
  \max_{k}{
    A_{i,k}  B_{k,j}
  }.
\end{equation}
$\maxmul{}$ and element-wise $\max{}$ together form a star semiring.
Kleene's algorithm \citep{kleene_representation_1951} can be used to compose a
matrix for every pair of nodes that encodes all possible paths between them.
Because $\maxmul{}$'s closure exists (the
\emph{star} in star semiring) and can be calculated efficiently
\citep{floyd_algorithm_1962}, even loopy paths can be handled.
Arc representations are learned end-to-end in conjunction with a downstream task.
This model is designed to produce encodings for pairs of nodes, but can be
slightly adjusted to produce encodings of individual nodes, subgraphs, or
the entire graph.

We will test the model's effectiveness for semantic parsing in two
experimental settings:
% \begin{itemize}
%   \item
  one where we run an NLP preprocessing pipeline to get a (sparse) labeled multigraph and learn lookup embeddings for each arc label; and
%   \item
  one where we learn a (dense) graph, with each arc's representation derived from the biLSTM hidden states of its endpoints.
% \end{itemize}
Comparing to an existing GCN-based system \citep{marcheggiani_encoding_2017} on the CoNLL-2009 benchmark \citep{hajic_conll-2009_2009} will allow us to directly test SSGEs vs. GCNs.

%multi-document summarization, with a GCN baseline \citep{yasunaga_graph-based_2017}.



% \subsection{Shortest Path RNNs}
% \label{subsec:sprnn}

% A \term{Shortest Path RNN}~(\term{SP-RNN}) is closely related to an
% \term{Observable Operator Process}~\citep[\term{OOP};][]{jaeger_observable_1997}. % trained using hard EM.


% \subsection{Soft Patterns}
% \label{subsec:softpattern}



\bibliographystyle{acl_natbib}
\bibliography{proposal}


\end{document}
