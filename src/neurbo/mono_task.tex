\section{Single-Task SDP}
\label{sec:mono_task}

Here we introduce our basic model, in which training and prediction for each
formalism is kept completely separate, as in Chapter~\ref{chap:arc-factored-sdp}.
We also lay out basic notation, which will be reused for our
multitask extensions.


\subsection{Problem Formulation}
\label{subsec:mono:formulation}
%\noindent{\textbf{Basic Notation.}}\ \ 
%We use lowercase boldface letters (e.g., $\mathbf{v}$, $\mathbf{x}$) to denote vectors, and denote the concatenation of them as $\left[\mathbf{v}; \mathbf{x}\right]$.
%Matrices are written using uppercase boldface letters (e.g., $\mathbf{U}$). 
%Function $F_{\text{A}}$ is a instantiation of $F$ with parameters $\theta_{\text{A}}$, and is written into $F_{\text{A}}$ for shorthand.

%We denote the span of consecutive words from $i$ to $j$ (inclusive) as $\mathbf{w}_{i:j}$, and we assume access to its POS tag sequence $\mathbf{p}_{i:j}$.\footnote{In this work gold-standard tag sequences are provided in the dataset.} \\
%\stcomment{bold, cuz it's multiple words/tags} 
%\hpcomment{I used bolded lower case to denote vectors
%  already. $w_{i:j}$ is supposed to be a sequence of scalars, and
%  $\mathbf{w}_{i:j}$ is a sequence of vectors.}
%\nascomment{I like bold for any sequence whether of words or reals}
%Given a sentence with $n$ words $w_{1:n}$, we refer to predicate $w_i$ as $\predicate{i}$. 
%An arc with predicate $w_i$, and argument $w_j$ is denoted as $\arc{i}{j}$.
%Similarly, we denote a labeled arc with label $\ell$ as $\larc{i}{j}{\ell}$. 


% \noindent{\textbf{Semantic dependency parsing}} \ \ 
The output of semantic dependency parsing is a labeled directed graph
(see Figure~\ref{fig:formalisms}).
Each arc has a label from a predefined set $\mathcal{L}$\stcomment{$^{(t)}$?},
indicating the semantic relation of the child to the head. 
Given input sentence $x$, let $\mathcal{Y}(x)$ be the set of possible semantic
graphs over $x$.
The graph we seek maximizes a score function
$\globalscore$:
\begin{equation}
\hat{y} = \argmax{y \in\mathcal{Y}(x)} \globalscore(x, y), 
\label{eq:score}
\end{equation}
We decompose $\globalscore$ into a sum of local scores %(or \emph{factors})
$s$ for \textbf{local structures} (or ``parts'') $p$ in the graph:
\begin{equation}
\globalscore(x, y) = \sum_{\factorpart \in y} {
\localscore(\factorpart).
}
\label{eq:score_decompose}
\end{equation}
For notational simplicity, we omit the dependence of $\localscore$ on
$x$.
See Figure~\ref{subfig:1st_order_factor_arg2} for examples of local
structures.
$\localscore$ is a parameterized function, whose parameters (denoted
$\Theta$ and suppressed here for clarity) will be learned from
the training data (\S\ref{subsec:learning}).
Since we search over every possible labeled graph (i.e., considering
each labeled arc for each pair of words), our approach can be considered a
\term{graph-based} (or \term{all-pairs}) method.
The models presented in this work all share this common
graph-based approach, differing only in the set of
structures they score and in the parameterization of the
scoring function $\localscore$. This approach also underlies 
state-of-the-art approaches to SDP \cite{martins2014sdp}. % \nascomment{link to
%  prior work like Andre's; please add cites if others fall into this paradigm}


\begin{figure}[tb]
  \centering
  \begin{subfigure}[b]{.3\columnwidth}
    \includegraphics[width=\columnwidth]{neurbo/fig/factors/1st_order_factor_.pdf}
    \caption{First-order.}
    \label{subfig:1st_order_factor_arg2}
  \end{subfigure}
  \rulesep
  \begin{subfigure}[b]{.3\columnwidth}
    \includegraphics[width=\columnwidth]{neurbo/fig/factors/2nd_order_factor_.pdf}
    \caption{Second-order.}
    \label{subfig:2nd_order_factor}
  \end{subfigure}
  \rulesep
  \begin{subfigure}[b]{.3\columnwidth}
    \includegraphics[width=\columnwidth]{neurbo/fig/factors/3rd_order_factor_.pdf}
    \caption{Third-order.}
    \label{subfig:3rd_order_factor}
  \end{subfigure}
  \caption{Examples of local structures. We refer to the number
  of arcs that a structure contains as its \term{order}.}
  \label{fig:substructures}
\end{figure}


\subsection{Basic Model}
\label{subsec:mono:basic}

Our basic model is inspired by recent successes in
neural arc-factored graph-based dependency parsing
\citep{kiperwasser2016simple,dozat2016deep,kuncoro-16}.
It borrows heavily from the neural arc-scoring architectures in those works, but
decodes with a different algorithm under slightly different constraints.


\subsubsection{Basic Structures}
%\nascomment{let's avoid the noun ``factor'' except in ``arc-factored''
%and ``factor graph''}

Our basic model factors over three types of structures ($\factorpart$ in Equation~\ref{eq:score_decompose}):
\begin{compactitem}
\item\pred, indicating a predicate word, denoted $\predicate{i}$;
\item\unlabeledarc, representing the existence of an arc from a
predicate to an argument, denoted $\arc{i}{j}$;
\item\labeledarc, an arc labeled with a semantic role, denoted $\larc{i}{j}{\ell}$.
\end{compactitem}
Here $i$ and $j$ are word indices in a given sentence, and $\ell$ indicates the arc label.
This list corresponds to the most basic structures used by
\citet{martins2014sdp}.  Selecting an output $y$ corresponds precisely
to selecting which instantiations of these structures are included.


To ensure the internal consistency of predictions, the following constraints
are enforced during decoding:
\begin{compactitem}
\item $\predicate{i}$ if and only if there exists at least one $j$ such that
$\arc{i}{j}$;
\item If $\arc{i}{j}$, then there must be exactly one label $\ell$ such that
$\larc{i}{j}{\ell}$.
Conversely, if not $\arc{i}{j}$, then there must not exist any
$\larc{i}{j}{\ell}$;
\end{compactitem}
We also enforce a \term{determinism} constraint \citep{flanigan2014amr}:
certain labels must not appear on more than one arc emanating from the same token.
The set of deterministic labels is decided based on their appearance in the training set.
Notably, we do not enforce that the predicted graph is connected or spanning.
If not for the \pred\ and determinism constraints, our model would be
\term{arc-factored}, and decoding could be done for each $i, j$ pair
independently.
Our structures do overlap though, and we employ \adcubed\ \citep{Martins2011DualDW}
to find the highest-scoring internally consistent semantic graph.
\adcubed\ is an approximate discrete optimization algorithm based on dual
decomposition.
It can be used to decode factor graphs over discrete variables when
scored structures
overlap, as is the case here.%\
% \footnote{
% If not for the deterministic roles constraint, there would be
% %Although there is 
% a straightforward $O(n^2)$ algorithm for exact decoding
% under our basic model \cite{martins2014sdp}. \hpcomment{maybe remove this footnote?}
% %, we use \adcubed\ so that we can
% %share a common decoding implementation with our extended models.
% }

% \nascomment{this is the place to define the first-order version, and
%   any ``nearly'' first-order versions we plan to explore.  I would get
% all of this out of the way first, before turning to multitask, then
% explain what additional higher-order factors we might consider in
% multitask parsing.}



\subsubsection{Basic Scoring}
\label{subsec:mono:scoring}

Similarly to \citet{kiperwasser2016simple}, %\hpcomment{removed a
                                %citation here, since the key point of
                                %Dozat and Manning is bilinear} 
our model
learns representations of tokens in a sentence using a bi-directional LSTM (BiLSTM).
Each different type of structure (\pred, \unlabeledarc, \labeledarc) then shares
these same BiLSTM representations, feeding them into
a multilayer perceptron (MLP) which is specific to the structure type.
We present the architecture slightly differently from prior work, to make the
transition to the multitask scenario (\S\ref{sec:multi_task}) smoother.
In our presentation, we separate the model into a function $\bm{\phi}$ that
represents the input (corresponding to the BiLSTM and the initial layers of the
MLPs), and a function $\bm{\psi}$ that represents the output (corresponding to
the final layers of the MLPs), with the scores given by their inner
product.\footnote{For clarity, we present single-layer BiLSTMs and MLPs, 
  while in practice we use two layers for both.
}

% We begin with two representation functions, which we
% call $\bm{\phi}$ and $\bm{\psi}$.
% Both embed first-order structures into $d$-dimensional real space. 
% 
% A first-order structure can be understood as a generic template instantiated
% with a word (\pred) or a pair of words (\unlabeledarc, \labeledarc).
% Therefore, we design two two separate representation functions aiming to capture the input words ($\bm{\phi}$)
% as well as the structure as a generic template ($\bm{\psi}$).\\
\paragraph{Distributed input representations.}
Long short-term memory networks (LSTMs) are a variant of recurrent neural
networks (RNNs) designed to alleviate the vanishing gradient problem in
RNNs \citep{hochreiter_1997}.
A bi-directional LSTM (BiLSTM) runs over the sequence in both directions
\citep{shcuster1997bidirectional,graves2012supervised}.

% Here we abstract a BiLSTM into a parameterized function $\bilstm$. 
Given an input sentence $x$ and its corresponding part-of-speech tag sequence,
each token is mapped to a concatenation of its word embedding vector and POS tag
vector.
%\footnote{We use the POS tags provided in the dataset.}
Two LSTMs are then run in opposite directions over the input vector sequence,
outputting the concatenation of the two hidden vectors at each position
$i$:  $\bilstm_i = \bigl[\fw{i}; \bw{i}\bigr]$
(we omit $\bilstm_i$'s dependence on $x$ and its own parameters).
$\bilstm_i$ can be thought of as an encoder that contextualizes each token
conditioning on all of its context, without any Markov assumption.
$\bilstm$'s parameters are learned jointly with the rest of the model
(\S\ref{subsec:learning}); we refer the readers to \citet{cho2015natural} for
technical details. 

\begin{figure}[htb]
%\begin{center}
	\includegraphics[width=\columnwidth]{neurbo/fig/diagram_singletask.pdf}
	\caption{Illustration of the architecture of the basic model. $i$ and $j$ denote
          the indices of tokens in the given sentence. 
          The figure depicts single-layer BiLSTM and MLPs, while in practice we use two layers for both.
          %Each token is
          %mapped to a concatenation of word vector and POS tag vector
          %by the embedding layer.
          %The basic model uses separate MLPs for each type of structures.
          %The input representations for predicate structures depend on only one word, 
          %while those for unlabeled arcs and labeled arcs depend on both words.
          %Predicates and unlabeled arcs are each mapped to a single output representation vector,
          %and each label gets a vector.
          %A first-order structure is scored by the inner product of its input and output representations.
          }
          %\vspace{-.25cm}
	\label{fig:diagram_singletask}
%\end{center}
\end{figure}
% $\bilstm$ is trained along with the rest part of the model.

% The input representation
% function $\bm{\phi}$ is built upon $\bilstm$, and defined over first-order structures.
% $\bm{\phi}$ is supposed to obtain the distributed representation for either a
% word (\pred) or a pair of words (\unlabeledarc, \labeledarc), depending on
% the structure type.
The input representation $\bm{\phi}$ of a \pred\ structure depends on the
representation of one word:
\begin{subequations}
\begin{align}
 \bm{\phi}(\predicate{i}) &=
 \tanh\bigl(\mathbf{C}_{\text{pred}} \bilstm_i +
 \mathbf{b}_\text{pred}\bigr). \label{eq:phi_pred}\\
\intertext{For \unlabeledarc\ and \labeledarc\ structures, it depends on both
the head and the modifier (but not the label, which is captured in the
distributed output representation):} \bm{\phi}(\arc{i}{j}) &=
 \tanh\bigl(\mathbf{C}_{\text{UA}} \bigl[\bilstm_i; \bilstm_j\bigr] +
 \mathbf{b}_\text{UA}\bigr), \label{eq:phi_ua}\\
 \bm{\phi}(\larc{i}{j}{\ell}) &=
 \tanh\bigl(\mathbf{C}_{\text{LA}} \bigl[\bilstm_i; \bilstm_j\bigr] +
 \mathbf{b}_\text{LA}\bigr).  \label{eq:phi_la}
 \end{align}
\end{subequations}


\paragraph{Distributed output representations.}
% In most NLP tasks, the outputs are discrete labels, either atomic (e.g., text
% categorization) or combinatorial (e.g., sequence labeling, parsing).
NLP researchers have found that embedding discrete output labels into a low
dimensional real space is an effective way to capture commonalities among
them
\interalia{srikumar2014learning,hermann_semantic_2014,fitzgerald2015semantic}.
In neural language models \interalia{bengio_neural_2003,mnih_three_2007} the
weights of the output layer could also be regarded as an output embedding.

We associate each first-order structure $p$ with a $d$-dimensional real vector
$\bm{\psi}(p)$ which does not depend on particular words in $p$.
Predicates and unlabeled arcs are each mapped to a single vector:
\begin{subequations}
\begin{align}
\bm{\psi}(\predicate{i}) &= \bm{\psi}_{\text{pred}},  \label{eq:psi_pred}\\
\bm{\psi}(\arc{i}{j})  &= \bm{\psi}_{\text{UA}}, \label{eq:psi_ua}
\intertext{and each label gets a vector:}
\bm{\psi}(\larc{i}{j}{\ell}) &= \bm{\psi}_{\text{LA}}(\ell).\label{eq:psi_la}
\end{align}
\end{subequations}


\paragraph{Scoring.}
Finally, we use an inner product to score first-order structures:
\begin{equation}
\label{eq:score_first_order}
s(p) = \bm{\phi}(p) \cdot \bm{\psi}(p).
\end{equation}
% Our score function is analogous to the multilayer perceptron (MLP) used in \citet{kiperwasser2016simple}. 
% Essentially, $\bm{\phi}$ returns the hidden layer representation of a 2-layer MLP with $\tanh$ nonlinearity, 
% and $\bm{\psi}$ is the output layer weights. 
% We present it in a way that can be generalized into multitask scenario (\S \ref{subsec:multi:ct}) with least effort.
Figure~\ref{fig:diagram_singletask} illustrates our basic model's architecture.

\subsection{Learning}
\label{subsec:learning}
The parameters of the model are learned using a max-margin objective.
Informally, the goal is to learn parameters for the score function so that the
gold parse is scored over every incorrect parse with a margin proportional to
the cost of the incorrect parse.
More formally, let $\mathcal{D} = \bigl\{(x_i, y_i)\bigr\}_{i=1}^{N}$ be the training set consisting of $N$ pairs of sentence $x_i$ and its gold parse $y_i$. Training is then the following $\ell_2$-regularized empirical risk minimization problem:
\begin{align}
 \min_{\Theta}{\frac{\lambda}{2}\lVert \Theta \rVert^2 + \frac{1}{N}\sum_{i=1}^{N}L\bigl(x_i, y_i; \Theta\bigr)},
\end{align}
%\stcomment{It's a little weird to
%just throw $\Theta$ in here out of nowhere. Maybe we should introduce
%$\Theta$ at the same time as $\globalscore$}
where $\Theta$ is the collection of all parameters in the model, and $L$ is the
structured hinge loss:
\begin{equation}
% \begin{split}
L\bigl(x_i, y_i; \Theta\bigr) = \hspace{-.2cm}\max_{y \in \mathcal{Y}(x_i)} \bigl\{ \globalscore\bigl(x_i, y\bigr) + c\bigl(y, y_i\bigr)\bigr\}
  - \globalscore\bigl(x_i, y_i\bigr).
% \end{split}
\end{equation}
$c$ is a weighted Hamming distance that
trades off between precision and recall  \citep{taskar-04}.
Following \citet{martins2014sdp}, we encourage recall over precision by using
the costs 0.6 for false negative arc predictions and 0.4 for false positives.


\subsection{Experiments}
\label{subsec:mono:experiment}

We evaluate our basic model on the English dataset from SemEval 2015 Task 18
closed track.\footnote{\url{http://sdp.delph-in.net}}
% It contains parallel annotations for all of the three formalisms over the same
% text.
We split as in previous work
\citep{almeida2015sdp,du2015sdp}, resulting in 33,964 training
sentences from \S 00--19 of the WSJ corpus, 1,692 development
sentences from \S 20, 1,410 sentences from \S 21 as
in-domain test data, and 1,849 sentences sampled from the Brown Corpus as out-of-domain test data.

The \emph{closed} track differs from the \emph{open} and \emph{gold} tracks in
that it does not allow access to any syntactic analyses.
In the open track, additional machine generated syntactic parses are
provided, while the gold-track gives access to various gold-standard syntactic analyses. 
Our model is evaluated with closed track data; it does not have access to any syntactic analyses during training or test. 

\thesisonly{
We refer the readers to \S\ref{sec:neurbo:implementation}
for implementation details, including training procedures, hyperparameters, pruning techniques, etc..
}

\begin{table}[tb]
\center
		\begin{tabulary}{\textwidth}{@{}l  l   r r r  r@{}}
			\toprule

			& \textbf{Model}
			& \textbf{DM}
			& \textbf{PAS}
			& \textbf{PSD}
			& \textbf{Avg.}\\

			\midrule

			\multirow{3}{*}{id}
			& Du et al., 2015
			& 89.1 &  91.3
			& 75.7 & 86.3 \\

			& A\&M, 2015
			& 88.2 &  90.9
			& 76.4 & 86.0 \\

			& \textsc{basic}
			& \textbf{89.4} &  \ul{\textbf{92.2}}
			& \ul{\textbf{77.6}} & \ul{\textbf{87.4}} \\

			\midrule[\cmidrulewidth]

			\multirow{3}{*}{ood}
			& Du et al., 2015
			& 81.8 &  87.2
			& 73.3 & 81.7 \\
			
			& A\&M, 2015
			& 81.8 &  86.9
			& 74.8 & 82.0 \\
			
			& \textsc{basic}
			& \ul{\textbf{84.5}} &  \ul{\textbf{88.3}}
			& \ul{\textbf{75.3}} & \ul{\textbf{83.6}} \\
			
			\bottomrule
		\end{tabulary}
	\caption[Caption for LOF]{Labeled parsing
          performance ($F_1$ score) on both in-domain (id) and
          out-of-domain (ood) test data. The
          last column shows the micro-average over the three
          tasks. Bold font indicates best performance without syntax.
          Underlines indicate statistical significance
          with \citet{bonferroni36teoria} correction compared
          to the best baseline system.\protect\footnotemark}
%           \vspace{-.25cm}
	\label{tab:mono_res_lf}
\end{table}
\footnotetext{Paired bootstrap, $p < 0.05$ after Bonferroni correction.}

\paragraph{Empirical results.}
As our model uses no explicit syntactic information, the most comparable models
to ours are two state-of-the-art closed track systems due to \citet{du2015sdp} and \citet{almeida2015sdp}.
\citet{du2015sdp} rely on graph-tree transformation techniques
proposed by \citet{du2014sdp}, and apply a voting ensemble to
well-studied tree-oriented parsers.
%\nascomment{I'm confused.  we said no
%  syntax!  do you mean that they applied syntactic parsers to semantics?}
%  \hpcomment{yes. I'm changing the word ``syntactic parsers'' to ``tree-oriented parsers''}
Closely related to ours is \citet{almeida2015sdp}, who used
rich, hand-engineered second-order features and \adcubed\ for inference.

Table~\ref{tab:mono_res_lf} compares our basic model to both baseline
systems (labeled $F_1$ score) on SemEval 2015 Task 18 test data.
Scores of those systems are repeated from the official evaluation results.
%The micro-averaged scores shown in the last column are calculated using the
%reported precision and recall\stcomment{This seems maybe unecessary to say?
%Or maybe could go in the table's caption?}.
Our basic model significantly outperforms the best
published results with a 1.1\% absolute improvement on the in-domain test set
and 1.6\% on the out-of-domain test set.
