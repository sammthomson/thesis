\section{Multitask SDP}
\label{sec:multi_task}
We introduce two extensions to our single-task model,
both of which 
use  training data for all three formalisms to improve performance on each
formalism's parsing task.
We describe a first-order model, where representation functions are
enhanced by parameter sharing while inference is kept separate for each task (\S\ref{subsec:multi:sharing}).
We then introduce a model with cross-task higher-order structures that uses joint
inference \emph{across} different tasks (\S\ref{subsec:multi:ct}).
%Similar to our single-task model, 
Both multitask models use
\adcubed\ for decoding, and are trained with the same margin-based objective,
as in our single-task model.



\subsection{Problem Formulation}
\label{{subsec:multi:formulation}}
We will use an
additional superscript $t \in \mathcal{T}$ to distinguish the three tasks
(e.g., $y^{(t)}$, $\bm{\phi}^{(t)}$), where $\mathcal{T} = \left\{\mathrm{DM}, \mathrm{PAS}, \mathrm{PSD}\right\}$.
Our task is now to predict three graphs
$\{y^{(t)}\}_{t\in\mathcal{T}}$ for a given input sentence $x$.
Multitask SDP can also be understood as parsing $x$ into a single unified  \emph{multigraph}
$y = \bigcup_{t\in\mathcal{T}}y^{(t)}$. 
Similarly to Equations~\ref{eq:score}--\ref{eq:score_decompose},
we decompose $y$'s score $\globalscore(x, y)$
into a sum of local scores for local structures in $y$,
and we seek a multigraph $\hat{y}$ that maximizes $\globalscore(x, y)$.


\subsection{Multitask SDP with Parameter Sharing}
\label{subsec:multi:sharing}
A common approach when using BiLSTMs for multitask learning is to share the
BiLSTM part of the model across tasks, while training specialized classifiers
for each task \citep{soggard2016deep}.
In this spirit, we let each task keep its own specialized MLPs, and explore
two variants of our model that share parameters at the BiLSTM level.

The first variant consists of a set of task-specific BiLSTM encoders 
as well as a common one that is shared across all tasks.
We denote it \textsc{freda}. 
\textsc{freda} uses a neural generalization of ``frustratingly easy'' domain
adaptation \citep{daume2007frustratingly,kim2016frustratingly}, where one augments
domain-specific features with a shared set of features to capture global patterns.
Formally, let $\{\bilstm^{(t)}\}_{t\in\mathcal{T}}$ denote the three task-specific
encoders.
We introduce another encoder $\shared$ that is shared across
all tasks.
Then a new set of input functions $\{\bm{\phi}^{(t)}\}_{t\in\mathcal{T}}$ can
be defined as in Equations~\ref{eq:phi_pred}--\ref{eq:phi_la}, for example:
\begin{equation}
\begin{split}
  \label{eq:phi_multitask_}
   \bm{\phi}^{(t)}(\larc{i}{j}{\ell}) =
  \tanh\bigl(\mathbf{C}_{\text{LA}}^{(t)} &\bigl[  \bilstm^{(t)}_i ;
      \bilstm^{(t)}_j ; %\\
      \shared_i; \shared_j \bigr]  +
  \mathbf{b}_\text{LA}^{(t)} \bigr) .
\end{split}
\end{equation}
The \pred\ and \unlabeledarc\ versions are analogous.
The output representations $\{ \bm{\psi}^{(t)} \}$ remain task-specific,
and the score is still the inner product between the input representation and
the output representation.
% A similar approach proved successful in sequence labeling
% \citep{kim2016frustratingly}.
% \hpcomment{is it safe to call it sequence tagging?}
%A similar approach proves successful 
% \nascomment{for a different problem?  what problem?} in \citet{kim2016frustratingly}.

The second variant, which we call \textsc{shared}, uses \emph{only} the shared
encoder $\shared$, and doesn't use task-specific encoders $\{\bilstm^{(t)}\}$.
It can be understood as a special case of \textsc{freda} where the dimensions
of the task-specific encoders are 0.


\subsection{Multitask SDP with Cross-Task Structures}
\label{subsec:multi:ct}

In syntactic parsing, higher-order structures have commonly been used to
model interactions between multiple adjacent arcs in the same dependency tree
\interalia{carreras2007experiments,smith2008dependency,martins2009concise,zhang_greed_2014}.
\citet{lluis2013joint}, in contrast, used second-order structures to
jointly model syntactic dependencies and semantic roles.
Similarly, we use higher-order structures \emph{across} tasks instead of
\emph{within} tasks. 
In this work, we look at interactions between arcs that share the same head
and modifier.\footnote{In the future we hope to model structures over larger motifs, both across
and within tasks, to potentially capture when an arc in one formalism corresponds to a
path in another formalism, for example.}
See Figures~\ref{subfig:2nd_order_factor} and \ref{subfig:3rd_order_factor} for
examples of higher-order cross-task structures.



\paragraph{Higher-order structure scoring.}

Borrowing from \citet{lei2014lowrank}, we introduce a low-rank tensor scoring
strategy that, given a higher-order structure $p$,
models interactions between the first-order structures (i.e., arcs) $p$ is made up of.
This approach builds on and extends the parameter sharing techniques in
\S\ref{subsec:multi:sharing}.
It can either follow \textsc{freda} or \textsc{shared} 
to get the input representations for first-order structures.

We first introduce basic tensor notation.
The \term{order} of a tensor is the number of its dimensions.
The \term{outer product} of two vectors forms a second-order tensor (matrix)
where $\left[\mathbf{u} \otimes \mathbf{v}\right]_{i,j} = u_iv_j$.
We denote the \term{inner product} of two tensors of the same dimensions by
$\left\langle \cdot, \cdot \right\rangle$, which first takes their element-wise
product, then sums all the elements in the resulting tensor.

For example, let $p$ be a labeled third-order structure, including one labeled arc from
each of the three different tasks: $p = \{ p^{(t)}\}_{t\in\mathcal{T}}$.
Intuitively, $s(p)$ should capture every pairwise interaction between the
three input and three output representations of $p$.
Formally, we want the score function to include a parameter for each term in
the outer product of the representation vectors:
$s(p) = $
\begin{align}
\label{score:tensor}
\left\langle
  \bm{\mathscr{W}},
  \bigotimes_{t\in\mathcal{T}}
  \left(\bm{\phi}^{(t)}\left(p^{(t)}\right) \otimes
      \bm{\psi}^{(t)}\left(p^{(t)}\right) \right)
\right\rangle,
\end{align}
where $\bm{\mathscr{W}}$ is a sixth-order tensor of parameters.\footnote{%
This is, of course, not the only way to model interactions
between several representations.
For instance, one could concatenate them and feed them into another MLP.
Our preliminary experiments in this direction suggested that it may be less
effective given a similar number of parameters, but we did not run full
experiments.
}

With typical dimensions of representation vectors, this  leads to an
unreasonably large number of parameters.
Following \citet{lei2014lowrank}, we upper-bound the rank of $\bm{\mathscr{W}}$ by $r$
to limit the number of parameters
($r$ is a hyperparameter, decided empirically).
Using the fact that a tensor of rank at most $r$ can be decomposed into a sum of
$r$ rank-1 tensors \citep{hitchcock1927expression}, we reparameterize $\bm{\mathscr{W}}$ to
enforce the low-rank constraint by construction:
\begin{equation}
\label{eq:tensor_decompose}
%\hspace{-.01cm}
\bm{\mathscr{W}} = \sum_{j=1}^{r} {\bigotimes_{t\in\mathcal{T}} 
%\hspace{-.1cm}
{\left(\left[\mathbf{U}_{\mathrm{LA}}^{(t)}\right]_{j,:}
%\hspace{-.25cm}
 \otimes \left[\mathbf{V}_{\mathrm{LA}}^{(t)}\right]_{j,:}\right)}}, 
%\hspace{-.1cm}
\end{equation}
where $\mathbf{U}_{\mathrm{LA}}^{(t)}, \mathbf{V}_{\mathrm{LA}}^{(t)} \in \R^{r\times d}$ are now our
parameters.
$[\cdot]_{j,:}$ denotes the $j$th row of a matrix.
Substituting this back into Equation \ref{score:tensor} and rearranging, the
score function $s(p)$ can then be rewritten as:
\begin{equation}
\label{eq:multiscore_decomposed}
\sum_{j=1}^{r} \prod_{t\in\mathcal{T}}
\left[\mathbf{U}_{\mathrm{LA}}^{(t)}\bm{\phi}^{(t)}\hspace{-.05cm}\left(p^{(t)}\right)\right]_{j}
\left[\mathbf{V}_{\mathrm{LA}}^{(t)}\bm{\psi}^{(t)}\hspace{-.05cm}\left(p^{(t)}\right)\right]_{j}.
\end{equation}
We refer readers to \citet{kolda2009tensor} for mathematical details.

For labeled higher-order structures our parameters consist of the set of six matrices,
$\{\mathbf{U}_{\mathrm{LA}}^{(t)}\} \cup \{\mathbf{V}_{\mathrm{LA}}^{(t)}\}$.
These parameters are shared between second-order and third-order labeled structures.
Labeled \emph{second-order} structures are scored as Equation~\ref{eq:multiscore_decomposed},
but with the product extending over only the two relevant tasks.
Concretely, only four of the representation functions are used rather than all six,
along with the four corresponding matrices from
$\{\mathbf{U}_{\mathrm{LA}}^{(t)}\} \cup \{\mathbf{V}_{\mathrm{LA}}^{(t)}\}$.
\emph{Unlabeled} cross-task structures are scored analogously,
% the parameter tensors are constructed analogously,
reusing the same representations, but with a separate set of parameter matrices
$\{\mathbf{U}_{\mathrm{UA}}^{(t)}\} \cup \{\mathbf{V}_{\mathrm{UA}}^{(t)}\}$.

Note that we are not doing tensor factorization; we are learning
$\mathbf{U}_{\mathrm{LA}}^{(t)}, \mathbf{V}_{\mathrm{LA}}^{(t)},
\mathbf{U}_{\mathrm{UA}}^{(t)},$ and $\mathbf{V}_{\mathrm{UA}}^{(t)}$
directly, and $\bm{\mathscr{W}}$ is never explicitly instantiated.


\paragraph{Inference and learning.}

Given a sentence, we use \adcubed\ to jointly decode all three formalisms.\footnote{%
Joint inference comes at a cost;
our third-order model is able to decode roughly 5.2 sentences 
(i.e., 15.5 task-specific graphs) per second on a single Xeon E5-2690 2.60GHz CPU.
}
The training objective used for learning is the sum of the losses for individual
tasks.
\thesisonly{
Hyperparameters are the same as those in the basic model, and are described in Appendix~\ref{sec:neurbo:implementation}.
One notable difference is that we apply early stopping based on the micro-averaged labeled ${F}_1$ score on the development set.
}

\subsection{Experiments}
\label{subsec:multi:experiment}


\paragraph{Experimental settings.}
We compare four multitask variants to the basic model, as well as the two baseline systems introduced in \S\ref{subsec:mono:experiment}.
\begin{compactitem}%[leftmargin=*]
  \item
    \textsc{shared1} is a first-order model.
    It uses a single shared BiLSTM encoder, and keeps the inference separate for each task.
    %It uses $\bm{\phi}$ (defined in Equation \ref{eq:phi_multitask}) as an
    %input representation function.
    %I.e., it only uses a single shared BiLSTM encoder, but with
    %task-specific output representations. \nascomment{check that!}
  \item
    \textsc{freda1} is a first-order model based on ``frustratingly
    easy'' parameter sharing.
    It uses a shared encoder as well as task-specific ones.
    The inference is kept separate for each task.
    %Its input representation function is $\widetilde{\bm{\phi}}$ (Equation
    %\ref{eq:phi_multitask_}).
    %I.e., it uses a shared encoder as well as task-specific encoders.
  \item
    \textsc{shared3} is a third-order model.
    %, equipped with cross-task structures. 
    It follows \textsc{shared1} and uses a single shared BiLSTM encoder,
    %It follows \textsc{shared1} and uses $\bm{\phi}$ as its representation function,
    but additionally employs cross-task structures and inference.
  \item
    \textsc{freda3} is also a third-order model.
    It combines \textsc{freda1} and \textsc{shared3} by using
    both ``frustratingly easy'' parameter sharing and cross-task structures and inference.
    %both $\widetilde{\bm{\phi}}$ and cross-task structures and inference.
\end{compactitem}
In addition, we also examine the effects of syntax by comparing our models to
the state-of-the-art open track system
\citep{almeida2015sdp}.\footnote{\citet{kanerva2015sdp} was the winner of the gold track, which overall saw
higher performance than the closed and open tracks.
Since gold-standard syntactic analyses are not available in most realistic
scenarios, we do not include it in this comparison.
} 
%\nascomment{the text above discussed some second order models, where
 % did those go?  }
%  \hpcomment{\citet{almeida2015sdp} submit to both closed and open tack, but we didn't compare the open track result in the single-task section. should we be more specific that the two baselines use the same model.}


\begin{table}[tb]
\ra{1.2}
   \center
    \begin{tabulary}{.95\textwidth}{@{}l  rrr  r c rrr  r@{}}
		\toprule

		& \multicolumn{4}{c}{In-domain} && \multicolumn{4}{c}{Out-of-domain} \\
		\cmidrule(lr){2-5} 
		\cmidrule(lr){7-10}
		& \textbf{DM}
		& \textbf{PAS}
		& \textbf{PSD}
		& \textbf{Avg.}
		&& \textbf{DM}
		& \textbf{PAS}
		& \textbf{PSD}
		& \textbf{Avg.}\\

		\midrule

		Du et al., 2015
		% in-domain
		& 89.1 &  91.3
		& 75.7 & 86.3
		% out-of-domain
		&& 81.8 &  87.2
		& 73.3 & 81.7 \\

		A\&M, 2015 (closed)
		% in-domain
		& 88.2 &  90.9
		& 76.4 & 86.0
		% out-of-domain
		&& 81.8 &  86.9
		& 74.8 & 82.0 \\

		A\&M, 2015 (open)$^\dagger$
		% in-domain
		& 89.4 &  91.7
		& 77.6 & 87.1
		% out-of-domain
		&& 83.8 &  87.6
		& 76.2 & 83.3 \\

		\textsc{basic}
		% in-domain
		& 89.4 &  \ul{92.2}
		& 77.6 & 87.4
		% out-of-domain
		&& \ul{84.5} &  \ul{88.3}
		& 75.3 & 83.6 \\

		\midrule[\cmidrulewidth]

		\textsc{shared1}
		% in-domain
		& 89.7 &  91.9
		& 77.8 & 87.4
		% out-of-domain
		&& \ul{84.4} &  \ul{88.1}
		& 75.4 & 83.5 \\

		\textsc{freda1}
		% in-domain
		& \ul{90.0} &  \ul{92.3}
		& \ul{78.1} & \ul{87.7}
		% out-of-domain
		&& \ul{84.9} &  \ul{88.3}
		& 75.8 & \ul{83.9}\\

		\midrule[\cmidrulewidth]

		\textsc{shared3}
		% in-domain
		& \ul{90.3} &  \ul{92.5}
		& \ul{\textbf{78.5}} & \ul{\textbf{88.0}}
		% out-of-domain
		&& \ul{\textbf{85.3}} &  \ul{88.4}
		& 76.1 & \ul{84.1}\\

		\textsc{freda3}
		% in-domain
		& \ul{\textbf{90.4}} & \ul{\textbf{92.7}}
		& \ul{\textbf{78.5}} & \ul{\textbf{88.0}}
		% out-of-domain
		&& \ul{\textbf{85.3}} & \ul{\textbf{89.0}}
		& \textbf{76.4} & \ul{\textbf{84.4}} \\

		\bottomrule
	\end{tabulary}
  \caption{Labeled ${F}_1$ score on the in-domain and out-of-domain test sets.
  The last columns show the micro-average over the three tasks. $\dagger$ denotes the use of syntactic parses. Bold font indicates best performance among all systems, and underlines indicate statistical significance with Bonferroni correction compared to A\&M, 2015 (open), the strongest baseline system.}
%   \vspace{-.3cm}
  \label{tbl:main}
\end{table}


\paragraph{Main results overview.} 
Table~\ref{tbl:main} compares our models to the best published results
(labeled ${F}_1$ score) on SemEval 2015 Task 18.
On the in-domain test set, our basic model improves over all closed track entries in all
formalisms.
It is even with the best open track system for DM and PSD, but improves on PAS
and on average, without making use of any syntax.
Three of our four multitask variants further improve over our basic
model; \textsc{shared1}'s differences are statistically insignificant.
Our best models (\textsc{shared3}, \textsc{freda3}) outperform the previous
state-of-the-art closed track system by 1.7\% absolute ${F}_1$, and the
best open track system by 0.9\%,  without the use of syntax.

We observe similar trends on the out-of-domain test set, with the
exception that, on PSD,
our best-performing model's 
improvement over the open-track system of 
\citet{almeida2015sdp} is not statistically significant.

The extent to which we might benefit from syntactic information remains unclear.
With automatically generated syntactic parses, \citet{almeida2015sdp} manage to
obtain more than 1\% absolute improvements over their closed track entry,
which is consistent with the extensive evaluation by
\citet{zhang2016transition}, but
we leave the incorporation of syntactic trees to future work. 
Syntactic parsing could be treated as yet another output
task, as explored in \citet{lluis2013joint} and in the transition-based
frameworks of \citet{henderson-13} and \citet{swayamdipta2016greedy}.%, for example.
%With gold-standard syntax parses, \citet{kanerva2015sdp} observe further 2\% to 5\% absolute improvements.\footnote{With machine-generated syntactic parses, \citet{kanerva2015sdp} get 86.2, 90.6, and 73.6 labeled ${F}_1$ for DM, PAS, and PSD, respectively.}
% In practice, high-quality syntactic analysis may not be available.
% Therefore, we design our models to not rely on any syntax information.
% Although it's less comparable, our basic model is able to achieve similar or
% stronger performance than open track \citet{almeida2015sdp}, and the multitask models yield 0.3\% to 0.9\% absolute improvements.
%Notably, all of our models outperform \citet{kanerva2015sdp} on two of the three tasks.





\paragraph{Effects of structural overlap.}
We hypothesized that the overlap between formalisms would enable multitask
learning to be effective;
in this section we investigate in more detail how structural overlap affected
performance.
% We look at overlap between formalisms in aggregate and also on a label-by-label
% basis.
By looking at \emph{undirected} overlap between unlabeled arcs, we discover
that modeling only arcs in the same direction may have been a design mistake.
% By breaking out performance improvement by label, we find that improvement does
% correlate with overlap.

DM and PAS are more structurally similar to each other than either
is to PSD.
Table \ref{tab:structural_similarity} compares the structural
similarities between the three formalisms in unlabeled ${F}_1$ score (each
formalism's gold-standard unlabeled graph is used as a prediction of each other
formalism's gold-standard unlabeled graph).
All three formalisms have more than 50\% overlap when ignoring arcs' directions,
but considering direction, PSD is clearly different; PSD reverses the
direction about half of the time it shares an edge with another formalism.
A concrete example can be found in Figure~\ref{fig:formalisms}, where 
DM and PAS both have an arc from \textit{``Last''} to \textit{``week,''} 
while PSD has an arc from \textit{``week''} to \textit{``Last.''}




% \begin{figure}
%  \centering
%   \begin{subfigure}{\columnwidth}
%     \includegraphics[width=\columnwidth]{neurbo/fig/role_sorted_by_overlap/freda3.freda1.abs/p_sorted_by_overlap.pdf}
%    \vspace{-.5cm}
%     \caption{Precision}
%   \end{subfigure}
%   \begin{subfigure}{\columnwidth}
%   \vspace{-.25cm}
%     \includegraphics[width=\columnwidth]{neurbo/fig/role_sorted_by_overlap/freda3.freda1.abs/r_sorted_by_overlap.pdf}
%     \vspace{-.5cm}
%     \caption{Recall}
%   \end{subfigure}
%     \caption{Absolute improvements in labeled precision and recall
%     		relative to the overlap of semantic roles for each of the formalisms and their micro-average.
% 		Compared models are \textsc{freda3} and \textsc{freda1}
%    		%\nascomment{do
%         		%you mean ``formalism''?  also, make the font in these figures bigger.}.
% 		}
%       \vspace{-.5cm}
%       \label{fig:improvement}
% \end{figure}

We can compare \textsc{freda3} to \textsc{freda1} to isolate the effect of
modeling higher-order structures.
Table~\ref{tab:res_id_ulf} shows performance on the development data in both
unlabeled and labeled ${F}_1$.
We can see that \textsc{freda3}'s unlabeled performance improves on DM and PAS,
but \emph{degrades} on PSD.
% This suggests that our design of cross-task structures could be improved by allowing
% arcs of opposite directions, which we leave to future work.
% In Figure~\ref{fig:improvement},
% we plot the absolute precision and recall improvements
% relative to the semantic roles' overlap (the number
% of second-order and third-order structures it appears in, divided by
% the number of first-order structures it appears in).
% We note two trends: modeling cross-task structures tends to improve recall more
% than precision; and the more overlap, the larger the effect size.
This supports our hypothesis, and suggests that in future work, a more careful
selection of structures to model might lead to further improvements.


\begin{table}[tb]
\center
\ra{1.2}
		\begin{tabulary}{\textwidth}{@{}l  RRR c RRR@{}}
			\toprule

			& \multicolumn{3}{c}{Undirected}
			& \phantom{}
			& \multicolumn{3}{c}{Directed}\\
			\cmidrule{2-4}
			\cmidrule{6-8}
			& \bf{DM} & \bf{PAS} & \bf{PSD} &
			& \bf{DM} & \bf{PAS} & \bf{PSD}\\

			\midrule
			
			\bf{DM}
			& - & 67.2 & 56.8 &
			& - &  64.2 & 26.1\\
			
			\bf{PAS}
			& 70.0 & - & 54.9 &
			& 66.9 &  - & 26.1\\
			
			\bf{PSD}
			& 57.4 & 56.3 & - &
			& 26.4 &  29.6 & -\\

			\bottomrule
		\end{tabulary}
	\caption{Pairwise structural similarities between the three
          formalisms in unlabeled ${F}_1$ score. Scores from \citet{oepen2015sdp}.} 
%            \vspace{-.25cm}
	\label{tab:structural_similarity}
\end{table}

\begin{table}[tb]
\center
\ra{1.2}
		\begin{tabulary}{\textwidth}{@{}l  RR c RR c RR@{}}
			\toprule

			& \multicolumn{2}{c}{\textbf{DM}}
			& \phantom{}
			& \multicolumn{2}{c}{\textbf{PAS}}
			& \phantom{}
			& \multicolumn{2}{c}{\textbf{PSD}}\\

			\cmidrule{2-3}
			\cmidrule{5-6}
			\cmidrule{8-9}
			& U$F$ & L$F$ &
			& U$F$ & L$F$ &
			& U$F$ & L$F$\\

			\midrule

			%these are test numbers
			%\textsc{shared1}
			%& 91.1 &  89.7
			%& 93.1 & 91.9 
			%& 88.8 & 77.8\\

			%these are dev numbers
			\textsc{freda1}
			& 91.7 &  90.4 &
			& 93.1 & 91.6 &
			& 89.0 & 79.8\\

			% these are test numbers
			%\textsc{shared3}
			%& 91.5 &  90.3
			%& 93.7 & 92.5
			%& 87.8 & 78.5\\

			%these are dev numbers
			\textsc{freda3}
			& 91.9 & 90.8 &
			& 93.4 & 92.0 &
			& 88.6 & 80.4 \\

			%these are test numbers 
			%\hline\hline
			%\textsc{basic}			
			%& 90.7 & 89.4
			%& 93.4 & 92.2
			%& 86.6 & 77.6 \\

			\bottomrule
		\end{tabulary}
	\caption{Unlabeled (U$F$) and labeled (L$F$) parsing
          performance of \textsc{freda1} and \textsc{freda3} on the development set of SemEval  2015 Task 18.
%           \vspace{-.25cm}
	\label{tab:res_id_ulf}}
\end{table}

