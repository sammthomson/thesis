\chapter{Introduction}
\label{chap:intro}
% \nas{
% When you make the case in the intro, I suggest that you distinguish (in separate paragraphs?) scientific arguments about representation (e.g., from linguistics) from application-oriented arguments (talking about symbolic manipulations, NLG, etc.).
% }


\begin{figure}%[.5\textwidth] %[htb,width=\textwidth] %[.5\textwidth]
  \centering
  \includegraphics[width=.95\textwidth]{fig/formalisms/multigraph.pdf}
  \caption{
    An example sentence annotated with semantic frames and roles from \term{FrameNet} \citep[][\textit{top/purple}]{baker_98},  semantic dependencies from \term{DM} \citep[][\textit{middle/red}]{oepen2014sdp} and syntax from \term{Universal Dependencies} \citep[][\textit{bottom/green}]{silveira14gold}.
    Nodes corresponding to token spans have solid blue fill, and nodes added by a formalism are hollow.
    Token nodes are shown both above and below the sentence for clarity.
    Gray arcs labeled ``+'' are added between adjacent token nodes to encode the linear order of the sentence (as graphs are considered to be unordered).
  }
  \label{fig:fn-dm-ud}
\end{figure}


The field of natural language processing has had a lasting love affair with trees.
The community has embarked on large-scale tree-based annotation efforts, such as the Penn Treebank~
\citep[PTB;][]{marcus_building_1993} and the universal dependency corpus~\citep[UD;][]{silveira14gold}. %---
% enabled the
And we have a swarm of algorithms and machine learning techniques at our disposal for working with linguistic
structures in the shape of trees, from
CKY~\citep{cocke_programming_1969,kasami_efficient_1965,younger_recognition_1967},
Chu-Liu-Edmonds~\citep{chu_shortest_1965,edmonds_optimum_1967}, and Eisner's algorithm~\citep{eisner_three_1996}, to Recursive Neural Networks~\citep{goller_learning_1996,costa_towards_2003,Socher:2013} and TreeLSTMs~\citep{tai_improved_2015}.
Trees ably represent \term{syntax}, the rules and process by which the words of a sentence combine with each other to form larger and larger phrases and eventually the sentence.
But when one tries to represent the \emph{result} of that combination
process---the meaning or \term{semantics} of a sentence---one quickly finds
that trees are no longer sufficient.
Entities may participate in multiple events, and
\term{graphs}, specifically labeled directed graphs, become necessary.
Graphs are well-studied objects,
but they have not received the same level of attention in NLP as have trees,
and there remain several unanswered questions around to their use in computational linguistics.
This thesis contributes several new techniques and algorithms for working with linguistic graphs.
We roughly divide our inquiry into two broad research questions:
\begin{itemize}
  \item What algorithms and techniques are effective at \emph{predicting} graphs?
  \item What algorithms and techniques are effective for \emph{conditioning on} a graph (when making some other prediction)?
\end{itemize}
Following terminology first introduced by Warren Weaver in the context of machine translation, we call the act of predicting a graph \term{decoding}, and the act of predicting some other representation \emph{from} a graph \term{encoding}.

While trees are graphs, graphs are not trees, so moving from syntax to semantics involves working with graph structures that follow \emph{fewer} constraints.
In a context-free grammar derivation~\citep{chomsky_a_note_1959}, such as those found in PTB, each constituent (other than the sentence itself) has a single parent constituent.
Once a constituent has been expanded via a production rule in the grammar, its children never interact again.
Thus a syntactic parse has a neat, orderly, hierarchical shape, where
constituents recursively expand until the surface words of the sentence are produced.
Similarly, in a syntactic dependency parse resulting from a dependency grammar~\citep{tesniere_elements_1959}, each token modifies another single token, flowing all the way up to a root token, resulting in an \term{arborescence} (a directed graph that is a rooted tree).
By contrast, a sentence's semantics is a tangled web of interactions.
Specifically, the entities and concepts mentioned in a sentence may each participate in multiple events and hold multiple relationships to each other.
\term{Coreference}, \term{control structure}, and \term{coordination}
(among others), are linguistic mechanisms that can signify that a participant is involved in more than one situation.
For instance, in the sentence \textit{``Now I can buy a soda and spend money''}~(Figure~\ref{fig:fn-dm-ud}), \textit{I} am the participant who has the capability referred to by \textit{``can''}, and within that capability, \textit{I} am also the instigating agent of both the hypothetical \textit{buying} event and the hypothetical \textit{spending} event.


% When dealing with natural language text as input in a machine learning system, it is
% often helpful \com{as an intermediate step} to process the text into structured
% representations of its linguistic content.
Trees are one kind of special case of graphs, but so are other data structures used in NLP.
Multi-word expressions, coreference, syntax, semantics, discourse, and many
more types of structured analyses %have been found useful in applications such
% as summarization, question answering, machine translation, natural language
% inference, etc. % \sam{cite}
% Each of these linguistic structures is 
are either annotated directly as a labeled
directed graph \interalia{copestake_minimal_2005,hajic2012psd,banarescu_abstract_2013,silveira14gold},
or are easily convertible to one \interalia{baker2007framenet,surdeanu2008conll}.
Sequences are representable as a a linear chain, which is a special case of a graph (see the gray \textit{``+''} arcs in Figure~\ref{fig:fn-dm-ud}, for example).
% Their generality over trees, for instance, allows them to represent
% relational semantics while handling phenomena like coreference and
% coordination.
Arcs of a graph have a source node and a destination node, and so naturally represent \emph{binary} relationships, but non-binary relations
(like in a logical form% with $n$-ary predicates
, for example) can be handled by the
introduction of new nodes, as in neo-Davidsonian
semantics~\citep{parsons_events_1990}.
In other words, graphs are extremely general.
If our two research questions can be answered, and we can find effective graph encoders and decoders, then we have tools for working with all of these formalisms, and can use the same generic framework for any stage in a core NLP pipeline.
Special cases of graphs like sequences and trees often have specialized algorithms that are more efficient, but specialized algorithms require specialized implementations, and there is definite value in developing tools that work for any or all representations out of the box.
We also note that even some syntactic formalisms are moving toward non-tree graphs
\citep{Marneffe2014UniversalSD}.
Furthermore, as we show in Chapter~\ref{chap:meurbo}, the union of multiple linguistic analyses can be combined into a single \term{multigraph}, leading to improved performance.
Even if the individual analyses are trees or sequences, their union will not
be, and so  in such multi-task learning scenarios general purpose graph
algorithms are needed.

Our main interest is in single-sentence semantics, for which labeled directed graphs are an especially natural and flexible representation.
Semantic graphs tend to have certain characteristics, which we tailor our algorithms toward to some extent.
The nodes in a linguistic graph can be labeled, often with the name of a
concept, entity, or event.
Nodes can be grounded in \emph{spans} from the sentence or they can be abstract.
Arcs between nodes are labeled with a linguistic relationship that holds between the nodes, and the relationships are usually asymmetric, requiring the use of directed arcs.
There tend to be $O(n)$ nodes and $O(n)$ arcs for a sentence with $n$ tokens.%
\footnote{
With this in mind, $O(n^3)$ algorithms are often acceptable~(as in Section~\ref{sec:kgen:kgen}), but NP-hard problems~(as in Chapter~\ref{chap:steiner}) are not, and require approximate algorithms.
}
Semantic graphs tend to be \term{connected}, reflecting the fact that sentences
often express a single \emph{coherent} statement or question.
See Figure~\ref{fig:fn-dm-ud} for an example sentence with several kinds of annotations.
\stcomment{
MRS argues that graphs are a more natural representation for
semantics than, say, recursive tree structures, because they underspecify the
order of composition in the right way;
i.e. derivation trees have spurious ambiguity w.r.t. the semantics.
For example, the semantics of intersective adjectives are commutative
and associative.
So the fact that ``fierce black cat'' has a different branching structure
than ``gato negro y feroz'' obscures the fact that they have the same
meaning \citep{copestake_minimal_2005}.
}
\stcomment{Could also compare to lambda-calculus logical forms.}
% Various formalisms have been used to represent natural language semantics, including lambda calculus logical forms \sam{cite}, forests of shallow tree stumps (as in semantic role labeling \sam{cite}), trees (\sam{as in Percy Liang stuff}), and labeled directed graphs (as in AMR, SDP \sam{cite}. see Figure~\ref{fig:sdp-formalisms}).
% Of these, labeled directed graphs are the most flexible --- other formalisms being either a special case of a graph, or convertible to a graph without loss of information.%
% 
% This thesis focuses on such graphical representations of text.
% We develop novel algorithms and models for
% \emph{decoding}---automatically extracting graphs from raw text; and
% \emph{encoding}---transforming graphs to fixed-size tensors
% so that they can be used as input to a downstream system.
% We primarily focus on \emph{semantic} graphs---graphs that represent sentence meanings.
% Semantic graphs are not necessarily trees, so they require methods that can handle arbitrary graphs.

This thesis makes four main contributions.
\begin{itemize}
  \item First, in Chapter~\ref{chap:arc-factored-sdp}, we introduce an arc-factored model for \term{semantic
  dependency parsing}~\citep[\term{SDP};][]{oepen2014sdp,oepen2015sdp}.
  We explore a variety of features and graph constraints, doing extensive empirical
  comparisons to find practical model choices for each of the three SDP formalisms.
  \item In Chapter~\ref{chap:meurbo}, we introduce \term{Neural Turbo Parsing (NeurboParsing)} and show that
  it is an even more effective approach for semantic dependency parsing.
  Turbo parsing allows for decoding with higher-order factors that score small
  motifs in the output graph.
  In particular we show how NeurboParsing can be used for
  \emph{multitask} semantic dependency parsing,
  where we get state-of-the-art results in three SDP formalisms.
  \item
  Next (Chapter~\ref{chap:steiner}), we show that the problem of decoding \emph{connected} semantic graphs is an
  extension of a classic, well-studied problem in network optimization called the
  \term{prize-collecting Steiner tree (\PCST)} problem
  \citep{goemans_general_1992}.
  This allows us to prove theoretical properties about decoding under certain
  graph constraints, and to adapt and extend existing \PCST\ solving techniques
  to decoding semantic graphs.
  \item
  In Chapter~\ref{chap:kgen}, we %move on to the problem \emph{encoding} graphs.
  %We 
  introduce two new \emph{lawful} neural encoders, \term{Soft Patterns} (\term{SoPa})
  and \term{Kleene Graph Encoder Networks} (\term{\KGEN})
  whose constrained forms allow for efficient exact inference.
  SoPa encodes \emph{sequences} using an array of small neurally-weighted finite state automata,
  and we test it on text classification.
  \KGEN\ extends SoPa in order to encode \emph{graphs}.
  The resulting output of \KGEN\ is as if every path in a graph had been
  encoded with a SoPa-like sequence model, then grouped by source and destination, then max-pooled.
  Even though there are infinitely many paths in a graph, this can be done
  efficiently with dynamic programming.
\end{itemize}
Chapters~\ref{chap:arc-factored-sdp}--\ref{chap:steiner}
work toward better graph decoding, and Chapter~\ref{chap:kgen} is devoted to graph encoding.
These contributions together are aimed toward a generic \emph{graph-to-graph} pipeline for structured prediction.
Given any task where one must predict some structured output
$y \in \mathcal{Y}$ given some structured input $x \in \mathcal{X}$:
\begin{enumerate}
  \item convert $x$ and $y$ to multigraphs $G_x$ and $G_y$, respectively,
  \item \textbf{encode} $x$ as a contextualized tensor $v_x$, and
  \item \textbf{decode} $y$, conditioned on $v_x$.
\end{enumerate}
While not the only options, we demonstrate in this thesis that \KGEN\ is an effective method for step 2, and that NeurboParsing is an effective approach for step 3.
Our experiments are tailored toward broad-coverage semantic dependency parsing, but our methods have much broader potential impact because they apply generally to graphs.
