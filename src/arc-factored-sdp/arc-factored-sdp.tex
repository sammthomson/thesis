\chapter{Arc-factored Semantic Dependency Parsing}
\label{chap:arc-factored-sdp}

\astfootnote{\textit{The work described in this chapter was done with Brendan O'Connor, Jeffrey Flanigan, David Bamman, Jesse  Dodge, Swabha Swayamdipta, Nathan Schneider, Chris Dyer, and Noah A.~Smith, and published as \citet{thomson-EtAl:2014:SemEval}.}}%
Semantic dependencies are a form of semantic graph representation where
the tokens of a sentence themselves are the nodes of the graph.
The arcs represent semantic relations between all content-bearing words in a
sentence.
% We contribute two accurate graph-based SDP systems.
In this chapter, we explore how varying sets of features and constraints affect semantic dependency parsing
performance in a linear, arc-factored model.


\begin{figure}[tbh]
  \centering
  \begin{subfigure}[b]{\columnwidth}
    \label{subfig:dm}
    \includegraphics[width=\columnwidth]{fig/formalisms/dm.pdf}
    \caption{DM}
  \end{subfigure}
  \begin{subfigure}[b]{\columnwidth}
    \label{subfig:pas}
    \includegraphics[width=\columnwidth]{fig/formalisms/pas.pdf}
    \caption{PAS}
  \end{subfigure}
  \begin{subfigure}[b]{\columnwidth}
    \label{subfig:psd}
    \includegraphics[width=\columnwidth]{fig/formalisms/psd.pdf}
    \caption{PSD}
  \end{subfigure}
  \caption{An example sentence annotated with the three semantic formalisms of
  the broad-coverage semantic dependency parsing shared tasks.}
%   \vspace{-.25cm}
  \label{fig:formalisms}
\end{figure}


% \section{Introduction}
% \label{sec:semeval}


% We present an arc-factored statistical model for
% semantic dependency parsing, as defined by the SemEval 2014 Shared
% Task 8 on Broad-Coverage Semantic Dependency Parsing.
% Our entry in the open track placed second in the competition.

We present in this chapter two systems that produce semantic dependency parses
in the three formalisms of the SemEval Shared Tasks on Broad-Coverage Semantic Dependency
Parsing \citep{oepen2014sdp,oepen2015sdp}.
We describe the task and data in \S\ref{sec:task}.
These systems generate parses by extracting
features for each potential dependency arc and learning a statistical model to
discriminate between good arcs and bad;
the first treats each labeled edge decision as an independent multiclass
logistic regression (\S\ref{s:logitedge}), while the second predicts arcs
as part of a graph-based structured support vector machine (\S\ref{s:graphparser}).
Common to both models is a rich set of features on arcs, described in
\S\ref{s:edgefeatures}.
\thesisonly{
We include a discussion of features found to
have no discernible effect, or negative effect, during development (\S\ref{s:badfeatures}).
}

Our system placed second in the open track of the SemEval 2014 Shared
Task 8 on Broad-Coverage Semantic
Dependency Parsing task (in which output from syntactic parsers and other outside resources \emph{can} be used).
We present our results in \S\ref{s:evaluation}.
In Chapter~\ref{chap:meurbo}, we improve on this work by showing that the multiple
semantic dependency formalisms can be decoded
\emph{jointly}, with higher-order neural factors that model the
formalisms' correlations with each other.
This multitask learning approach gives us state-of-the art performance on
all three formalisms in the SDP shared task.


\input{arc-factored-sdp/data}


\section{Models} \label{s:models}

We treat the problem as a three-stage pipeline.
The first stage prunes words by predicting whether they have any incoming or
outgoing edges at all (\S\ref{subsec:arc-factored-sdp:singleton-model}); 
if a word does not, then it is not 
considered for any attachments in later stages.
The second stage predicts where edges are
present, and their labels (\S\ref{s:edge_model}).
The third stage predicts whether a predicate word is a \emph{top} or not
(\S\ref{s:top_model}).
Formalisms sometimes annotate more than one ``top'' per sentence, but we
found that we achieve the best performance on all formalisms by predicting only
the one best-scoring ``top'' under the model.
\bocomment{Singleton pruning does not matter for LogitEdge.  It doesn't affect accuracy, I'm pretty sure.  But it is essential for the graph model.}



\subsection{Singleton Classification} \label{subsec:arc-factored-sdp:singleton-model}

For each formalism, we train a classifier to recognize \emph{singletons},
nodes that have no parents or children.
(For example, punctuation tokens are often singletons.)
This makes the system faster without affecting accuracy.
For singleton prediction, we use a token-level logistic regression
classifier, with features including
the word, its lemma, and its
part-of-speech tag.
If the classifier predicts a probability of 99\% or higher 
the token is pruned; this removes around 10\% of tokens.
(The classifier performs differently on different formalisms;
on \PAS\ it has perfect accuracy, 
while on
\DM\ and \PCEDT\ accuracy is in the mid-90's.)




\subsection{Edge Prediction} \label{s:edge_model}

In the second stage of the pipeline, we predict the set of labeled directed
edges in the graph.
We use the same set of edge-factored features (\S\ref{s:features}) in two
alternative models: an edge-independent multiclass logistic
regression model (\logitedge, \S\ref{s:logitedge}); and a structured SVM 
\citep{taskar_max_2003,tsochantaridis_support_2004} that enforces a
\emph{determinism} constraint for certain labels, which allows each word to
have at most one outgoing edge with that label (\svmedge,~\S\ref{s:graphparser}).
For each formalism, we trained both models with varying features enabled and
hyperparameter settings and submitted the configuration that produced the best
labeled $F_1$ on the development set.
For \DM\ and \PCEDT, this was \logitedge;
for \PAS, this was \svmedge.
We report results only for the submitted configurations, with different features
enabled.
Due to time constraints, full hyperparameter sweeps and comparable feature sweeps were not possible.


\subsubsection{\logitedge\ Parser}
\label{s:logitedge}



The \logitedge\ model considers only token index pairs $(i, j)$ where %that are
% within 10 tokens of each other (i.e.~
$|i-j| \leq 10$, $i \ne j$, and both $t_i$ and
$t_j$ have been predicted to be non-singletons by the first stage.
% In other words, edges with more than 9 tokens between its two endpoints are not
% considered.
Although this prunes some gold edges, among the formalisms,
95\%--97\% of all gold edges are between tokens of distance 10 or less.
Both directions $i \rightarrow j$ and $j \rightarrow
i$ are considered between every pair.

%\noindent
Let $L$ be the set of $K+1$ possible output labels: the formalism's original $K$
edge labels, plus the additional label \noedge, which indicates that no edge
exists from $i$ to $j$.
The model treats every pair of token indices $(i, j)$ as an
independent multiclass logistic regression over output space $L$.
Let $x$ be an input sentence.
For candidate parent index $i$, child index $j$, and edge label $\ell$, we
extract a feature vector $\bm{f}(x, i, j, \ell)$, where $\ell$ is conjoined with
every feature described in \S\ref{s:features}.
The multiclass logistic regression model defines a distribution over $L$,
parameterized by weights $\bm\phi$:
\begin{align}
  P(\ell\mid\bm\phi,x, i,j)  = \frac{
  	\exp\{\bm\phi \cdot \bm{f}(x, i, j, \ell)\}
  } {
  	\sum_{\ell^\prime \in L} {
  		\exp\{\bm\phi \cdot \bm{f}(x, i, j, \ell^\prime)\}
  	}
  }.
\end{align}

\noindent
$\bm\phi$ is learned by minimizing total negative log-likelihood of the above
(with weighting; see below), plus $\ell_2$ regularization.
AdaGrad \citep{duchi_adaptive_2011} is used for optimization.
This seemed to optimize faster than L-BFGS \citep{Liu1989LBFGS}, at least for earlier
iterations, though we did no systematic comparison. Stochastic gradient steps
are applied one at a time from individual examples, and a gradient step for the
regularizer is applied once per epoch.

The output labels have a class imbalance; in all three formalisms, there
are many more $\noedge$ examples than true edge examples.
We improved $F_1$ performance by
downweighting $\noedge$ examples through a weighted log-likelihood objective,
$\sum_{i,j} \sum_\ell w_\ell \log P(\ell\,|\,\bm\phi, x, i, j)$, 
with $w_{\noedge}=0.3$ (selected on development set)
and $w_{\ell} = 1$ otherwise.

% 0.4

% Besides the edge logistic regression system, there were both pre- and post-processing steps.

% \textbf{Preprocessing:}
% \bocomment{TODO need to check how much preproc was used for this.  Was singleton pruning turned on?  It looks like we commented out the prune features in LRParser.java.  But singleton pruning might have been turned on.}

% \textbf{Decoding and postprocessing:}
\noindent
\textbf{Decoding:} 
To predict a graph structure at test-time for a new sentence,
the most likely edge label is predicted for every candidate $(i, j)$ pair of
unpruned tokens.
If an edge is predicted for both directions for a single $(i,j)$
pair, only the edge with the higher score is chosen.
(There are no such bidirectional edges in the training data.)
This post-processing actually did not improve accuracy on \DM\ or \PCEDT;
it did improve \PAS\ by $\approx$0.2\% absolute $F_1$, but we did not submit \logitedge\ for \PAS.



\subsubsection{\svmedge~Parser}
\label{s:graphparser}



In the \svmedge~model, we use a structured SVM
with a determinism constraint.
This constraint ensures that each word token has at most one outgoing edge for
each label in a set of deterministic labels $L_d$.
For example, in \DM\ a predicate never has more than one child with edge
label ``ARG1.''
 $L_d$ was chosen to be the set of edges that were $> 99.9\%$
deterministic in the training data.\footnote{%
By this we mean that of the nodes that have at least one
outgoing $\ell$ edge, $99.9\%$ of them have only one outgoing $\ell$ edge.
For \DM, $L_d=L \setminus$\{``\_and\_c,'' ``\_or\_c,'' ``\_then\_c,''
``loc,'' ``mwe,'' ``subord''\};
for \PAS, $L_d = L$;
and for \PCEDT, $L_d=$\{``DPHR,'' ``INTF,''  ``VOCAT''\}.}
%We found this contraint improved the $F_1$-score for the
%\PAS\ formalism only.

Consider the fully dense graph of all edges between all words predicted
as not singletons by the singleton classifier \S\ref{subsec:arc-factored-sdp:singleton-model} (in all
directions with all possible labels). Unlike \logitedge, the label set $L$ does
not include an explicit \textsc{NoEdge} label.
If $\bm\psi$ denotes the model
weights, and $\bm{f}$ denotes the features, then an edge from $i$ to $j$ with
label $\ell$ in the dense graph has a weight $c(i,j,\ell)$ assigned to it using the linear
scoring function $c(i,j,\ell) = \bm\psi \cdot \bm{f}(x,i,j,\ell)$.

\noindent
\textbf{Decoding:} For each node and each label $\ell$, if $\ell \in L_d$, the
decoder adds the highest scoring outgoing edge, if its weight is positive.
For $\ell \not\in L_d$, every outgoing edge with positive weight is added.
This procedure is guaranteed to find the highest scoring subgraph (largest sum
of edge weights) of the dense graph subject to the determinism constraints.
Its runtime is $O(n^2)$.



The model weights are trained using the
\term{structured hinge loss}~\citep{tsochantaridis_support_2004}.
If $x$ is a sentence and $y$ is a graph over that sentence, let the features 
be denoted $\bm{f}(x,y) = \sum_{(i,j,\ell) \in y}
\bm{f}(x, i,j,\ell)$.
The structured hinge loss for each training example $(x_i, y_i)$ is:
\begin{align}
-\bm\psi^\top \bm{f}(x_i,y_i) + \max_{y}{\{\bm\psi^\top \bm{f}(x_i,y) +
\mathit{cost}(y,y_i) \}}
\end{align}
where $\mathit{cost}(y,y_i) = \alpha |y\setminus y_i| +
\beta |y_i\setminus y|$.
$\alpha$ and $\beta$ trade off between precision and recall for the
edges \citep{gimpel_softmax-margin_2010}.
The loss is minimized with AdaGrad
using early-stopping on a development set. % Tops are predicted using the
%top prediction model \S\ref{s:top_model}.


\subsubsection{Edge Features}
\label{s:edgefeatures}

\label{s:features}

Table~\ref{table:edgefeatures} describes the features we used for predicting
edges.
These features were computed over an edge $e$ with parent token $s$ at
index $i$ and child token $t$ at index $j$. 
Unless otherwise stated, each feature template listed has an indicator
feature that fires for each value it can take on.  For the submitted results,
\logitedge~uses all features except Dependency Path v2, POS Path, and Distance
Thresholds, and \svmedge~uses all features except Dependency
Path v1.  This was due to \svmedge~being faster to
train than \logitedge~when including POS Path features, and due to time constraints for the submission we were unable to retrain
\logitedge~with these features.

% \renewcommand{\floatpagefraction}{0.8}
\begin{table}
\ra{1.2}
% 	\center
% 	\begin{small}
	
	\begin{tabulary}{\columnwidth}{@{}p{\textwidth}@{}}
		\toprule
%\textbf{Bias:} Always fires.

\textbf{Tokens:} 
The tokens $s$ and $t$ themselves. \\

\textbf{Lemmas:} 
Lemmas of $s$ and $t$. \\

\textbf{POS tags:} 
Part of speech tags of $s$ and $t$. \\
% 
\textbf{Linear Order:} 
Fires if $i < j$. \\

\textbf{Linear Distance:} 
$i - j$. \\

\textbf{Dependency Path v1 (\logitedge~only):} 
The concatenation of all POS
tags, arc labels and up/down directions on the path in the syntactic dependency
tree from $s$ to $t$.  Conjoined with $s$, with $t$, and without either. \\

\textbf{Dependency Path v2 (\svmedge~only):} 
Same as Dependency Path v1, but with the lemma of $s$ or $t$ instead of the
word, and substituting the token for any ``IN'' POS tag. \\

\textbf{Up/Down Dependency Path:} 
	The sequence of upward and
	downward moves needed to get from $s$ to $t$ in the syntactic dependency
	tree. \\

\textbf{Up/Down/Left/Right Dependency Path:} 
	The unlabeled path through
	the syntactic dependency tree from $s$ to $t$, annotated with whether each
	step through the tree was up or down, and whether it was to the right or left
	in the sentence. \\

\textbf{Is Parent:} 
	Fires if  $s$ is the parent of $t$ in the syntactic
	dependency parse. \\

\textbf{Dependency Path Length:} 
	Distance between $s$ and $t$ in the syntactic dependency parse. \\

\textbf{POS Context:} 
	Concatenated POS tags of tokens at $i-1$, $i$,
	$i+1$, $j-1$, $j$, and $j+1$. Concatenated POS tags of tokens at $i-1, i, j-1$,
	and $j$. Concatenated POS tags of tokens at $i, i+1, j$, and $j+1$. \\

\textbf{Subcategorization Sequence:} 
	The sequence of dependency arc labels out of $s$, ordered by the index of the
	child.
	Distinguish left children from right children.
	If $t$ is a direct child of $s$, distinguish its arc label with a ``+''.
	Conjoin this sequence with the POS tag of $s$. \\

\textbf{Subcategorization Sequence with POS:} 
	As above, but add the
	POS tag of each child to its arc label. \\

\textbf{POS Path (\svmedge~only):} 
	Concatenated POS tags between and including $i$ and $j$.
	Conjoined with head lemma, with dependent lemma, and without either. \\


\textbf{Distance Thresholds (\svmedge~only):} 
	Fires for every integer between $1$ and $\lfloor \log(|i-j|+1)/\log(1.39)
\rfloor$ inclusive. \\
% e.g.~for $|i-j|=1..9$, up through $\{2,3,4,4,5,5,6,6,6,\ldots\}$.
\bottomrule

\end{tabulary}
% \end{small}
\caption{Features used in edge prediction}
\label{table:edgefeatures}
\end{table}




\thesisonly{
\subsubsection{Feature Hashing}

%\bocomment{ALTERNATE VERSION TO SAVE SPACE:
  The biggest memory usage was in the map from
  feature names to integer indices during feature extraction.
  For experimental expedience, we
  implemented multitask feature hashing
  \citep{weinberger_feature_2009}, which hashes feature names to indices, under
  the theory that errors due to collisions tend to cancel.  No
  drop in accuracy was observed.
}
% }
% \sam{I think we can get away with this short version}
% %% 7-10 million features (percepts) before label conjunction:
% % cab:~dbamman/semeval/mar3 % wc -l *.model
% %    7518732 mar3_recsplit.dm.model
% %    7038841 mar3_recsplit.pas.model
% %    9939549 mar3_recsplit.pcedt.model
% %   24497122 total

% \codenote{
% \url{https://github.com/Noahs-ARK/semeval-2014/pull/20}
% Code:
%   See use of flag LRParser.useHashing, esp LRParser::perceptNum() 
%   and Model::coefIdx(): hashed values before and after label conjunction.
% }

% \noindent
% For \logitedge, the biggest memory usage was from
% the hash table that implemented
% a map of feature names to integer indices, used during feature extraction.
% Tens of millions of features (before conjunction) resulted in several gigabytes of memory use.  To facilitate more convenient experimentation,
% we eliminated this by using
% multitask feature hashing
% \citep{weinberger_feature_2009},
% which randomly assigns features' numeric indices with a hash function.\footnote{We also tried replacing the hash table with a Patricia Tree, trying several open-source implementations; but they had worse memory usage than a GNU Trove hash table. 
%   \codenote{\url{https://github.com/brendano/myutil/blob/0a5697a7f066c265a75ce1b7bd527feafa683c8a/src/vocabalts/vocabalts_results.txt}}}
% Let $F$ be the space of all feature names (before conjunction with the output label).
% In the usual approach, a hash table is maintained containing a
% one-one onto map from features to integer indices $g: F \rightarrow \{1,\ldots,|F|\}$,
% and each output label has an array of model parameters $\bm\phi^\ell \in \mathbb{R}^{|F|}$,
% which $g(f)$ indices into.
% In the hashing approach, a hash function $h_{\ell}$
% maps feature name to one of $B$ bucket values,
% $h_{\ell} : F \rightarrow \{1, \ldots, B\}$.
% (A different hash function is used for each output label.)
% The model parameters are stored in an array $h_\ell(\bm\phi^\ell) \in \mathbb{R}^{B}$,
% for which an entry is defined as
% \[
%   h_\ell(\bm\phi^\ell)_{b} = 
%   \sum_{\substack{\text{feat} \in F 
%     \\\text{ s.t. } h_\ell(\text{feat})=b}}
%   {\phi^\ell_{\text{feat}}}
% \]
% This is easy to implement:
% array lookups and gradient steps are simply implemented with $h$ instead of $g$, and feature vectors are also stored in a hashed space.
% \citep{weinberger_feature_2009} show that, even with some collisions,
% dot products are approximately preserved with high probability;
% and indeed, we observed no drop in accuracy compared to using an explicit feature map.
% We used a value of $B$ as large as was convenient for memory usage (due only to $h(\phi)$), $B=10^{8}$.
% This is actually higher than $|F|\approx 10^7$, though plenty of collisions still happen: about 9.5\% of features are hashed to a value shared by at least one other feature.

% %% simulation:
% % num buckets count 0: 90484843
% % num buckets count 1: 9046266
% % num collisions: num features that are in a non-singleton bucket: 953734
% %%  average over simulations for last value: 9.518e+05 


\subsection{Top Prediction} \label{s:top_model}


We trained a separate token-level binary logistic regression model to classify
whether a token's node had the ``top'' attribute or not.
At decoding time, all predicted predicates (i.e., nodes where there is at least one outbound edge)
are possible candidates to be ``top'';
the classifier probabilities are evaluated, and the highest-scoring node is
chosen to be ``top.''
This is suboptimal, since some graphs have multiple tops (in \PCEDT\ this is
more common);
but selection rules based on probability thresholds gave worse $F_1$
performance on the dev set.
For a given token $t$ at index $i$, the top classifier's features
included $t$'s POS tag, $i$, those two conjoined, and the depth
of $t$ in the syntactic dependency tree.


\section{Experiments and Results}
\label{s:evaluation}

We participated in the Open Track, and used the syntactic dependency parses supplied by the organizers.  Feature engineering was performed on a development set (\S 20), training on \S\S 00--19.
We evaluate labeled precision (\LP), labeled recall (\LR), labeled $F_1$ (\LF),
and labeled whole-sentence match (\LM) on the held-out test data using the
evaluation script provided by the organizers. 
\LF\ was averaged over the formalisms to determine the winning system.
Table~\ref{table:perf} shows a detailed breakdown of our scores.
In Table~\ref{table:arc-factored-vs-turbo}, we compare our labeled $F_1$
to that of the winning system, due to~\citet{martins2014sdp}.
The winning system took a graph-based approach similar to ours.
Their main improvement over our system was a set of \emph{second-order}
features, used to score adjacent arcs,
with inference via \adcubed~\citep{Martins2011DualDW}.
They resubmitted a similar system to the 2015 version of the shared task.
In the next chapter, we build off of their 2015 model.

\sam{have room now to talk about this more.}

% CMU:
% DM:
% LP: 0.844641
% LR: 0.834849
% LF: 0.839716
% LM: 0.087537
% 
% 
% PAS:
% LP: 0.907832
% LR: 0.885141
% LF: 0.896343
% LM: 0.260386
% 
% 
% PCEDT:
% LP: 0.768139
% LR: 0.707173
% LF: 0.736396
% LM: 0.071217

% Priberam:
% DM:
% LP: 0.902322
% LR: 0.881050
% LF: 0.891559
% LM: 0.268546
% 
% PAS:
% LP: 0.925576
% LR: 0.909676
% LF: 0.917557
% LM: 0.378338
% 
% PCEDT:
% LP: 0.801397
% LR: 0.757900
% LF: 0.779042
% LM: 0.106825

\begin{table}
\centering
\begin{tabulary}{\textwidth}{@{\extracolsep{\fill}}r c c c c@{}}
\toprule
& \textbf{\LP} & \textbf{\LR} & \textbf{\LF} & \textbf{\LM} \\

\midrule

\textbf{\DM}
& 0.8446 & 0.8348 & 0.8397 & 0.0875 \\
\textbf{\PAS}
& 0.9078 & 0.8851 & 0.8963 & 0.2604 \\
\textbf{\PCEDT}
& 0.7681 & 0.7072 & 0.7364 & 0.0712 \\

\midrule

\textbf{Average}
& 0.8402 & 0.8090 & 0.8241 & 0.1397 \\

\bottomrule
\end{tabulary}
\caption{Labeled precision (\LP), recall (\LR), $F_1$ (\LF), and
whole-sentence match (\LM) on the held-out (in-domain) test data.
}
\label{table:perf}
\end{table}


\begin{table}
\centering
\begin{tabulary}{\textwidth}{@{\extracolsep{\fill}}r c c c c@{}}
\toprule
& \textbf{\DM} & \textbf{\PAS} & \textbf{\PCEDT} & \textbf{Avg.} \\ 

\midrule

{\bf CMU (ours)}  & 0.8397 & 0.8963 & 0.7364 & 0.8241 \\
{\bf M\&A} & {\bf 0.8916} & {\bf 0.9176} & {\bf 0.7790} & {\bf 0.8627} \\

\bottomrule
\end{tabulary}
\caption{Labeled $F_1$ on the held-out (in-domain) test data.
}
\label{table:arc-factored-vs-turbo}
\end{table}


\thesisonly{
\include{arc-factored-sdp/negative-results}
}

\section{Conclusion}
We found that feature-rich discriminative models perform well at the task of
mapping from sentences to semantic dependency parses.
\thesisonly{
We also noted additional features and constraints which did not
appear to help (contrary to expectation).
}
There are a number of clear extensions to this work that could potentially improve
performance.
While an edge-factored model allows for efficient inference, there is
much to be gained from \textbf{higher-order features}
\citep{mcdonald_online_2006,martins_turning_2013}.
The amount of information
shared between the three formalisms suggests that a \textbf{multitask learning}
\citep{evgeniou_regularized_2004} framework could lead to gains.
In fact, in the following chapter we introduce an improved model that explores both
of these ideas, additionally replacing hand-engineered features with recurrent neural factors.
Also, there is structure in the formalisms which we did not exploit 
(such as the deterministic processes by which an original PCEDT tree annotation was converted into a graph);
formulating more subtle \textbf{graph constraints} to capture this a priori
knowledge could lead to improved performance.

