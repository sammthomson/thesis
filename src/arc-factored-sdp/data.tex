\section{Broad-Coverage Semantic Dependency Parsing (SDP)}
\label{sec:task}

\begin{table}[tb]
\centering
		\begin{tabulary}{\columnwidth}{@{}l RR c RR c RR}
			\toprule
			& \multicolumn{2}{c}{\bf{DM}}
			& \phantom{ }
			& \multicolumn{2}{c}{\bf{PAS}}
			& \phantom{ }
			& \multicolumn{2}{c}{\bf{PSD}}\\
			\cmidrule{2-3}
			\cmidrule{5-6}
			\cmidrule{8-9}
			& id & ood
			& & id & ood
			& & id & ood  \\
			\midrule
		 	\# labels & 59 & 47 && 42 & 41 && 91 & 74\\
			 \% trees & 2.3 & 9.7 && 1.2 & 2.4 && 42.2 & 51.4 \\
			 \% projective & 2.9 & 8.8 && 1.6 & 3.5 && 41.9 & 54.4 \\
			\bottomrule
		\end{tabulary}
	\caption{Graph statistics for in-domain (WSJ, ``id'') and
          out-of-domain (Brown corpus, ``ood'') data. Numbers taken from \citet{oepen2015sdp}.} 
	%\vspace{-.5cm}
	\label{tab:data} 
% 	\vspace{-.25cm}
\end{table}



First defined in a SemEval 2014 shared task \citep{oepen2014sdp}, and then
extended by \citet{oepen2015sdp}, the broad-coverage semantic dependency parsing (\term{SDP})
task is centered around three semantic formalisms whose annotations have been
converted into bilexical dependencies.
Broad-coverage semantic dependency parsing aims to provide a
shallow semantic analysis of text not limited to a specific application domain.
As distinct from deeper semantic analysis (e.g., parsing to a full
lambda-calculus logical form), shallow semantic parsing captures relationships
between pairs of words or concepts in a sentence, and has wide application for
information extraction, knowledge base population, and question answering (among others).
See Figure~\ref{fig:formalisms} for an example.
The formalisms come from varied linguistic traditions, but all three
aim to capture predicate-argument relations between content-bearing
words in a sentence.


While at first glance similar to syntactic dependencies, semantic
dependencies have distinct goals and characteristics, more akin to semantic
role labeling \citep[SRL;][]{gildea2002srl} or the abstract meaning
representation \citep[AMR;][]{banarescu_abstract_2013}.
% , or lambda-calculus logical forms \stcomment{cite}.
They abstract
over different syntactic realizations of the same or similar meaning
(e.g., \textit{``She gave me the ball.''} vs. \textit{``She gave the ball to
me.''}).
Conversely, they attempt to distinguish between different senses even when
realized in similar syntactic forms (e.g., \textit{``I baked in the kitchen.''}
vs. \textit{``I baked in the sun.''}).

Structurally, they are labeled directed graphs whose vertices are tokens in the
sentence.
This is in contrast to AMR whose vertices are abstract concepts, with no
explicit alignment to tokens, which makes parsing more
difficult~\citep{flanigan2014amr}.
Their arc labels encode broadly-applicable semantic relations rather
than being tailored to any specific downstream application or ontology.\footnote{%
This may make another disambiguation step necessary to use these
representations in a downstream task, but there is evidence that modeling
semantic composition separately from grounding in any
ontology is an effective way to achieve broad
coverage~\citep{Kwiatkowski2013ScalingSP}.
}
They are not necessarily trees, because a token may be an argument of more than
one predicate (e.g.,  in \textit{``John wants to eat,''} John is both the wanter and the would-be eater).
Their analyses may optionally leave out non--content-bearing tokens, such as
punctuation or the infinitival \textit{``to,''} or prepositions that simply
mark the type of relation holding between other words.
But when restricted to content-bearing tokens (including adjectives, adverbs,
etc.), the subgraph is connected.
In this sense, SDP provides a \emph{whole-sentence} analysis.
This is in contrast to PropBank-style SRL, which gives an analysis of
only verbal and nominal predicates \citep{palmer2005proposition}.
Semantic dependency graphs also tend to have higher levels of non-projectivity
than syntactic trees \citep{oepen2014sdp}.
Sentences with graphs containing cycles have been removed from the dataset by
the organizers, so all remaining graphs are directed acyclic graphs.
The three formalisms %come from very different linguistic theories, but all
% are all represented as labeled directed graphs, with words as vertices, and
also have ``top'' annotations, corresponding roughly to the
semantic focus of the sentence.  
A ``top'' need not be a root of the graph.
Because they have the same format and follow roughly the same constraints, this
allows us to use the same machinery (\S\ref{s:models}) for
training and testing statistical models for the three formalisms.
We will see in the next chapter that it also makes the tasks amenable to multitask learning.
Table \ref{tab:data} summarizes some of the dataset's high-level statistics.


\paragraph{Formalisms.}
Following the SemEval shared tasks, we consider three formalisms.
The \term{DM} (DELPH-IN MRS) representation comes from DeepBank \citep{flickinger_deepbank_2012}, which
are manually-reranked parses from the LinGO English Resource Grammar
\citep{copestake_erg_2000}.
LinGO is a head-driven phrase structure
grammar~\citep[HPSG;][]{pollard_hpsg_94} whose syntactic derivations simultaneously produce MRS graphs.
The \term{PAS} (Predicate-Argument Structures) representation is extracted from the Enju Treebank, which
consists of automatic parses from the Enju HPSG parser \citep{miyao_linguistic_2006}.
PAS annotations are also available for the Penn Chinese Treebank
\citep{xue2005ctb}.
The \term{PSD} (Prague Semantic Dependencies) representation is extracted from the tectogrammatical layer
of the Prague Czech-English Dependency Treebank \citep{hajic2012psd}.
PSD annotations are also available for a Czech translation of the WSJ Corpus.
In this work, we train and evaluate only on English annotations.
Of the three, PAS follows syntax most closely, and prior work has found it the easiest to predict.
PSD has the largest set of labels, and parsers have significantly lower performance on it
\citep{oepen2015sdp}.

% \sam{Stats about \% multiple roots, \% multiple tops, \% tree, \% acyclic}



% 35,657 annotated sentences from the WSJ Corpus are
% provided for training, with 1,410 sentences from WSJ Section 21 providing an
% in-domain (\term{id}) test set, and 1,849  sentences from the Brown Corpus as
% an out-of-domain (\term{ood}) test set.
