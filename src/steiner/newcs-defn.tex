\section{The Node-and-Edge-weighted Connected Subgraph (\NEWCS) Problem}
\label{sec:newcs_defn}

% \subsection{Definition and Complexity}
\label{subsec:steiner:newcs:defn}

Here we define a novel extension of the
\PCST\ problem in which node weights are not required to be positive and
edge weights are not required to be negative.
Let $G = (V_G, E_G)$ be an undirected graph with real-valued node weights,
$w(v) \in \R, \forall v \in V_G$, and real-valued edge weights, $w(e) \in \R, 
\forall e \in E_G$.
The \term{node-and-edge-weighted connected subgraph (\NEWCS)} problem is to find a
connected, but not necessarily spanning subgraph $H = (V_H, E_H) \subseteq G$
of maximal weight:
\begin{equation}
\label{eq:newcs_obj}
\NEWCS(G, w) =
\argmax{\substack{H \subseteq G,\\H \text{ connected}}}{\left\{{
  \sum_{v \in V_H}{w(v)} + \sum_{e \in E_H}{w(e)}
}\right\}}.
\end{equation}
In the remainder of the chapter, we will assume $G = (V_G, E_G)$ is the input
graph, and $H = (V_H, E_H) \subseteq G$ is a connected subgraph of $G$ of
maximal weight.
We will refer to the number of nodes in $G$ as $n$.\footnote{
For dependency parsing, $n$ coincides with the number of tokens in the
sentence, but for joint semantic parsing $n$ is usually larger.}
Equation~\ref{eq:newcs_obj} is exactly the same as Equation~\ref{eq:pcst_obj};
we have only removed the conditions on the weight function $w$.
This allows us to state and prove the following trivial theorem:

\vspace{.2cm}
\begin{thm}
\label{thm:steiner:newcs-np-hard}
The \NEWCS\ problem is NP-hard.
\end{thm}
\begin{proof}
\NEWCS\ is a relaxation of \PCST, so every \PCST\ problem is a \NEWCS\
problem.
Since \PCST\ can be reduced to \NEWCS, \NEWCS\ is also NP-hard.
\end{proof}

We can situate this with respect to other results in graph-based
parsing to give a more complete picture of available
techniques and their complexities.
MSTParser \citep{mcdonald2005online} was originally a first-order non-projective
dependency parsing model (i.e. each arc is scored independently) with $O(n^2)$
inference via the Chu-Liu-Edmonds algorithm~%
\citep[CLE;][]{chu_shortest_1965,edmonds_optimum_1967}.
% \footnote{
% $n$ is the number of nodes in the input graph.
% For dependency parsing, this number coincides with the number of tokens in the
% sentence.}
Second-order MST parsing (i.e. motifs of two adjacent arcs can be scored together)
was subsequently shown to be NP-complete~\citep{mcdonald_online_2006}.
This led researchers to explore approximate inference techniques for fast
higher-order non-projective parsing~\interalia{koo2010dual,martins_turning_2013}.
JAMR~\citep{flanigan2014amr} extended MST parsing techniques from arborescences to (non-tree)
semantic graphs, with an arc-factored model and $O(n^2 \log n)$ inference via \MSCG.
Theorem~\ref{thm:steiner:newcs-np-hard} shows that the spanning constraint
was computationally critical;
removing it results in an NP-hard problem, even without higher-order factors.
This result, along with preliminary experiments, leads us to believe that
approximate inference is required if graph-based methods
are to be practical for joint node and edge prediction.
We propose one such method in \S\ref{sec:steiner:beasley}.
First, as a reasonable baseline, we give a formulation of the problem as an
integer linear program (ILP), which can be solved either exactly or
approximately by off-the-shelf ILP solvers like Gurobi\footnote{
\url{http://www.gurobi.com/}} or CPLEX.\footnote{%
\url{https://www.ibm.com/analytics/data-science/prescriptive-analytics/cplex-optimizer}
}
\todo{
Something about projective parsing, Natalie Schluter's DAG algorithm, complexity results?
}
\todo{
Something about the page-2 SDP papers?
}
