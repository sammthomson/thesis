\section{An Approximate Algorithm}
\label{sec:steiner:beasley}

In preliminary AMR parsing experiments, we found ILP solving using Gurobi
to be prohibitively slow, with exact solutions sometimes taking hours,
and even approximate solutions taking multiple minutes.
Inspired by Beasley's Steiner tree solving algorithm  
\citep{beasley_sst_1989}, along with similar successful approaches in
syntactic dependency parsing~\citep{koo2010dual,rush_improved_2012,martins_turning_2013}, we investigated combining a discrete graph algorithm with Lagrangian relaxation.
Here we introduce a novel approximate algorithm, \term{\BeasGraph},
which extends Beasley's algorithm to handle \NEWCS\ problems (and hence \PCST\ problems as well).

At a high level, Beasley's algorithm modifies the input graph and adds constraints in order
to turn the problem of finding a non-spanning tree into
one of finding a spanning tree subject to constraints.
In \BeasGraph, instead of finding a maximal spanning tree, we find a
maximal spanning connected subgraph.
As noted in \S\ref{subsec:steiner:newcs:defn}, finding spanning
connected subgraphs is computationally easier than finding
(possibly non-spanning) connected subgraphs.
The constraints we add ensure that an optimal \NEWCS\ solution can be
recovered from the solution to the connected spanning subgraph problem.
The constraints can be approximately enforced with Lagrangian
relaxation~\citep{geoffrion_lagrangean_1974,fisher_lagrangian_2004},
or more specialized methods like \adcubed~\citep{Martins2011DualDW},
to get an approximate \NEWCS\ algorithm.
% transform a
% \NEWCS\ problem instance into an instance of \MSCG, plus additional
% constraints.

Formally, let $G = (V_G, E_G)$ and $w: (V_G \cup E_G) \rightarrow \R$ be
a \NEWCS\ problem instance.
We modify $G$ by introducing a \dummyroot\ node: $V_{G^\prime}=V_G \cup \{\dummyroot\}$.
We also introduce two labeled edges between \dummyroot\ and every other node $v$,
one with label \selectlabel\ and weight $0$,\footnote{
Similarly to \S\ref{sec:steiner:ilp}, when using this solver as
part of a semantic parser, a ``top'' model score could be used as a
weight here.
This jointly solves ``top'' prediction along with concept and edge prediction,
with the added benefit of breaking symmetry.
} and one with label \omitlabel\ and weight $-w(v)$:
$E_{G^\prime} =
  E_G \cup \bigcup_{v \in V_G}{ \{
    \ledge{\dummyroot}{v}{\selectlabel},
    \ledge{\dummyroot}{v}{\omitlabel}
  \} }
%   \{ \ledge{\dummyroot}{v}{\selectlabel} \mid v \in V_G \} \cup
%   \{ \ledge{\dummyroot}{v}{\omitlabel} \mid v \in V_G \}
$.
Intuitively, an \omitlabel\ edge will indicate that its adjacent node does
\emph{not} appear in the solution.
% The connected component that \selectlabel\ points to will be the solution to
% the \NEWCS\ problem.

We will use a binary indicator variable $z_e$ for each edge $e$, as before,
to express constraints.
We constrain that at most one \selectlabel\ edge can be chosen:
\begin{equation}
\label{eqn:steiner:beasley:at-most-one-select}
\sum_{v \in V_G} { z_{\ledge{\dummyroot}{v}{\selectlabel}}} \le 1.
\end{equation}
% This corresponds to the Steiner constraint that the solution must be
% connected (i.e. have at most one connected component).
% We introduce edges between \dummyroot\ and every
% other node $v$, with label \omitlabel\ and weight $-w(v)$.
And for a given node $v$, we require that its \omitlabel\ edge and its
\selectlabel\ edge cannot both be included:
\begin{equation}
\label{eqn:steiner:beasley:omit-no-select}
z_{\ledge{\dummyroot}{v}{\omitlabel}} + z_{\ledge{\dummyroot}{v}{\selectlabel}} \le 1,
\forall v \in V_G.
\end{equation}
Lastly, if a node $v$'s \omitlabel\ edge is included, then no other edge adjacent to $v$ may
be included:
\begin{equation}
\label{eqn:steiner:beasley:omit-no-adjacent}
z_{\ledge{\dummyroot}{v}{\omitlabel}} + z_{\edge{v}{u}} \le 1, \forall u, v \in V_G.
\end{equation}
All of these constraints
(Equations~\ref{eqn:steiner:beasley:at-most-one-select}--\ref{eqn:steiner:beasley:omit-no-adjacent})
can be organized into a single system of linear inequalities:
\begin{equation}
\label{eqn:steiner:beasley:all-constraints}
A \tensor{z} \leq \tensor{1}.
\end{equation}

Now if we can find a maximum spanning connected subgraph
$H^\prime \subseteq G^\prime$ satisfying Equation~\ref{eqn:steiner:beasley:all-constraints},
we can recover a solution $H \subseteq H^\prime$ to the \NEWCS\ problem.
Call a node $v$ \term{omitted} in $H^\prime$ if $\ledge{\dummyroot}{v}{\omitlabel} \in E_{H^\prime}$.
Likewise, call a node $v$ a \term{top} in $H^\prime$ if $\ledge{\dummyroot}{v}{\selectlabel} \in E_{H^\prime}$.
Let %$H=(V_H, E_H)$, where
$V_H = \{v \in V_G \mid v \text{ not omitted in } H^\prime\}$ and
$E_H = E_{H^\prime} \cap E_G$.
In other words, to get $H$ we discard the \dummyroot\ node,
discard the \omitlabel\ and \selectlabel\ edges, discard all omitted nodes
from $H^\prime$, and keep everything else.

\vspace{.2cm}
\begin{lemma}
\label{lemma:steiner:beasley:h-connected}
$H$ is connected.
\end{lemma}
\begin{proof}
If $V_H$ is empty, then %every node is omitted, and by Equation~\ref{eqn:steiner:beasley:omit-no-adjacent}
% $E_H$ is also empty, so
the lemma is vacuously true.

When $V_H$ is nonempty, we will show every node in $V_H$ is connected to a single
top node $t \in V_H$, and hence to each other:
Let $v \in V_H$ be any non-omitted node.
$H^\prime$ is connected and spanning, so $v$
must have a simple path in $H^\prime$ to \dummyroot.
The path $p$ from $v$ to \dummyroot\ cannot go through an
\omitlabel\ edge, because omitted nodes have
degree 1 (by Equation~\ref{eqn:steiner:beasley:omit-no-adjacent}).
The path therefore must go through a \selectlabel\ edge, as \dummyroot\
has no other kind of adjacent edges.
There is at most one \selectlabel\ edge (by Equation~\ref{eqn:steiner:beasley:at-most-one-select});
let $t$ be the top node in $H^\prime$.
So $p$ must consist of a path from $v$ to $t$, and then a \selectlabel\ edge from $t$ to \dummyroot.
Since $p$ is simple, the path from $v$ to $t$ does not use the \selectlabel\ edge,
so it must use only edges from $E_G$.
Thus there is a path in $H$ from $v$ to $t$.
$t \in V_H$ (by Equation~\ref{eqn:steiner:beasley:omit-no-select}).
Thus every node in $H$ is connected to each other (through $t$).
\end{proof}

\vspace{.2cm}
\begin{lemma}
\label{lemma:steiner:beasley:lemma2}
The node-and-edge-weight of $H$ is equal to the edge-weight of $H^\prime$ plus a constant:
\begin{equation}
\label{eqn:steiner:beasley:lemma2}
w(H) = w(H^\prime) + C,
\end{equation}
where,
\begin{align}
w(H) =
  \sum_{v \in V_H}{w(v)} + \sum_{e \in E_H}{w(e)}, \\
w(H^\prime) =
  \sum_{e \in E_{H^\prime}}{w(e)}, \\
C =
  \sum_{v \in V_G}{w(v)}.
\end{align}
\end{lemma}

\begin{proof}
Recall that \selectlabel\ edges contribute no weight;
\omitlabel\ edges have the negated weight of their adjacent node;
and all original edges keep their original weight.
So,
\begin{align}
w(H^\prime) + C =&
  \sum_{e \in E_H}{w(e)} + \sum_{\ledge{\dummyroot}{v}{\omitlabel} \in E_{H^\prime}}{w(\ledge{\dummyroot}{v}{\omitlabel})} + C \\
  =& \sum_{e \in E_H}{w(e)} + \sum_{v \text{ omitted in } H^\prime}{-w(v)} + C \\
  =& \sum_{e \in E_H}{w(e)} + \sum_{v \text{ not omitted in } H^\prime}{w(v)} \\
  =& w(H)
\end{align}
Note that $C$ is a sum over all nodes in the input graph, and so is
constant with respect to $H^\prime$.
\end{proof}

\vspace{.2cm}
\begin{lemma}
\label{lemma:steiner:beasley:lemma3}
Every connected subgraph $H \subseteq G$ has a corresponding feasible
spanning connected subgraph $H^\prime \subseteq G^\prime$.
\end{lemma}
\begin{proof}
Proof by construction:
If $H$ is empty, let
$E_{H^\prime} = \{ \ledge{\dummyroot}{v}{\omitlabel} \mid v \in V_G \}$.
Otherwise, arbitrarily select $t \in V_H$.
Let
$E_{H^\prime} =
V_H \cup
\{ \ledge{\dummyroot}{t}{\selectlabel} \} \cup
\{ \ledge{\dummyroot}{v}{\omitlabel} \mid v \not\in V_H \}$.
\end{proof}


\vspace{.2cm}
\begin{thm}
\label{thm:steiner:beasley:thm1}
$H$ is a solution to the \NEWCS\ problem.
\end{thm}
\begin{proof}
Lemmas \ref{lemma:steiner:beasley:h-connected} and \ref{lemma:steiner:beasley:lemma3}
together show that there is an invertible transformation from connected
subgraphs of $G$ to spanning connected subgraphs of $G^\prime$ satisfying
Equation~\ref{eqn:steiner:beasley:all-constraints}.
Lemma~\ref{lemma:steiner:beasley:lemma2} shows that their objective values are
equal up to a constant, so the maximizer of one is also the maximizer of the other.
\end{proof}

Theorem~\ref{thm:steiner:beasley:thm1} shows that when the constraints are
exactly enforced, we recover an exact solution.
When a Lagrangian method is used, sometimes the constraints can
be enforced exactly, in which case we can obtain a \term{certificate of optimality},
but sometimes the method fails to converge.
In that case, we resort to a heuristic in order to return a feasible but not
necessarily optimal solution.
At each iteration during Lagrangian relaxation,
\MSCG\ is run on $G^\prime$ with weights that have been
adjusted by Lagrange multipliers, outputting a graph $H^\prime$.
Before convergence, $H^\prime$ may not satisfy
Equation~\ref{eqn:steiner:beasley:all-constraints}.
But a key property of Lagrangian relaxation is that
the Lagrange-adjusted objective gives an upper bound on
the true objective, $w(\NEWCS(G, w))$.
We also use $H^\prime$ to find a feasible solution.
As our heuristic, we use the highest positive-scoring connected
component of $H$ (or the empty graph if all connected components have negative
weight).
\sam{could note/cite other heuristics from PCST literature.}
Note that every feasible solution gives a lower bound on the objective.
These upper and lower bounds could potentially be used to improve an exact
solver in a branch-and-bound framework\todo{cite}.
In our case though, we simply return the highest-scoring feasible solution
over all iterations, along with the lowest upper bound.
% The upper bound divided by the lower bound gives an underestimate of the
% performance ratio of our method.
\BeasGraph\ is summarized in Algorithm~\ref{algo:steiner:beasgraph}.
See Figure~\ref{fig:steiner:beasley} for an illustration of a successful run
of the algorithm.
\begin{algorithm}
  \caption{\BeasGraph}
  \label{algo:steiner:beasgraph}
  \KwData{
    A graph $G=(V_G,E_G)$ with node and edge weights given by $w$. \\
    $K \in \mathbb{N}$, a maximum number of iterations. \\
    $\eta \in \R^{\geq 0}$, a learning rate.
  }
  \KwResult{
    A connected subgraph of $G$, and an upper bound on the weight of any such subgraph.
  }
  $V_{G^\prime} \leftarrow V_G \cup \{\dummyroot\}$ ; \\
  $E_{G^\prime} \leftarrow
  E_G \cup \bigcup_{v \in V_G}{ \{
    \ledge{\dummyroot}{v}{\selectlabel},
    \ledge{\dummyroot}{v}{\omitlabel}
  \} }$ ; \\
  Let $w(\ledge{\dummyroot}{v}{\selectlabel}) = 0$ and $w(\ledge{\dummyroot}{v}{\omitlabel}) = -w(v)$ for all $v \in V_G$.
  Let $\tensor{w}$ be a column vector containing all edge weights. \\
  Let
  $\mathcal{L}(\tensor{z}, \tensor{\lambda}) =
    \tensor{w}^\top \tensor{z} + \tensor{\lambda}^\top (\tensor{1} - A\tensor{z})$ \tcp*{Lagrangian relaxation}
  $\tensor{\lambda} \leftarrow \tensor{0}$ ; \\
  $H^{\text{best}} \leftarrow \emptyset$ \tcp*{best feasible solution}
  $w^{\text{UB}} \leftarrow \sum_{e \in E_{G^\prime}}{\text{max}(0, w(e))}$ \tcp*{lowest upper bound}
  \For{$i$ in $1, \ldots, K$}{
    $\tensor{w^\prime} \leftarrow \tensor{w} - A^\top \tensor{\lambda}$ \tcp*{Lagrange-adjusted weights}
    $H^\prime \leftarrow \MSCG(G^\prime, w^\prime)$ ; $\tensor{z} \leftarrow$ binary indicators of $E_{H^\prime}$ ; \\
    $H \leftarrow$ highest-weighted connected component of $H^\prime \setminus \{ \dummyroot \}$ ; \\
    \If{$w(H) \geq w(H^{\text{best}})$} {
      $H^{\text{best}} \leftarrow H$ ;
    }
    \If{$\mathcal{L}(\tensor{z}, \tensor{\lambda}) \leq w^{\text{UB}}$} {
      $w^{\text{UB}} \leftarrow \mathcal{L}(\tensor{z}, \tensor{\lambda})$ ;
    }
    \If{$w(H^{\text{best}}) = w^{\text{UB}}$} {
      \Return{$H^{\text{best}}$, $w^{\text{UB}}$} \tcp*{$H^{\text{best}}$ is optimal.}
    }
    \tcc{Take a projected gradient step toward minimizing $\mathcal{L}$.}
    $\tensor{\lambda} \leftarrow
        \tensor{\lambda} -
          \eta 
          \nabla %_{\tensor{\lambda}}
%           \frac{\partial}{\partial \tensor{\lambda}}
          \mathcal{L}(\tensor{z}, \tensor{\lambda})
    $ ; \\ %\tcp*{gradient step}
    $\tensor{\lambda} \leftarrow \text{max} \left(\tensor{0}, \tensor{\lambda} \right)$ ; \\ %\tcp*{projection}
  }
  \Return{$H^{\text{best}}$, $w^{\text{UB}}$}  \tcp*{$H^{\text{best}}$ is non-optimal.}
\end{algorithm}
\input{fig/steiner/beasley-fig}

\BeasGraph\ is particularly well suited to graph-based semantic parsing,
because additional linguistically motivated constraints can be easily added
the set of constraints enforced by Lagrangian relaxation.
In fact, inference in JAMR~\citep{flanigan2014amr,flanigan_cmu_2016} is
already done via \MSCG\ plus Lagrangian relaxation, so the extension to 
joint concept and relation identification using \BeasGraph\ is
straight-forward.
In the following section, we introduce methods that work for \NEWCS\ (and
hence \PCST) solving, and so are of interest to the algorithms community.
Note though that they may not interact well with additional task-specific
constraints, and so may require more care if they are to be used in NLP.
