\section{Reducing the Problem Size with Tests}
\label{sec:steiner:tests}

As with all NP-hard problems, the runtime complexity of \NEWCS\
solving is highly dependent on the size of the input.
Memory can be an issue also;
for very large \PCST\ instances, ILP solvers will often run out of available memory
before returning a solution~\citep{ljubic-06}.
A common approach in \PCST\ solving is to run a set of heuristic reduction
\term{tests} to reduce the size of the input graph before sending it to a
solver~\citep{duin_reduction_1989}.
A reduction \emph{test} is a solution-preserving invertible transformation of
the input graph:
an optimal solution for the transformed graph can be ``untransformed'' to give an
optimal solution for the original graph.
Whereas \BeasGraph\ transformed the problem into a different problem,
tests transform a problem instance into a smaller instance of the same problem.
Because of this, tests can be composed and run in sequence;
in practice they are tried repeatedly until all tests fail to reduce the
problem size.

Our main contribution is a set of tests based on
\term{Steinerpaths}~\citep{duin-87}.
We expand the scope and applicability of Steinerpaths, and propose practical
methods for finding them~(\S\ref{subsubsec:steiner:steinerpaths}).
\thesisonly{
We also catalog some tests from prior work on \PCST\ solving, noting
which can be easily adapted to \NEWCS\ solving~(\S\ref{subsubsec:steiner:prior-tests}).
}




\subsection{Steinerpaths}
\label{subsubsec:steiner:steinerpaths}

\cite{duin-87} introduced the useful idea of a \term{Steinerpath}.
We present here a simpler, slightly generalized definition,
% \footnote{
% Our definition corresponds to a \emph{chain} of Steinerpaths under
% the original definition.
% Since the useful property of Steinerpaths is that their
% source implies their destination, and implication is transitive,
% this simpler definition is functionally equivalent to the original.
% }
using our own notation and without any assumptions on the sign of weights.
% \footnote{
% \sam{this footnote is wrong. there are node weights.}
% They are looking at the less general \term{Steiner problem in graphs}, where
% there are no node weights, and a set $K \subseteq V_G$ is \emph{required} to be included in the
% solution.
% They require Steinerpaths to eminate from $K$, but we see no need for such a
% requirement.
% They present an algorithm which finds only some Steinerpaths out of $K$;
% it is not exhaustive.
% }.
% \subsubsection{Convert to Directed Graph}
Given an undirected input graph $G = (V_G, E_G)$ and node and edge weights
$w: (V_G \cup E_G) \rightarrow \R$, first let us construct a new
directed graph $G^* = (V_G, A_{G^*})$, where
\begin{equation}
A_{G^*} = %\{ \arc{u}{v} \mid \edge{u}{v} \in E_G \} \cup
%\{ \arc{v}{u} \mid \edge{u}{v} \in E_G \}
  \bigcup_{\edge{u}{v} \in E_G}{
    \{ \arc{u}{v}, \arc{v}{u} \}
  },
\end{equation}.
$G^*$ has an arc going in both directions for each original edge.
Define the weight of an arc to be the weight of its destination plus
the weight of its original undirected edge: 
\begin{equation}
w(\arc{u}{v}) = w(v) + w(\edge{u}{v}).
\end{equation}
These weights can be thought of as the value of adding an arc
when its source is already part of the solution but its destination is not.
A \term{Steinerpath} is a
simple path
$p = \arcpath{
  \arc{v_o}{v_1},
  \arc{v_1}{v_2},
  \ldots,
  \arc{v_{m-1}}{v_m}
}$ in $G^*$
such that every \term{terminal subpath} (or \term{suffix}) of $p$ has
nonnegative weight:
\begin{equation}
\forall \ell \in \{ 0,\ldots,m-1\}:
\sum_{i=\ell}^{m-1} {w(\arc{v_i}{v_{i+1}})} \geq 0.
\end{equation}
A terminal subpath of $p$ is a path with the same endpoint as $p$ that uses
only arcs from $p$.
It follows immediately from the definition that every suffix of a Steinerpath
is also a Steinerpath.
The following lemma captures why Steinerpaths are of interest.

\vspace{.2cm}
\begin{lemma}
\label{lemma:steinerpath}
If there is a Steinerpath from $v_0$ to $v_m$ in $G^*$, and
there is an optimal \NEWCS\ solution containing $v_0$, then there is an optimal
solution containing both $v_0$ and $v_m$.
\end{lemma}

\begin{proof}
Lemma~\ref{lemma:steinerpath} is proven for \PCST\ in \cite{duin-87} under a
slightly different definition of Steinerpath.
Our definition corresponds to a \emph{chain} of Steinerpaths under
their original definition.
The proof under our definition follows from theirs, and the fact that
implication is transitive.

The intuition behind the proof is that if an optimal solution $H$ already
contains some non-empty subset of
nodes in the path $\{v_{i_1}, \ldots, v_{i_\ell}\}$, with
$i_1 < \ldots < i_\ell$, then $H$ could be improved by adding the
remainder of the Steinerpath
$\arcpath{
  \arc{v_{i_\ell}}{v_{i_\ell+1}},
  \ldots,
  \arc{v_{m-1}}{v_m}
}$.
\end{proof}
Another way to state Lemma~\ref{lemma:steinerpath} in terms of indicator
variables is that, if we add the constraint $z_{v_0} \le z_{v_m}$, then
our (new, smaller) feasible set still contains an optimal solution.
If we can find another Steinerpath in the opposite direction,
from $v_m$ to $v_0$, then we can equate
the two variables, strictly reducing the size of the problem.

Steinerpaths were originally introduced in the context of a slightly different
variant of the Steiner tree problem, in which a set of nodes $K \subseteq V_G$ is
\emph{required} to be included in the solution.
Steinerpaths were required to emanate from $K$, but we see no need for such a
requirement.
One of our contributions is recognizing that Steinerpaths between any
two nodes can be used to reduce the size of a \NEWCS\ problem.


\subsubsection{Finding Steinerpaths}
\label{subsubsec:steiner:finding-steinerpaths}

Steinerpaths starting from a fixed set of nodes were originally found using
a variant of Dijkstra's algorithm~\citep{duin-87}.
Since we wish to discover Steinerpaths regardless of their source, we instead
propose a cubic-time all-pairs Steinerpaths algorithm~(Algorithm~\ref{alg:all_pairs_steinerpath})
based on the Floyd-Warshall all-pairs shortest path
algorithm~\citep{roy_transitivite_1959,floyd_algorithm_1962,warshall-62}:
% We present a novel algorithm (Algorithm~\ref{alg:all_pairs_steinerpath}) for
% finding some (but not all) Steinerpaths between all pairs of nodes in $V_G$ in $O(n^3)$ time.
\newcommand{\shortpath}{p}
\newcommand{\shortdist}{d}
\begin{algorithm}
  \caption{All-Pairs Steinerpaths}
  \label{alg:all_pairs_steinerpath}
  \KwData{A graph $G$ with node and edge weights given by $w$.}
  \KwResult{A set of Steinerpaths in $G$.}
  Let $G^*$ be the edge-weighted directed graph defined from $G$ as above. \\
  Let $w_{\leq 0}$ be a new arc weight function, given by $w_{\leq 0}(a) = -\min(w(a), 0)$. \\
%   First, only look at nonpositive edges in $A_{G^*}$ (where the cost of the edge is
%   bigger than the weight of the destination). \\
  Run Floyd-Warshall on $G^*$ with arc distances given by $w_{\leq 0}$ to get,
  for all pairs $s, t$, the shortest path from $s$ to $t$.
  Call this shortest path $\shortpath_{s,t}$, and its distance $\shortdist_{s,t}$. \\
  \For{$s \in V_G$}{
    \For{$t \in V_G$} {
      \For{$u \in V_G$} {
        \If{ $w(\arc{t}{u}) \geq \shortdist_{s,t}$ and $u \not\in \shortpath_{s,t}$ }{
          \textbf{yield} $\arcpath{ \shortpath_{s,t}, \arc{t}{u} }$ \;
        }
      }
    }
  }
\end{algorithm}

Algorithm~\ref{alg:all_pairs_steinerpath} runs in $O(n^3)$ time, where $n = |V_G|$.
Floyd-Warshall runs in cubic time, and the triply-nested for loop (Lines~4--8)
also runs in cubic time.
We restrict arc weights to only their nonpositive part (Line~2) because
Floyd-Warshall assumes nonnegative distances as input, in order to return
simple paths.\footnote{
Even by allowing arcs of weight 0, the implementation requires some extra
care to break ties in favor of simple paths.}
Because $w_{\leq 0}$ uses only the nonpositive part of arc weights,
$-\shortdist_{s,t}$ is an underestimate of $w(\shortpath_{s,t})$, and in fact
$-\shortdist_{s,t}$ is an underestimate of the weight of every suffix of
$\shortpath_{s,t}$.
Thus, Line~7 ensures that every suffix of the returned path has positive weight,
and that the path is simple.
Algorithm~\ref{alg:all_pairs_steinerpath} is \term{precise}:
every path returned is a Steinerpath.
But the algorithm does not return every Steinerpath.
In fact, exhaustively finding every Steinerpath is an intractable problem,
which we prove by reduction from a known NP-complete problem:

\vspace{.2cm}
\begin{thm}
Determining in general whether or not there exists a Steinerpath
from $s$ to $t$ for $s, t \in V_G$ is NP-hard.
\end{thm}
\begin{proof}
We show that the problem of determining whether a graph $K$
admits a Hamiltonion circuit with cost $\le k$ can be reduced to finding
a Steinerpath in a graph of size $|V_K|+3$.

Given an edge-weighted graph $K$, arbitrarily choose a start vertex $r \in V_K$.
Remove $r$ and add two new vertices $r_s$ and $r_t$.
Each should get a copy of the same set of adjacent arcs that $r$ had.
Let $M$ be some number greater than the sum of all edge weights in $K$.
Give each node weight $M$.
Also introduce two new nodes $s$ and $t$ with weight $0$.
% Give node $s$ weight $0$ and give node $t$ weight $k$.
Introduce an arc $\arc{s}{r_s}$ with weight $-M (n+1)$.
Introduce an arc $\arc{r_t}{t}$ with weight $k$.
There exists a Hamiltonion circuit in $K$ with cost $\le k$ if and only if there
exists a Steinerpath from $s$ to $t$.
\todo{needs more explanation, proof.}
\end{proof}

Not only is intractable to find every Steinerpath, the following lemma shows
that even if we are given all Steinerpaths, they
do not alone suffice to reduce the problem space to a single solution.
\vspace{.2cm}
\begin{lemma}
Optimal solutions are not necessarily covered by Steinerpaths.
\end{lemma}  
\begin{proof}
Let $G$ be the graph illustrated in
Figure~\ref{fig:steiner:non-covered}.
$G$ is its own solution ($H=G$),
but there are no Steinerpaths that include the edge $\edge{b}{e}$.
\end{proof}
\input{fig/steiner/non-covered}

% \thesisonly{
% \sam{
% I think I'm gonna leave this whole subsection out of the proposal.
% }
% \subsection{Adapting Existing \PCST\ Tests}
% \label{subsubsec:steiner:prior-tests}
% }

% \nascomment{
%   need to clarify that the transformations are happening on the
%   undirected graph (I think?).}
% 
% \sam{could note $O(?)$ runtimes for all of these.}
% 
% \paragraph{Least-Cost Test}
% \citep{beasley_algorithm_2006}.
% Let $d(u, v)$ be the minimum cost of a path between $u$ and $v$ (ignoring node
% weights) for any pair of nodes $u$ and $v$.
% These can be found for all node pairs in $O(n^3)$ time with the Floyd-Warshall
% algorithm \citep{roy_transitivite_1959,floyd_algorithm_1962,warshall-62}. 
% For any arc $e = (u,v)$, if $d(u, v) < c(e)$, then $e$ can be removed from the
% graph.
% 
% \paragraph{Min-Adjacency Test}
% For any edge $e = (u,v)$, if $c(e) \le \min{\{w(u), w(v)\}}$ and $c(e) \le
% \min_{v^\prime}{c(u, v^\prime)}$, then any solution that includes any of $u$,
% $v$, and $e$ must include all of $u$,
% $v$, and $e$, so they can be merged into a single node with weight $w(u) + w(v) -
% c(e)$. 
% \sam{cite} \nascomment{clarify what ``merge'' means -- I'm guessing it
%   means all edges involving $u$ or $v$ now involve the new node (give
%   it a symbol?)}
% 
% \paragraph{Degree $\ell$ Test}
% \sam{TODO}
% 
% 
% \paragraph{Merging more aggressively}
% 
% Minimum adjacency can be generalized:
% 
% \begin{itemize}
%   \item Can merge anytime we've proven $z_u = z_v$ and .
% 	In fact, we can add inequalities in even more cases.
% 	If the edge $e=(u,v)$ satisfies the minimum adjacency test, then we can
% 	conclude that $z_u + z_v - 1 \le z_e$ (i.e. if $z_u = z_v = 1$, then $z_e = 1$).
% 	If we've already proven that $z_u \le z_v$,
% 	then we can conclude that $z_u \le z_e$.
%   \item Can merge over paths.
% \end{itemize}
% 
% 
% \sam{
% This section was just copy-pasted from Ben's notes.
% It needs to be rewritten to use the same notation as the rest of the paper.
% }
% \begin{quote}
% \noindent Claims:\\
% \textbf{1. Steinerpaths from Individual Edges}
% For every ordered pair of node $(u,v)$, say $e = (u,v)$.\\
% Form a directed edge $e_{u \rightarrow v}$ with weight given by $wt(e_(u \rightarrow v)) = wt(e) + wt(v)$ (positive weights are good, negative weights are bad, for both edges and nodes).\\
% If $wt(e_(u \rightarrow v)) > 0$ for any pair of nodes $(u,v)$, then $(u,v)$ is Steinerpath from $u$ to $v$.\\
% This claim is identical to that for the normal Prize-Collecting Steiner Tree problem.\\
% \textbf{2. Steinerpaths from Floyd-Warshall Paths}\\
% For every ordered pair of nodes $(u,v)$, say $p(u,v)$ is the shortest path between $u$ and $v$.\\
% Let $wt_-(p(u,v))$ be the sum of all nodes and edges along the path with weights less than zero (not counting endpoint nodes).\\
% Again, form a directed path $p_{u\rightarrow v}$ with weight given by $wt(p_{u\rightarrow v}) = wt_- (p(u,v)) + wt(v)$.\\
% If $wt(p_{u\rightarrow v}) > 0$, then $p(u,v)$ is a Steinerpath from $u$ to $v$.\\
% The difference here from the normal \PCST\ is that we only count negative nodes and edges along the path.\\
% \textbf{3. Partial Orderings from Steinerpaths}\\
% If there exists a Steinerpath from $u$ to $v$, then $z_u \leq z_v$.\\
% This is the same as the normal \PCST\.\\
% \textbf{4. Merging Over Edges if $z_u = z_v$}\\
% Say $e = (u,v)$ and $z_u = z_v$.
% If at least one of $u$ or $v$ has no adjacent edges with weight greater than $wt(e)$, OR $wt(e) \geq 0$, we can merge over $e$ to create a new node of weight $wt(u) + wt(v) + wt(e)$.\\
% The difference here from the normal \PCST\ is that even if $u$ and $v$ both have better adjacent edges, if the intermediate edge has weight greater than zero, we can start merge over it. This is because we know that if $u$ and $v$ are in the solution, there is an optimal solution that includes that edge.\\
% Also, when discarding other adjacent edges to $u$ and $v$, we all the positive edges, rather than just taking the max (because we want all the positive edges we can get in the solution).\\
% \textbf{5. Merging Over Paths if $z_u = z_v$}\\
% Say $p(u,v)$ is a path between $u$ to $v$, and $z_u = z_v$. 
% If at least one of $u$ and $v$ has no adjacent edge $e$ s.t. both $wt(e) > wt_-(p(u,v))$ and $e$ is not in $p(u,v)$, we can merge over $p(u,v)$ to create a new node of weight $wt(p(u,v)) + wt(u) + wt(v)$, where $wt(p(u,v))$ counts all the edges and nodes along $p(u,v)$, except the endpoints.\\
% The difference here is that we can only use $wt_-(p(u,v))$ when testing if we can merge. That is, we can only count negative edge and node weights.\\
% Again, when discarding other adjacent edges to $u$ and $v$, we all the positive edges, rather than just taking the max.\\
% \textbf{6. Least-Cost Test}:\\
% This taken is adapted from Ljbuic et al (2006).\\
% Say $p(u,v)$ is a path between $u$ and $v$, and $e = (u,v)$ is the edge between $u$ and $v$. If $wt_-(p(u,v)) > wt(e)$, and $wt(e) \leq 0$, $e$ can be discarded from the graph.\\
% The difference here from the normal \PCST\ is that we only consider negative node and edge weights along $p(u,v)$. This is because there might be an easy way to get all the positive nodes and edges along $p(u,v)$, and the best way to connect $u$ and $v$ might then be $e$, not $p(u,v)$. So $e$ could still be useful in that case.\\
% 
% 
% The definition of Steinerpaths still applies, as does the result that $\exists
% \text{Steinerpath}(u, v) \Rightarrow z_u \le z_v$.
% 
% We're going to want to run Floyd-Warshall using the directed graph (where
% directed edge weights include the weight of the destination node), but
% edge weights clamped to zero: $w(u,v) := min(0, w(v) - c(u, v))$.
% If we use these min-cost paths, least-cost test~\citep{beasley_algorithm_2006} now applies without further
% modification.
% 
% Merging nodes and edges into one new node (like we do in min-adjacency) is a
% little more complicated when we have arbitrary weights.
% When merging happens in the original \PCST\ problem, there may be multiple
% incoming edges from a single outside node into the new node.
% All but one of these can be discarded.
% The same goes for outgoing edges.
% In the generalized \NEWCS\ problem, all edges with positive weight should be kept.
% If there are no edges with positive weight, then exactly one negative edge should
% be kept.
% Actually, both of these rules can be thought of as immediate applications of the
% least-cost test.
% 
% \end{quote}







% \section{An Approximate Algorithm}
% 
% \sam{We haven't implemented this, and we don't prove any approximation bounds,
% so maybe we shouldn't include it?} \nascomment{include if we use it.
% if not, maybe just point out in a footnote that it exists.}
% 
% 
% We introduce an algorithm that gives a feasible solution to \PCST\ (which gives a
% lower-bound).
% 
% Assume that we have already done the same preprocessing we've already been
% doing:
% 
% \begin{itemize}
%   \item Reduce the graph as much as you can. \sam{too informal}
%   \item Find as many inequalities among indicator variables as you can. 
% \end{itemize}
% 
% Given those inequalities, make an index from each node $v$, to the set of nodes
% $u$ it is less than (i.e. implies), along with the respective shortest paths,
% $\text{path}(v, u)$, and costs, $d(v, u)$:
% \begin{equation}
% A(v) = \{
% 	(u, \text{path}(v, u), d(v, u)) \text{ s.t. } z_v \le z_u
% \}
% \end{equation}
% 
% At the $i^{th}$ step in the algorithm, let $V_i$ be the set of nodes we've already added.
% Let
% \begin{equation}
% \delta(v)  = w(v) - d(V_i, A(v) \setminus V_i) + \sum_{u \in A(v) \setminus V_i}
% {( w(u) - d(v, u)
% 	)}
% \end{equation}
% where the distance function $d$ has been extended to sets:
% \begin{equation}
% d(X, Y) := \min_{x \in X, y \in Y}{d(x,y)}.
% \end{equation}
% In other words, consider the weight of the vertex $v$, plus everything it
% implies that we haven't already added, minus the cost of connecting them to
% what we already have.
% 
% Greedily pick
% \begin{equation}
% v_{i+1} =
% \argmax{v \in V_G \setminus V_i} {\{
% \delta(v)
% % 	w(v) - d(V_i, A(v) \setminus V_i) + \sum_{u \in A(v) \setminus V_i} {(
% % 		w(u) - d(v, u)
% % 	)}
% \}}.
% \end{equation}
% 
% Add $v_{i+1}$ and the respective paths, if doing so improves the score (i.e. if
% $\delta(v) > 0$):
% 
% \begin{equation}
% V_{i+1} = V_i \cup
% 	 \text{path}(V_i, A(v_{i+1}) \setminus V_i)
% % 	\{v_{i+1}\} 
% 	\cup \bigcup_{u \in A(v) \setminus V_i}
% {\text{path}(v_{i+1}, u)}
% \end{equation}
% 
% \sam{there might be something better we could do here; adding every path is not
% necessarily the optimal way to connect them all.}
% 
% Otherwise terminate and return $V_i$.












% \subsection{\PCST}
% 
% \nascomment{need more hand-holding here.  first discuss the
%   availability of benchmark \PCST\ problems.  then say we're going to
%   quantify the usefulness of our method, relative to baseline (which
%   needs to be clearly stated, along with the differences to our
%   method), in two ways:  wall time, and number of feasible solutions.
%   we may not have space to actually give the DP here ... might be
%   necessary to simply state its existence and move it to supplementary materials}

% \subsection{Counting how many possible solutions we rule out}
% 
% \sam{we didn't implement this algorithm.
% without implementing it and running experiments with it, it doesn't actually
% tell us much.
% I think we should cut this section.}
% Here we describe a
% dynamic program whose input is a lattice representing a partial ordering on Booleans, and whose output is a count of how many of the
% $2^N$ possible configurations respect the partial ordering:
% 
% For each variable in the lattice $x$, we'll recursively calculate two numbers
% \begin{itemize}
%   \item $f_0(x)$, the number of legal configurations of its descendents if $x =
%   0$, and
%   \item $f_1(x)$, the number of legal configurations of its descendents if $x =
%   1$.
% \end{itemize}
% Letting $child(x)$ denote the set of $x$'s direct children (variables $u$ where
% we've proven $x \le u$, but $\not\exists x \le v \le u$), these can be
% calculated by:
% \begin{itemize}
%   \item $f_0(x) = \prod_{u \in child(x)}{(f_0(u) + f_1(u))}$
%   \item $f_1(x) = \prod_{u \in child(x)}{f_1(u)}$.
% \end{itemize}
% 
% 
% But before we do that we have to convert the lattice into a tree.
% Whenever a variable $x$ has more than one parent, $|par(x)| > 1$, replace its
% parents with $2^{|par(x)|}$ new nodes, each representing a possible
% configuration of its parents.
% Each new parent node gets a copy of everything below them.
% \sam{need to be describe this more clearly if we're actually going to include
% this algorithm.}
% 
% It can be immediately seen by induction that $f_1(x) = 1, \forall x$. 


% \subsubsection{Experiments}
% 
% \begin{itemize}
%   \item Number of nodes we reduce on standard datasets. \sam{this is where Ben's numbers go}
%   \item \nascomment{wall time?  compared to a ``baseline'' set of
%     constraints added to the problem}
% \end{itemize}
% 



% \section{Random Notes, Lemmas}
% 
% \sam{
% The following are for the traditional \PCST\ problem (only non-negative edge
% costs, node prizes). 
% I haven't thought about them for the generalized problem.
% These are sort of interesting, but should maybe get cut.
% } \nascomment{include if helpful}
% \begin{lemma}
% \sam{this isn't novel}
% Every leaf node in the solution has higher node weight than its incoming edge
% cost.
% \end{lemma}
% 
% \begin{lemma}
% \sam{not sure if this is novel. pretty obvious though}
% For every edge $e$ in the solution, $e$ splits the solution into two
% parts. The total weight of either part is higher than the cost of $e$.
% \end{lemma}
% 
% \begin{lemma}
% \sam{this isn't novel}
% Finding Steiner trees when there are only two positive nodes $u_1,
% u_2$ is easy:
% 
% Find the shortest path between $u_1$ and $u_2$, $path(u_1, u_2)$. 
% Say it has cost $d(u_1, u_2)$.
% If $d(u_1, u_2) \le \min{\{w(u_1), w(u_2)\}}$, then $path(u_1, u_2)$ is an
% optimal Steiner tree.
% Otherwise the single node with the largest weight is the optimal Steiner tree.
% \end{lemma}
% 
% \begin{lemma}
% \sam{this is novel AFAIK}
% \sam{to be clearer, this is when arbitrary node weights are allowed. if we find
% that $\le 3$ nodes have positive weight, we can do this trick.}
% Finding Steiner trees when there are only three positive nodes, $u_1, u_2,
% u_3$, is also easy:
% 
% For each $v \in V_G$, look at $w(v) + \sum_{u_i} {\max(0, w(u_i) - d(v, u_i))}$.
% Pick the biggest $v$, and every node it implies.
% \end{lemma}
% \begin{proof}
% A tree with $\le 3$ leaves can have at most one vertex with degree $> 2$.
% $v$ will be that vertex.
% \end{proof}
