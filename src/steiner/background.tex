\section{Background}
\label{sec:steiner:background}

\subsection{The Maximum Spanning Connected Graph Algorithm}

In \citet{flanigan2014amr} we introduced the maximum spanning
connected graph algorithm (\MSCG).
We don't claim \MSCG\ as a contribution of this thesis,
but \MSCG\ forms a core component of our novel \NEWCS\ algorithm~%
(\S\ref{sec:steiner:beasley}), so we review it here.
The input to \MSCG\ is an undirected graph $G = (V_G, E_G)$ with
real-valued edge weights $w(e) \in \R, \forall e \in E_G$, and
it returns a spanning connected subgraph
$H \subseteq G$ of maximal weight:
\begin{equation}
\label{eq:steiner:bg:mscg}
\MSCG(G, w) =
\argmax{\substack{H \subseteq G,\\H \text{ spanning},\\H \text{ connected}}}{
  \sum_{e \in E_H}{w(e)}
}.
\end{equation}
Only edge weights are considered, as spanning subgraphs must include
every node.

The algorithm extends Kruskal's
algorithm for finding minimum spanning trees~\citep{kruskal_shortest_1956}.
Whereas Kruskal's algorithm assumes that every edge weight is negative,
\MSCG\ handles nonnegative edges as well, and as a result may not output a
tree.
\MSCG\ consists of two steps:
\begin{enumerate}
  \item Add all edges with nonnegative weight in $E_G$ to $E_H$.
  \item Pick the least negative edge in $E_G$ that has not been considered yet.
  If it connects two disconnected components in $E_H$, add it to $E_H$.
  Otherwise discard it.
\end{enumerate}
Step 2 is repeated until the graph is connected.
\citet{flanigan2014amr} contains a proof that this greedy algorithm gives
the optimal solution.
It requires sorting edges by weight, so its runtime is $O(|E_G| \log |E_G|)$, which is $O(n^2 \log n)$ when $G$ is dense.




\subsection{The Prize-collecting Steiner Tree Problem}
\label{sec:pcst_defn}

% \sam{Maybe relate to the node-weighted Steiner tree problem \citep{segev_node-weighted_1987}.
% }
First let us formally state the prize-collecting Steiner tree problem~%
\citep{goemans_general_1992,bienstock_note_1993}.
Let $G = (V_G, E_G)$ be an undirected graph with positive node weights,
$w(v) \ge 0, \forall v \in V_G$, and negative edge weights, $w(e) \le 0, 
\forall e \in E_G$.
The \term{prize-collecting Steiner tree (\PCST)} problem is to find a
connected (but not necessarily spanning) subgraph $H = (V_H, E_H) \subseteq G$
of maximal weight:
\begin{equation}
\label{eq:pcst_obj}
\PCST(G, w) =
\argmax{\substack{H \subseteq G,\\H \text{ connected}}}{\left\{{
  \sum_{v \in V_H}{w(v)} + \sum_{e \in E_H}{w(e)}
}\right\}}.
\end{equation}
The \term{node-weighted Steiner tree (\textsc{Nwst})} problem is a similar
variant which allows positive or negative node weights~\citep{segev_node-weighted_1987}.
In both cases though, edge weights are required to be negative, so the
solution is guaranteed to be a tree.

Steiner problems are of interest to utility companies, for instance, who might wish to
connect paying customers to their grid in such a way as to maximize profits.
\PCST\ is an extension of the original Steiner tree problem (in which every
node has weight zero or infinity), and both are NP-complete~\citep{karp-72}.
Hundreds of papers have been written on variants of Steiner tree problems;
see \citep{hwang_steiner_1992} for a survey.
Remarkably, to the best of our knowledge, no work has been done on the problem
with unrestricted edge and node weights, and
most techniques for Steiner problems crucially rely on
the guarantee that the solution will be a tree.
While constraining edge weights to be negative makes sense as a model of the
cost of running electricity lines, and constraining node weights to be positive
makes sense as a model of the expected value of potential customers, neither constraint is
appropriate when predicting semantic graphs.
Positive edge weights are necessary in order to predict reentrancies, which
semantic graphs are known to have.
And negative node weights are needed to give a model the power to
express that a concept is more likely \emph{not} to appear in a parse.
In the next section we introduce an extension of \PCST\ that removes these
constraints, and is more suitable for semantic parsing.

\sam{something here about prior work on \PCST\ solving.}
\sam{define \term{spanning}, \term{connected}?}
% State-of-the-art \PCST\ solvers take the general strategy of applying reduction
% tests to reduce the size of the problem, before feeding it into an integer
% linear program (ILP) solver or branch-and-cut procedure \citep{ljubic-06}.
% \sam{Oversimplified. We'll have a related work
%   section? \nascomment{give a forward pointer to the relevant section
%     where this is discussed in more detail, if we have one}}
% This work introduces graph reductions which are strictly stronger
% \nascomment{not sure what ``stronger'' means here} than
% reductions from prior work, but which can still be found in polynomial time.
% We show in experiments on standard \PCST\ datasets that our technique is faster
% than the prior state-of-the-art solvers \sam{we hope!}.
% We also present a generalization of \PCST\ in which node values and edge costs can
% take on any positive or negative value, motivated by applications in natural
% language processing.
% We show that our techniques can, with slight modifications, be extended to the
% \NEWCS\ problem.

