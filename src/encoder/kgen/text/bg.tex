\subsection{Background: Star Semirings and Kleene's Algorithm}
\label{ssec:kgen:kgen:bg}

A star semiring is an algebraic structure which is closely connected to regular
expressions and weighted finite state automata.
Formally, it is a six-tuple, $(\SS, \szero, \sone, \splus, \stimes, \sstar)$,
where $\SS$ is a set, $\szero, \sone \in \SS$ are distinguished elements,
$\splus,~\stimes: \SS \times \SS \rightarrow \SS$ are
binary operations, and $\sstar: \SS \rightarrow \SS$ is a unary operation,
subject to the following laws. For all $a, b, c \in \SS$:
\begin{subequations}
\begin{align}
\label{eqn:kgen:kgen:star-semiring}
\szero \splus a = a \splus \szero = a && \textit{($\szero$ is the identity for $\splus$)} \\
(a \splus b) \splus c = a \splus (b \splus c) && \textit{($\splus$ is associative)} \\
a \splus b = b \splus a && \textit{($\splus$ is commutative)} \\
\sone \stimes a = a \stimes \sone = a && \textit{($\sone$ is the identity for $\stimes$)} \\
(a \stimes b) \stimes c = a \stimes (b \stimes c) && \textit{($\stimes$ is associative)} \\
a \stimes (b \splus c) = (a \stimes b) \splus (a \stimes c) && \textit{($\stimes$ left distributes over $\splus$)} \\
(a \splus b) \stimes c = (a \stimes c) \splus (b \stimes c) && \textit{($\stimes$ right distributes over $\splus$)} \\
a\sstar = \sone \splus (a \stimes a\sstar) = \sone \splus (a\sstar \stimes a). && \textit{(asteration / transitive closure)}
\end{align}
\end{subequations}
When clear from context, we will write $a \stimes b$ as $ab$.

Star semirings extend \term{semirings}, which have been used extensively in dynamic programming algorithms,
especially in natural language processing~\citep{goodman_semiring_1999,eisner-goldlust-smith:2004:ACLdemo}.
Intuitively, addition is used to combine alternate events (in our case alternate paths),
and multiplication is used to compose sequences of events that occur together jointly (in our case, the individual arcs along a path).
The distributive property enables refactoring, allowing computation to be
memoized and reused in a dynamic program.
\emph{Star} semirings add the asteration operator, $\sstar$, which is used to repeat a sequence zero or more times (
in our case, following a loop on a non-simple path).
Star semirings have the same syntax as regular expressions, so in some sense they can be thought
of as \emph{interpreters} for regular expressions.
They are related to \term{Kleene algebras}, which follow the same laws, plus
an additional \term{idempotency} law:
\begin{equation}
a \splus a = a, \forall a \in \SS.
\end{equation}
We happen to use Kleene algebras in all of our experiments, but idempotency is not strictly required for KGEN.

As an example, the \term{max-times} semiring on the unit interval,
$\mathcal{M} =(\interval{0}{1}, 0, 1, \max{}, \times)$, can be made into a
star semiring by letting $x\sstar = 1$ for every $x \in \interval{0}{1}$.

Furthermore, any star semiring $\SS = (\A, \szero, \sone, \splus, \stimes, \sstar)$ can be lifted to a star semiring over square matrices in the following way:
\begin{subequations}
\label{eq:kgen:kgen:star-semiring-matrices}
\begin{align}
  \SS^{d \times d} = (\A^{d \times d}, \tensor{0}, \tensor{I}, \splus, \stimes, \sstar), \\
  \text{ where: } \notag\\
  \tensor{0}_{i,j} = \szero,
    \\ %&& \textit{(zeros)} \\
  \tensor{I}_{i,j} = \begin{cases}
    \sone, & \text{if $i=j$} \\
    \szero, & \text{otherwise},
  \end{cases}
    \\ %&& \textit{(ones on the diagonal)} \\
  [A \splus B]_{i,j} = A_{i,j} \splus B_{i,j},
    \\ %&& \textit{(element-wise addition)} \\
  [A \stimes B]_{i,j} = \bigoplus_{k=1}^n {( A_{i,k} \stimes B_{k,j})}, \\%    && \textit{(generalized matrix multiplication)} \\
  A\sstar = \Kleene_\SS(A).
    && \textit{(Alg.~\ref{algo:kgen:kgen:kleenes})} 
\end{align}
\end{subequations}
We describe Kleene's algorithm~\citep{kleene_representation_1951,oconnor_a_very_2011}
in Algorithm~\ref{algo:kgen:kgen:kleenes}.
As in the Floyd-Warshall algorithm, $\Kleene_\SS(A)$ is built up iteratively,
at each step allowing one more node to be used in paths.
Line~5 gives the main recurrence of the dynamic program.
A path from $i$ to $j$ will either go through $k$ as an intermediate
node ($A^{(k-1)}_{i,k} A^{(k-1)}_{k,k}\sstar A^{(k-1)}_{k,j}$), or not ($A^{(k-1)}_{i,j}$).
Some well-known special cases can calculate reachability ($\SS = \text{Boolean}$),
shortest paths ($\SS = \text{min-plus}$), or can convert an FSA into a regular
expression ($\SS = \text{RegEx}$).
When calculating shortest paths or reachability, $a\sstar = \sone$, i.e. loops are never needed, so the $A^{(k-1)}_{k,k}\sstar$ term can be omitted.
A straightforward implementation is $O(n^3)$, but in many cases
the two inner loops may be vectorized, yielding runtimes closer to $O(n)$.
\begin{algorithm}[t]
  \KwData{
    A square matrix $A \in \SS^{d \times d}$ with entries from some
    star semiring $\SS$.
    $A$ can be thought of as the edge weights of a
    directed graph having $\{1, \ldots, d \}$ as vertices.
  }
  \KwResult{
    $A$'s asteration, $\Kleene(A)$.
  }
  \tcc{$A^{(k)}_{i,j}$ encodes paths from $i$ to $j$ that only use intermediate nodes in $\{1, \ldots, k \}$.}
  $A^{(0)} \leftarrow A$ \tcp*{paths having no intermediate nodes}
  \For{$k = 1, \ldots, d$}{
    \For{$i = 1, \ldots, d$} {
      \For{$j = 1, \ldots, d$} {
%       \tcc{A path from $i$ to $j$ will either go through $k$ as an intermediate
%       node (possibly more than once), or not.}
      $A^{(k)}_{i,j} \leftarrow
%         ( A^{(k-1)}_{i,k} \stimes A^{(k-1)}_{k,k}\sstar \stimes A^{(k-1)}_{k,j} )
        ( A^{(k-1)}_{i,k} A^{(k-1)}_{k,k}\sstar A^{(k-1)}_{k,j} )
        \splus A^{(k-1)}_{i,j}
      $ ; \\
      }
    }
  }
  \Return $\Kleene_\SS(A) = A^{(n)}$;
  \caption{\Kleene\ / Generalized Floyd-Warshall}
  \label{algo:kgen:kgen:kleenes}
\end{algorithm}
Lifting a star semiring into a square matrix is an effective
way in general to construct a star semiring with a \emph{non-commutative} $\stimes$ operation.
This is important for our purposes, because we will use $\stimes$ to encode sequences, and we
wish to be sensitive to the order of the sequence we are encoding.\footnote{%
$\splus$, on the other hand, \emph{must} be commutative, by law~(Equation~\ref{eqn:kgen:kgen:star-semiring}).
This is important for our encoder to respect the natural invariances of graphs.
I.e. alternative paths have no order.
}
