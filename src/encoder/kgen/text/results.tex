\subsection{Results}
\label{subsec:kgen:kgen:results}


\begin{table}[t]
\ra{1.2}
\centering
\begin{tabularx}{\linewidth}{@{}l X X X@{}}
  \toprule
  \textbf{Performance} (dev. data, $n \leq 20$) & {\bf P} & {\bf R} & {\bf F$_1$}   \\
  \midrule
  MLP  &
    75.4 & 70.2  & 72.7  \\
  \textbf{GCN} {\small $(K=1)$} $\rightarrow$ MLP  &
    87.5 & 86.2 & 86.8   \\
  \textbf{KGEN} $\rightarrow$ MLP &
    91.5 & 88.8 & 90.2   \\
  \midrule[\cmidrulewidth]
  BiLSTM $\rightarrow$ MLP  &
    91.0 & 89.0 & 90.0   \\
  BiLSTM $\rightarrow$ \textbf{GCN} {\small $(K=1)$} $\rightarrow$ MLP  &
    91.5 & 89.9 & \underline{90.7}   \\
  BiLSTM $\rightarrow$ \textbf{KGEN} $\rightarrow$ MLP &
    90.9 & 89.5 & 90.2   \\
  \bottomrule
\end{tabularx}
\caption{%
\label{tab:kgen:kgen:results-dev}%
Labeled precision, recall, and F$_1$ on development data, filtered to sentences
of length less than or equal to 20.
The upper three rows are models tested \emph{without} any biLSTM.
The first row is a syntax-free model, with no graph contextualization step.
\textbf{GCN} is a reimplementation of the graph convolutional network of \citet{marcheggiani_encoding_2017},
and \textbf{KGEN} is our own Kleene Graph Encoder Network.
The lower three rows add a biLSTM under each of the three tested models.
}
\end{table}


\begin{table}[t]
\ra{1.2}
\centering
\begin{tabularx}{\linewidth}{@{}l X X X@{}}
  \toprule
  \textbf{Performance} (test data) & {\bf P} & {\bf R} & {\bf F$_1$}   \\
  \midrule
  MLP &
    70.6 & 64.2  & 67.2  \\
  \textbf{GCN} {\small $(K=1)$} $\rightarrow$ MLP &
    82.6 & 80.0 & 81.3   \\
  \textbf{KGEN} $\rightarrow$ MLP &
    88.4 & 83.7 & \underline{86.0}   \\
  \midrule[\cmidrulewidth]
  BiLSTM $\rightarrow$ MLP &
    85.4 & 81.4  & 83.3  \\
  BiLSTM $\rightarrow$ \textbf{GCN} {\small $(K=1)$} $\rightarrow$ MLP &
    85.6 & 82.3  & 84.0  \\
  BiLSTM $\rightarrow$ \textbf{KGEN} $\rightarrow$ MLP &
    86.5 & 84.5  & 85.5  \\
  \midrule[\cmidrulewidth]
  \midrule[\cmidrulewidth]
  \textsc{NeurboParser Basic}  &
    -    & -    & 89.4   \\
  \textsc{NeurboParser Freda3}  &
    -    & -    & 90.4   \\
  \textsc{D\&M Basic}  &
    -    & -    & 91.4   \\
  \textsc{D\&M  +Char +Lemma}  &
    -    & -    & \textbf{93.7}   \\
  \bottomrule
\end{tabularx}
\caption{%
\label{tab:kgen:kgen:results-id-test}%
Labeled precision, recall, and F$_1$ on the test data.
The top three rows are models tested \emph{without} any biLSTM.
The first row is a syntax-free model, with no graph contextualization step.
\textbf{GCN} is a reimplementation of the graph convolutional network of \citet{marcheggiani_encoding_2017},
and \textbf{KGEN} is our own Kleene Graph Encoder Network.
The next three rows add a biLSTM under each of the three tested models.
In the lower four rows, we quote performance results from recently published state-of-the-art systems, for reference.
}
\end{table}


We report labeled precision, recall and F$_1$ on development data for our
models and baselines in Table~\ref{tab:kgen:kgen:results-dev}.
The upper section contains models without a biLSTM, and the lower section contains models run with a biLSTM.
The straight MLP with no context performs poorly, unsurprisingly;
it has access only to token lookup embeddings, and other than the distance
features, has no knowledge of the linear order of the sentence.
Both GCN and KGEN on the other hand, perform surprisingly well without an LSTM,
with KGEN significantly (4.7 absolute F$_1$) better.
These two models are making use of the linear order of the sentence, but only
through the input graph $G$ (Figure~\ref{fig:kgen:kgen:dusty-ud-adj}).
The GCN tested has 1 layer, so a token's representation only has access to
a window limited to the previous token, the next token, and any direct
neighbors in the syntactic graph.
KGEN on the other hand is able to take much better advantage of the linear
order information encoded in $G$.
Because $G$ is fully connected and includes both directions for every arc,
the output of \KGEN\ is \term{fully contextualized}, even without a biLSTM.
By this we mean that the representation for every token pair $(s, t)$ includes
information from \emph{every} arc in $G$ (and hence also every token in $\seq{x}$).

Both GCN and the syntax-free model have much improved performance when run on
top of a biLSTM, with gains of $17.3$ and $3.9$ absolute F$_1$, respectively.
The gains accurately reflect how much more information is available to the model.
Somewhat surprisingly, \KGEN\ is unimproved with an LSTM;
A reasonable conclusion is that \KGEN\ is capturing the relevant linear
order context of the sentence \emph{as well as a biLSTM}.
% It seemed to already be doing a good job of capturing linear order without it.
The two variants of \KGEN\ are tied as the best performing models on the development set, beating
the BiLSTM$\rightarrow$GCN by $1.4$ absolute F$_1$.
Of course, the development set was used for tuning, so the results on the unseen
test data in the following paragraph should be taken more seriously.

We report the same statistics for the test data %\emph{in-domain test data}
in Table~\ref{tab:kgen:kgen:results-id-test}, along with four previously
published results for reference.
Similar trends are seen on the test data, except that here, \KGEN\
\emph{without} a biLSTM is the best performing model we tested.
A possible explanation is that the biLSTM is redundant, so the extra model
complexity only leads to overfitting.
Scores are roughly 5 points lower than on the development data across the
board, likely because we tested on test
sentences of all lengths, whereas longer sentences were filtered out of the
train and development splits.
This, along with our very limited hyperparameter tuning, also likely explains
the bulk of the gap between our models and \textsc{NeurboParser}/\textsc{D\&M}.
More comprehensive experiments could possibly close the gap.

The results are already quite promising though, with KGEN without a biLSTM
outperforming a GCN even with a biLSTM.
The fact that KGEN is able to achieve strong performance without a biLSTM means
that it is able to learn both sequential order and syntactic context from a
single unified multigraph.
This bodes well for its potential when adding more preprocessed graphs to its
input multigraph.
There is also potential for its decisions to be interpretable.
Assuming the same interpretation methods used in
SoPa~(\S\ref{sec:Interpretability}) can be applied to KGEN, 
then KGEN's use of sequential order and syntactic context are both
interrogable.

% \begin{table}[t]
% \ra{1.2}
% \centering
% \begin{tabularx}{\linewidth}{@{}l X X X@{}}
%   \toprule
%   \textbf{Performance} (out-of-domain test data) & {\bf P} & {\bf R} & {\bf F$_1$}   \\
%   \midrule
%   MLP &
%     70.6 & 64.2  & 67.2  \\
%   \textbf{GCN} {\small $(K=1)$} $\rightarrow$ MLP &
%     82.6 & 80.0 & 81.3   \\
%   \textbf{KGEN} $\rightarrow$ MLP &
%     88.4 & 83.7 & 86.0   \\
%   \midrule[\cmidrulewidth]
%   \textit{\small still running\ldots} \\
%   BiLSTM $\rightarrow$ MLP &
%     TK & TK  & TK  \\
%   BiLSTM $\rightarrow$ \textbf{GCN} {\small $(K=1)$} $\rightarrow$ MLP &
%     TK & TK  & TK  \\
%   BiLSTM $\rightarrow$ \textbf{KGEN} $\rightarrow$ MLP &
%     TK & TK  & TK  \\
%   \midrule[\cmidrulewidth]
%   \citet{Peng-EtAl:2017:ACL} \textsc{BASIC} \textit{(test)} &
%     -    & -    & 89.4   \\
%   \citet{Peng-EtAl:2017:ACL} \textsc{FREDA3} \textit{(test)} &
%     -    & -    & 90.4   \\
%   \citet{dozat_simpler_2018} Basic \textit{(test)} &
%     -    & -    & 91.4   \\
%   \citet{dozat_simpler_2018} +Char +Lemma \textit{(test)} &
%     -    & -    & \textbf{93.7}   \\
%   \bottomrule
% \end{tabularx}
% \caption{%
% \label{tab:kgen:kgen:results-id-test}%
% Labeled precision, recall, and F$_1$ on the in domain test data.
% The first row is a syntax-free model, with no graph contextualization step.
% \textbf{GCN} is a reimplementation of the graph convolutional network of \citet{marcheggiani_encoding_2017},
% and \textbf{KGEN} is our own Kleene graph encoder network.
% In the lower rows, we quote performance results from recently published state-of-the-art systems, for reference.
% }
% \end{table}



