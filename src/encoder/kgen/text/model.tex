\subsection{Semantic Dependency Parsing Model}
\label{ssec:kgen:kgen:model}

Since our focus is on encoding, we return to an arc-independent decoder,
as in \logitedge~(\S\ref{s:logitedge}).
Although we found that modeling arc interactions was beneficial,
and achieved state-of-the-art at the time~(Chapter~\ref{chap:meurbo}),
the current state-of-the-art in semantic dependency
parsing~\citep{dozat_simpler_2018} uses arc-independent decoding, so
this is a reasonable simplifying choice.
Let $\seq{x} = x_1, \ldots, x_n$ be an input sentence of length $n$.
Let $\mathscr{L}$ be the set of $K+1$ possible arc labels: \DM's original $K$
arc labels, plus the additional label \noedge, which represents the absence of an arc.
As in \logitedge\, we consider all token index pairs $1 \leq i, j \leq n$ (in both directions),
and treat each ordered pair as a multiclass classification problem over $\mathscr{L}$.
We do not do any singleton pruning or enforce any constraints on the output, so decoding
can be done for each pair independently and in parallel.
We do prune pairs where $|i-j| > D$; the maximum arc distance $D=20$ was
chosen so as to prune out fewer than 0.5\% of gold arcs.
We also prune the label set $\mathscr{L}$ to the most common 20 labels (out of 59),
pruning out fewer than 0.5\% of gold arcs, but reducing the label set size significantly.

For candidate source index $i$ and destination index $j$, %and arc label $\ell$,
we calculate a dense representation with a parameterized function
$\tensor{f}_{\tensor{\theta}}(\seq{x}, i, j)$ (variants of $\tensor{f}$ will be
described in \S\ref{sssec:kgen:kgen:model:arc-repr}).
Then we pass the arc representation through an MLP, followed by a $\softmax{}$
% We perform an affine transform followed by a $\softmax{}$
to define a distribution over $\mathscr{L}$~(\S\ref{sssec:kgen:kgen:model:output}).
% The parameters ${\tensor{\theta}}, \{\tensor{v}_\ell\}_{\ell \in \mathscr{L}}$,
% and $\{b_\ell \}_{\ell \in \mathscr{L}}$ 
Parameters
are learned to
minimize the negative log likelihood of the training data under our model.
We use Adam~\citep{kingma2014adam} with weight decay and parameter dropout for optimization
(see Section~\ref{sec:kgen:kgen:appendix} for further optimization details).


\subsubsection{Token Representations}
\label{sssec:kgen:kgen:model:token}

We learn a lookup embedding $\tensor{v}_x \in \R^{d_w}$ for every word that appears
in the training or development data.
Word vectors are initialized with normalized GloVe 840B embeddings~\citep{Pennington:2014}
($d_w = 100$), and updated during training.
Given a sentence $\seq{x}$, we run a 1-layer bidirectional
LSTM over $\seq{x}$,
concatenate the forward and backward hidden state of each token,
and apply a learned affine layer to
obtain a contextualized token vector $\tensor{h}_i \in \R^{d_w}$ for each
token index $1 \leq i \leq n$.


\subsubsection{Arc Representations}
\label{sssec:kgen:kgen:model:arc-repr}

We experiment with three alternative ways of incorporating syntax to
produce arc representations:
none (syntax-unaware), GCN, and KGEN.
GCN and KGEN both construct directed graph $G$ based on a preprocessed syntactic dependency parse.
The nodes of $G$ are the $n$ tokens of $\seq{x}$, plus a synthetic node labeled \textsc{ROOT},
representing the syntactic root of the sentence.
First we add every arc $\larc{i}{j}{\ell}$ in the dependency parse to $G$.
Then we add a \textsc{next} arc from every token to its successive token.
These arcs allow the graph network access to the linear order of the sentence.
For every \textsc{forward} arc $\larc{i}{j}{\ell}$, we a
add a \textsc{backward} arc $\larc{j}{i}{\ell^{-1}}$ to $G$ (see Figure~\ref{fig:kgen:kgen:dusty-ud-adj}).
For the GCN model, we add a self-loop $\larc{i}{i}{\textsc{SelfLoop}}$ to every node $i$ in the graph.
In GCNs, this is necessary in order to propagate information from a node to itself in successive GCN
layers;
in KGEN, this is unnecessary.


\begin{figure}
  \centering
  \includegraphics[width=.95\textwidth]{encoder/kgen/fig/dusty_ud_adj.pdf}
  \caption{
    Our example sentence, along with the multigraph that will be embedded and contextualized.
    Solid blue ``next" arcs (above the sentence) are added between each node and its successor.
    Solid green arcs are syntactic dependency arcs (predicted in preprocessing).
    Every solid arc has a corresponding dashed \emph{backward} arc in the opposite direction.
  }
  \label{fig:kgen:kgen:dusty-ud-adj}
\end{figure}




\paragraph{Baseline: No Graph Contextualization}
For our first baseline, we use no graph contextualization step.
We form an arc representation for the ordered pair $(i, j)$ by concatenating 
their respective token representations, along with three distance features.
% We then pass this vector through a 2-layer multilayer perceptron:
\begin{equation}
% \tensor{f}(\seq{x}, i, j) = \text{MLP}(\tensor{h}_i ; \tensor{h}_j; [ \text{abs}(i - j), \1_{i < j}, \1_{i = j} ]).
\tensor{f}(\seq{x}, i, j) = \tensor{h}_i ; \tensor{h}_j; [ \text{abs}(i - j), \1_{i < j}, \1_{i = j} ].
\end{equation}


\paragraph{Baseline: Graph Convolutional Network}
For our second baseline, we reimplement the graph convolutional network (GCN) model of
\citet{marcheggiani_encoding_2017}, which we will refer to as \textsc{M\&T} for short.
\textsc{M\&T} was originally designed to perform dependency-based semantic
role labeling on the CoNLL-2009 dataset~\citep{hajic_conll-2009_2009}.
As a testament to the generality and flexibility of graph-based methods, it
can be used virtually without modification for semantic dependency parsing.
We follow the model faithfully, and we will restate it here for completeness.
GCNs consist of $K$ layers, and in each layer a node is updated based on a function of
its adjacent arcs and nodes.
Let $\tensor{u}_i^{(k)}$ denote the representation of the $i^\text{th}$ node at the $k^\text{th}$
layer.
Each layer is defined inductively in terms of the previous layer, and the current layer's parameters.
In \textsc{M\&T}, the update function is defined as follows:
\begin{subequations}
\begin{align}
  \tensor{u}_j^{(0)} = \tensor{h}_j,
  &&
  \textit{ (base layer)}\\
  \tensor{u}_j^{(k+1)} = \text{ReLU}\left(
  \sum_{\larc{i}{j}{\ell} \in G}{
    g_{\larc{i}{j}{\ell}}^{(k)}
    \left(
      \tensor{V}_{\text{dir}(\ell)}^{(k)} \tensor{u}_i^{(k)} + \tensor{b}_\ell^{(k)}
    \right)
  }\right),
  &&
  \textit{ (successive layer update)}  \\
  \text{where,} \notag\\
  g_{\larc{i}{j}{\ell}}^{(k)} =
    \sigma\left(
      \tensor{\hat{v}}_{\text{dir}(\ell)}^{(k)} \cdot \tensor{u}_i^{(k)} + \hat{b}_\ell^{(k)}
    \right),
  && 
  \textit{ (edge gate)} \\
  \text{ and node representations can be read from the final layer: }  \notag\\
  \text{GCN}(G, \{\tensor{h}_i\}_{i \leq n}) =  \{\tensor{u}_i^{(K)}\}_{i \leq n}.
  && \textit{ (result)}
\end{align}
\end{subequations}
\noindent
$\text{ReLU}$ is the rectified linear unit~\citep{hahnloser_digital_2000}, $\text{ReLU}(x) = \max(0, x)$.
The parameter matrices $\tensor{V}_{\text{dir}(\ell)}^{(k)}$ and vectors
$\tensor{\hat{v}}_{\text{dir}(\ell)}^{(k)}$ are looked up based on the
only \emph{direction} of the arc label,
$\text{dir}(\ell) \in \{ \textsc{Forward}, \textsc{Backward}, \textsc{SelfLoop} \}$,
not the label itself.
This choice was originally made in order to control the model size~\citep{marcheggiani_encoding_2017}.
In the first layer of a GCN, each node's representation contains information from its immediate neighbors.
At the $K^\text{th}$ layer, information has been propagated from all other nodes within $K$ hops.
We form an arc representation for the ordered pair $(i, j)$ by concatenating 
their contextualized token representations, along with three distance features:
\begin{equation}
\tensor{f}(\seq{x}, i, j) = \tensor{u}_i^{(K)} ; \tensor{u}_j^{(K)}; [ \text{abs}(i - j), \1_{i < j}, \1_{i = j} ].
\end{equation}
\noindent
If nodes $i$ and $j$ are within $K$ hops of each other in $G$, then
their representations are ``aware'' of each other, in a sense.
% But the two vectors are calculated without reference to their opposite node.
But note that the same vector $\tensor{u}_i^{(K)}$ is reused for \emph{all} arc
representations emanating from the node $i$; only the distance features are calculated
specifically for the pair $(i, j)$.
So for the model to be effective, $\tensor{u}_i^{(K)}$ must encode information
about its relationship to \emph{all} other candidate destination nodes.


\paragraph{Our Model: Kleene Graph Encoder Network}
\label{sssec:kgen:kgen:model:kgen}

% Let $(\SS, \szero, \sone, \splus, \stimes, \sstar)$ be a star semiring, with
% binary (infix) operators $\splus, \stimes: \SS \times \SS \rightarrow \SS$,
% their identities $\szero, \sone \in \SS$,
% and the unary (postfix) operator
% $\sstar: \SS \rightarrow \SS$.
% We assume that, in addition to following the laws,
% elements of $\SS$ are representable in memory, and that
% the functions $\splus$, $\stimes$, and $\sstar$ are subdifferentiable.
Here we describe our novel use of Kleene's algorithm as a graph contextualization
step. % in order to more effectively handle graphs as input to a neural network.
First we will describe it in general, without specializing to a specific star semiring.
Then we will describe the specific star semiring, $\maxmul$, that we use in our experiments.
$\maxmul$ was chosen because of its experimental success as a sequence encoder (Section~\ref{sec:kgen:sopa}),
and its straightforward extension into a star semiring.
% A \KGEN\ follows the following general steps:

Given the directed multigraph $G=(\{1,\ldots,n+1\}, A_G)$  %with $V_G=\{v_1, \ldots, v_n \}$
(as in Figure~\ref{fig:kgen:kgen:dusty-ud-adj}), 
% and token representations $\{\tensor{h}_i\}_{i\leq n}$,
we embed $G$'s arcs into some
star semiring $\SS$ with a differentiable parameterized function
$\tensor{S}: A_G  \rightarrow \SS$.
% Assume without loss of generality that $V_G = \{1, \ldots, n \}$.
We create an adjacency matrix $A \in \SS^{n \times n}$ where
\begin{equation}
  A_{i,j} =
  \bigoplus_{\larc{v_i}{v_j}{\ell} \in A_G}{\tensor{S}(\larc{v_i}{v_j}{\ell})}.
\end{equation}
In other words, if there is no arc from $i$ to $j$, then $A_{i,j} = \szero$;
if there are multiple arcs, then they are embedded and summed.
We then run Kleene's Algorithm (Algorithm~\ref{algo:kgen:kgen:kleenes}) to obtain a
``contextualized'' adjacency matrix $A^\sstar = \Kleene_\SS(A)$.
See Figure~\ref{fig:encoder:kgen:path-graph} for an illustration of this
contextualization step.
\input{encoder/kgen/fig/path-graph}
The contextualized arc representation for $(i, j)$ can then be read directly from the matrix,
$A^\sstar_{i,j}$.
% The remainder of the network calculates a loss as a differentiable
% parameterized function of $\Kleene_\SS(A)$:
% \begin{equation}
% \mathcal{L} = f(\Kleene_\SS(A)).
% \end{equation}
When $\SS$'s operations are differentiable, then the entire network
is differentiable and can be learned end-to-end.
We call the part of the network including
$\tensor{S}$ and $\Kleene$ a
Kleene graph encoder network.


\paragraph{The batched $\maxmul$ star semiring.}

Here we describe how the \term{Soft Patterns (SoPa)} model described in
Section~\ref{sec:kgen:sopa} can be extended into a star semiring, and so can
be used in the KGEN framework.
Let $\mathcal{M}^{d \times d}$ be the \term{max-times} star semiring lifted
to $d \times d$ matrices~(Equation~\ref{eq:kgen:kgen:star-semiring-matrices}).
Concretely, element-wise $\max$ is used for $\splus$, with the zero matrix as its identity;
$\maxmul$~(Equation~\ref{eqn:encoder:sopa:maxmul}) is used for $\stimes$, with identity $I$; and
$\Kleene_\M$ is used for $\sstar$.
Such a matrix can be thought of in SoPa terms as the transition matrix for one input for one pattern.
We embed arcs into $\mathcal{M}^{d \times d}$ similarly to our calculation of
transition matrices in SoPa (Equation~\ref{eqn:main-self-transitions}):
\begin{equation}
\label{eqn:kgen:kgen:model:transition-matrix}
\left[\tensor{\tensor{S}}(\larc{s}{t}{\ell})\right]_{i,j} =
  \begin{cases}
    \sigma\left(
      \tensor{w}_{i,j} \cdot \left[\tensor{h}_s;\tensor{h}_t;\tensor{v}_\ell] + a_{i,j}
    \right), & \text{ if } j = i \text{ or } j = i+1 \\
    0, & \text{ otherwise,}
  \end{cases}
\end{equation}
where $\tensor{w}_{i,j}$ is a vector of parameters,
$a_{i,j}$ is a scalar bias parameter, $\tensor{h}_s$ is the token 
representation of token $s$, $\tensor{v}_\ell$ is a lookup
embedding of the arc label $\ell$, and $\sigma$ is the sigmoid function.
Like in Equation~\ref{eqn:encoder:sopa:maxmul}, our transition matrix has
two non-zero diagonals: the main diagonal, and one above it.
Similarly to how SoPa uses an array of a few hundred patterns, we add another index
$1 \leq b \leq B$, where $B$ is the number of patterns\footnote{%
In our experiments, we use $B=500$ and $d=5$ (Appendix~\ref{sec:kgen:kgen:appendix}).
Since the pattern length $d$ is small and fixed, the fact that asteration in
$\mathcal{M}^{b \times d \times d}$ is $O(B d^3)$ still quite reasonable.
Our GPU implementation is closer to $O(d)$.
}:
\begin{equation}
\label{eqn:kgen:kgen:model:transition-matrix}
\left[\tensor{\tensor{S}}(\larc{s}{t}{\ell})\right]_{b,i,j} =
  \begin{cases}
    \sigma\left(
      \tensor{w}_{b, i,j} \cdot \left[\tensor{h}_s;\tensor{h}_t;\tensor{v}_\ell] + a_{b,i,j}
    \right), & \text{ if } j = i \text{ or } j = i+1 \\
    0, & \text{ otherwise.}
  \end{cases}
\end{equation}
The star semiring operations extend along this new dimension by operating on each pattern
independently.

Once $A^\sstar$ has been calculated, we extract the first row of each transition matrix,
and concatenate them, along with distance features:
\begin{equation}
\tensor{f}(\seq{x}, s, t) =
  \left[\tensor{A^\sstar}_{s,t}\right]_{1,1,:} ;
  \ldots ;
  \left[\tensor{A^\sstar}_{s,t}\right]_{B,1,:} ;
  [ \text{abs}(s - t), \1_{s < t}, \1_{s = t} ].
\end{equation}
To have exactly matched the SoPa model, we would have taken the single $(1,d)^\text{th}$ entry
from each pattern, which represents the weight of having matched the entire pattern, from
start to end.
To avoid the extensive tuning of pattern lengths that SoPa required, we simply
take the whole first row, which gives the weight of matching each initial
prefix of the pattern.

Our \SoftP experiments (\S\ref{sec:kgen:sopa}) suggest that $\mathcal{M}^{b \times d \times d}$ is
an effective choice for encoding sequences.
We also choose $\mathcal{M}$ as a base semiring
because its closure can be calculated without worry of overflow, unlike
the vanilla sum-product semiring (which overflows $x^\sstar = \infty$ for any $x \geq 1$).


\subsubsection{Output Layer}
\label{sssec:kgen:kgen:model:output}
As a final step, we pass the computed arc representation for $(i, j)$ through
a two-layer multilayer
perceptron with output dimension $|\mathscr{L}|$, and take a softmax to get
a probability distribution over $\mathscr{L}$:
\begin{align}
  P(\ell \mid,\seq{x}, i,j; \theta)  = \left[\softmax{(
    \text{MLP}(\tensor{f}_{\tensor{\theta}}(\seq{x}, i, j)
  )}\right]_{\ell}.
%    \frac{
%   	\exp\{\tensor{v}_\ell^\top \tensor{f}_{\tensor{\theta}}(\seq{x}, i, j) + b_\ell \}
%   } {
%   	\sum_{\ell^\prime \in \mathsc{L}} {
%   		\exp\{\tensor{v}_{\ell^\prime}^\top \tensor{f}_{\tensor{\theta}}(\seq{x}, i, j) + b_{\ell^\prime} \}
%   	}
%   }.
\end{align}
\noindent

% 
% 
% \paragraph{Embedding into $\mathcal{M}^{d \times d}$}
% The carrier set of $\mathcal{M}$ is the unit interval $\interval{0}{1}$,
% so assuming we have vector embeddings of nodes, $\psi(v) \in \R^{d_1}$, and
% arcs, $\psi(a) \in \R^{d_2}$, then we can concatenate, apply a
% multilayer perceptron with output size $dd$, apply a sigmoid, and then reshape:
% \begin{equation}
% \tensor{S}(\arc{u}{v}) = \text{reshape}_{d \times d}(\sigma(\text{MLP}([\psi(u); \psi(v); \psi(\arc{u}{v})]))).
% \end{equation}




% \paragraph{Extracting node and graph representations.}
% 
% So far we have calculated a matrix of
% contextualized arc representations, $\Kleene(A)$.
% If node representations are needed, we propose using $\tensor{S}(v_i) = \Kleene(A)_{i,i}$ as a
% representation for $v_i$.
% This encodes all paths from $v_i$ to $v_i$, which includes the entire subgraph
% of $G$ that is reachable from $v_i$.
% If we want a representation for the entire graph, then attention can be used~\citep{Bahdanau:2015}:
% \begin{align}
% \alpha_{i,j} = \text{MLP}(\Kleene(A)_{i,j}) \\
% Z = \sum_{i,j}{\alpha_{i,j}} \\
% \tensor{S}(G) = \frac{1}{Z}\sum_{i,j}{\alpha_{i,j} \Kleene(A)_{i,j}}.
% \end{align}
% 
% 
% \subsubsection{GCN as KGEN}
% \label{sssec:kgen:kgen:model:as-gcn}
% 
% A graph convolutional network~\citep[GCN;][]{kipf_semi-supervised_2016} learns a
% representation for a node that incorporates information
% from all nodes within $k$ hops it.
% In this subsection, we show that 