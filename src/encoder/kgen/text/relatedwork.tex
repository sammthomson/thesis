\subsection{Related Work}
\label{ssec:kgen:kgen:relatedwork}

\paragraph{Encoding nodes}

Graph convolutional networks have been used on citation networks and on a knowledge
graphs~\cite{defferrard_convolutional_2016,kipf_semi-supervised_2016},
 machine translation~\citep{bastings_graph_2017}, named
entity recognition~\citep{cetoli_graph_2017}, semantic role
labeling~\citep{marcheggiani_encoding_2017}, and multi-document
summarization~\citep{yasunaga_graph-based_2017}, among others.
GCNs provide contextualized \emph{node} representations, which
can be influenced by other nodes within a fixed distance, $k$.
In exchange for limiting the distance, GCNs can use non-lawful
composition functions.
They can be thought of as encoding, in a node $v$'s
vector, all paths emanating from $v$ (and ending anywhere) that have
length exactly $k$.
\KGEN\ on the other hand, encodes, in a representation for each pair of nodes, $\tensor{f}(u, v)$,
all paths from $u$ to $v$ of any length.
\KGEN's is a finer-grained representation, because outgoing
representations from $u$ can be always pooled, $\max_v{\tensor{f}(u, v)}$,
to get a node representation analogous to GCN's.
It is unclear that a GCN node representation for $u$ can be ``unpooled'' to recover
a representation for each opposite node $v$.

\paragraph{Encoding paths}
Paths of syntactic dependencies have long been recognized as useful
features for predicting semantic relations.
Conveniently, syntactic dependencies form an arborescence, so there is a unique
path from any given source token to any other given destination token,
making feature extraction easy and efficient.
In Chapter~\ref{chap:arc-factored-sdp}, we experimented with several variants
of hand-engineered path features (see Table~\ref{table:edgefeatures}),
and found them to be some of the most predictive features.
Roth and Lapata \shortcite{roth_neural_2016} were the first to automatically learn syntactic dependency path
features with neural nets.
Their semantic role labeling model ran an LSTM on the path between every
candidate pair of nodes, and was learned end-to-end.
This strategy works well for arborescences, which are relatively sparse,
and where most of the predictive
information lives on the direct shortest path between two nodes.
But is it possible to capture information that lives \emph{outside}
the shortest path?
What if there is no shortest path, or there are multiple?
This situation arises as more NLP preprocessing is conditioned on\ldots
for instance, one could add predicted discourse trees, or named entity recognition,
and then automatically learn features from the union of all the preprocessed
structures.
\KGEN\ encodes and aggregates \emph{all} paths between every pair of nodes (of any length,
even paths with loops), so it is able to handle multigraphs, and capture ``off the beaten path''
information.
Because the recurrence of our path-RNN and the pooling function we use to
aggregate multiple paths together follow algebraic laws~(\S\ref{ssec:kgen:kgen:bg}),
we are able to calculate and aggregate encodings for all paths efficiently.

Along this line, Toutanova et al. \shortcite{toutanova-EtAl:2016:P16-1} introduced a model for
knowledge base completion that uses a \emph{bilinear} composition function on paths.
Their model includes a dynamic program to score all paths up to a fixed length.
Our method generalizes and improves on \cite{toutanova-EtAl:2016:P16-1} in a few key ways:
\begin{itemize}
    \item Instead of calculating and aggregating scalar scores directly for paths, we calculate tensor representations.
    Arbitrary (i.e., non-linear) downstream modules can then be used on the output of \KGEN,
    allowing for more powerful function learning without losing efficient inference.
    \item We relax the \emph{bilinear} requirement, requiring only that the recurrence and pooling
    functions together follow the \emph{semiring} laws~(\S\ref{ssec:kgen:kgen:bg}).
    Bilinear functions are a special case
    under the sum-product semiring, but we experiment with other useful semirings.
    \item Specifically, by working with a max-pooling semiring instead of a sum-pooling one, we have a natural
    extension into a \emph{star semiring} without fear of overflows.
    We can then encode all paths of \emph{any} length between a given pair of nodes, even loopy paths.
    We hypothesize that this is important for encoding graph structures that deviate from the shortest
    path between two nodes.
\end{itemize}


\paragraph{Invariance to node reorderings}
Our approach and GCNs have the advantage that they can be agnostic to node order when
desired.
Nodes are unordered by definition in general graphs.\footnote{%
One can always opt in to order-sensitivity by adding additional
synthetic arcs, as in the \textit{``+''} arcs in Figure~\ref{fig:fn-dm-ud}
and the \textit{``next''} arcs in Figure~\ref{fig:kgen:kgen:dusty-ud-adj}.}
Deep Sets \citep{zaheer_deep_2017} studied the similar goal of being invariant to
element ordering when encoding sets.
They argue that encoding elements into $\R^d$, sum-pooling,
then running an arbitrary non-linear function such as an MLP can be a universal function approximator on subsets,
when $d$ is at least the size of the set to be encoded.
Follow up work showed $d$ \emph{must} be as large as the set \citep{wagstaff_limitations_2019}.
We note that because semiring $\splus$ is commutative and associative by law, this same goal is accomplished.
Attention is also order invariant, a property which graph attention networks \cite{velickovic_graph_2018}
make use of.
\sam{transformers!}
These are all special cases of invariance under group actions \cite{bloem-reddy_probabilistic_2019},
the group action being permutation.

Another approach to contextualizing a node is to linearize its neighbors,
and then run an (order-sensitive) RNN over them~\citep{peng_cross-sentence_2017,zayats_conversation_2017}.\footnote{%
An extreme version of this is to linearize the entire graph
and encode it with an RNN~\citep{niepert_learning_2016,konstas_neural_amr_2017,zellers_neural_2018}.%
}
An ordering of the nodes must be chosen though,
even if there is no natural ordering.\footnote{
In \citet{zellers_neural_2018} we found that our model for scene graph parsing
was relatively robust to ordering schemes, but we did observe differences.
}
Instead of choosing just one ordering, multiple permutations could be sampled,
as in Janossy pooling \cite{murphy_janossy_2018}, but this comes at a high computational
cost and only achieves approximate order invariance.
We believe it is worth exploring approaches that are order invariant by construction,
and finding the expressive limits of such approaches.

