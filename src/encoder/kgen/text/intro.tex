\begin{figure}
  \centering
  \includegraphics[width=.95\textwidth]{encoder/kgen/fig/dusty_dm_ud.pdf}
  \caption{
    An example sentence annotated with semantic dependencies from \term{DM} \citep[][\textit{top/red}]{oepen2014sdp}
    and syntax from \term{Universal Dependencies} \citep[][\textit{bottom/green}]{silveira14gold}.
  }
  \label{fig:kgen:kgen:dusty-dm-ud}
\end{figure}


\setcounter{footnote}{0}
\astfootnote{\textit{%
The work described in Section~\ref{sec:kgen:kgen} was done with Hao Peng,
Jan Buys, and Noah A.~Smith.}}%
In this section, we introduce a new class of graph neural networks, called
\term{Kleene Graph Encoder Networks (KGEN)}.
KGEN is a model architecture that generalizes \SoftP\~(\S\ref{sec:kgen:sopa}),
and extends it in order to encode \emph{graphs} instead of sequences.
In the same way that an RNN recursively propagates information forward (or backward) in a
sentence through adjacent tokens, graph neural networks propagate information
between adjacent nodes in a graph.
Graphs are extremely general, and a good general purpose
graph encoder has many potential uses.
Graph convolutional networks, for example, were originally introduced in order to do
semi-supervised learning on citation networks and on a knowledge
graphs~\cite{defferrard_convolutional_2016,kipf_semi-supervised_2016},
but have since been used for a variety
of tasks, including machine translation~\citep{bastings_graph_2017}, named
entity recognition~\citep{cetoli_graph_2017}, semantic role
labeling~\citep{marcheggiani_encoding_2017}, and multi-document
summarization~\citep{yasunaga_graph-based_2017}, among others.


While most existing graph neural networks learn contextualized representations
of \emph{nodes}, our model learns contextualized representations of \emph{paths}.
We argue that this is a finer-grained level of representation (from which node
representations can be recovered), and so has more potential uses.
It is an especially natural level of representation when used for the task of
predicting arcs over the same set of nodes as the input.
% , which we show experimentally in \S\ref{subsec:kgen:kgen:experiments}.
We describe the most general form of the model
architecture~(\S\ref{ssec:kgen:kgen:model}) in terms of star semirings, an
algebraic view of the model and accompanying algorithm which draws inspiration
from prior work in NLP on semiring parsing~\citep{goodman_semiring_1999}.
We then describe the specific model we experiment with,
which uses the $\maxmul$ operation defined in the previous section (Equation~\ref{eqn:encoder:sopa:maxmul}).

We test our model by encoding a syntactic dependency graph, and using
the resulting representations to predict semantic dependencies~\citep{oepen2014sdp,oepen2015sdp}.
Our experiments compare our model, KGEN, to a GCN baseline, and a syntax-free baseline
on semantic dependency parsing (\S\ref{subsec:kgen:kgen:experiments}).


We make the following contributions:
\begin{itemize}
  \item We introduce \textbf{KGEN}, a new class of graph neural networks that captures features of
  \emph{all paths} in an input graph, efficiently, using a dynamic program.
  \item We are the first to formulate a machine learning algorithm in terms of \term{star semirings},
  expanding on the long line of semiring parsing work in
  NLP~\citep{goodman_semiring_1999,eisner-goldlust-smith:2004:ACLdemo}.
  We show how the \emph{star} in star semirings can be used to calculate
  otherwise infinitely loopy computations in cubic time.
  \item We show experimentally that KGEN is effective at capturing
  both syntactic and linear order context, improving a semantic dependency parser
  more than a graph convolutional network baseline.
%   \item We show that KGEN results in interpretable models when trained.
\end{itemize}



\begin{figure}
  \begin{subfigure}[b]{.4\columnwidth}
    \centering
    \label{fig:kgen:kgen:dusty-dm-ud-paths-arg1}
    \includegraphics[height=1.41 in]{encoder/kgen/fig/dusty_dm_ud_paths_arg1.pdf}
    \caption{An \arclabel{arg1} DM arc corresponding to a length 2 syntactic path.}
  \end{subfigure}
  \begin{subfigure}[b]{.4\columnwidth}
    \centering
    \label{fig:kgen:kgen:dusty-dm-ud-paths-loc}
    \includegraphics[height=2.41 in]{encoder/kgen/fig/dusty_dm_ud_paths_loc.pdf}
    \caption{A \arclabel{loc} DM arc corresponding to a length 2 syntactic path.}
  \end{subfigure}
  \caption{
    We highlight two DM arcs that do not have a directly corresponding syntactic arc, but do
    have a corresponding \emph{path} of syntactic arcs.
  }
  \label{fig:kgen:kgen:dusty-dm-ud-paths}
\end{figure}



