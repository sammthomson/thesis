
\isection{Background}{background}

\paragraph{Surface patterns.} 
Patterns \cite{Hearst:1992} are particularly useful tool in NLP  \interalia{Lin:2003,Etzioni:2005,Schwartz:2015}. 
The most basic definition of a pattern is a sequence of words and wildcards (e.g., ``\textbf{X} is a \textbf{Y}''), which can either be manually defined or extracted from corpus using cooccurrence statistics. 
Patterns are then used to be  matched against a specific text span by replacing wildcards with concrete words. 

\citet{Davidov:2010} introduced a \emph{flexible} notion of patterns, which supports partial matching of the pattern with a given text by skipping some of the words in the pattern, or introducing new words. 
In their framework, when a sequence of text partially matches a  pattern, hard-coded partial scores are assigned to the pattern match.
Here, we represent patterns as WFSAs with neural weights, and support these partial matches in a soft manner.
%We use an ensemble of patterns to collectively encode a document, learning them end-to-end for a document classification task.


\paragraph{WFSAs.}
We review weighted finite-state automata with
\epstrans s before we move on to our special case in \secref{soft-patterns}.
A WFSA\=/$\epsilon$ with $d$ states over a vocabulary $V$ is formally defined as a 
tuple $F = \langle \tensor{\pi}, \tensor{T}, \tensor{\eta} \rangle$, where
$\tensor{\pi} \in \R^{d}$ is an initial weight vector, $\tensor{T} : (V \cup \{\epsilon\}) \rightarrow \R^{d \times d}$ is a transition weight function, and $\tensor{\eta} \in \R^{d}$ is a final weight vector.
Given a sequence of words in the vocabulary $\seq{x} = \langle x_1, \ldots, x_n \rangle$,
the Forward algorithm \cite{baum_statistical_1966} scores $\seq{x}$ with respect to $F$.
Without \epstrans s, Forward can be written as a series of matrix multiplications:
\begin{equation}
\label{eqn:kgen:sopa:forward_no_eps}
p_{\text{span}}^{\prime}(\seq{x}) =
  \tensor{\pi}^\top
  \left(\prod_{i=1}^n{\tensor{T}(x_i) }\right)
  \tensor{\eta}
\end{equation}
\epstrans s are followed without consuming a word, so Equation~\ref{eqn:kgen:sopa:forward_no_eps} must be
updated to reflect the possibility of following any number (zero or more) of
\epstrans s in between consuming each word:\begin{equation}
\label{eqn:kgen:sopa:forward}
p_{\text{span}}(\seq{x}) =
  \tensor{\pi}^\top \tensor{T}(\epsilon)^*
    \left(\prod_{i=1}^n{
      \tensor{T}(x_i) \tensor{T}(\epsilon)^*
    }\right)
    \tensor{\eta}
\end{equation}
where $^*$ is matrix asteration: $A^* \coloneqq \sum_{j=0}^\infty{A^j}$.
In our experiments we use a first-order approximation,
$A^* \approx I + A$, which corresponds to allowing zero or one \epstrans\ at a time.
% \footnote{We allow at most
% one \epstrans\ at a time.
% This corresponds to a first-order approximation
% $\tensor{T}(\epsilon)^* \approx \tensor{I} + \tensor{T}(\epsilon)$.
% It avoids infinite values, and keeps $\tensor{T}(\epsilon)^*$ sparse and finite when $\tensor{T}(\epsilon)$ is.
% }
When the FSA $F$ is probabilistic, the result of the Forward algorithm
can be interpreted as the marginal probability of all paths
through $F$ while consuming $\seq{x}$ (hence  the symbol ``$p$'').

The Forward algorithm can be generalized to any semiring \citep{eisner_parameter_2002}, a fact
that we make use of in our experiments and analysis.\footnote{The semiring parsing view \cite{goodman_semiring_1999} has produced unexpected connections in the past
\cite{eisner_inside_2016}.
We experiment with max-times and max-plus semirings, but note that
our model could be easily updated to use any semiring.
}
The vanilla version of Forward uses the probability semiring: $\oplus = +$, $\otimes = \times$.
\com{Let $\A$ be a semiring with operations 
We first overload $\otimes$ to be a binary operation on matrices. 
Given two matrices $\tensor{A} \in \A^{\ell \times m}$ and $\tensor{B} \in \A^{m \times n}$, let
$\tensor{A} \otimes \tensor{B} \in \R^{\ell \times n}$ be:
\begin{equation}
\label{eqn:smatmul}
[\tensor{A} \otimes \tensor{B}]_{i,j} =
  \bigoplus_{k=1}^m{\left(
    A_{i,k} \otimes B_{k,j}
  \right)}
\end{equation}
Equation~\ref{eqn:smatmul} is analogous to matrix multiplication, replacing addition with $\oplus$ and multiplication with $\otimes$.
The Forward algorithm can then be written:
\begin{equation}
s_{\text{span}}(\seq{x}) =
  \tensor{\pi}^\top \otimes
    \tensor{T}(\epsilon)^* \otimes
    \left(\bigotimes_{i=1}^n{
      \tensor{T}(x_i) \otimes
      \tensor{T}(\epsilon)^*
    }\right)
    \otimes \tensor{\eta}
\end{equation}}
A special case of Forward  is the Viterbi algorithm \cite{viterbi_error_1967}\com{---$\A = \R$ and }, which sets $\oplus = \max$. Viterbi finds the \emph{highest scoring} path
through $F$ while consuming $\seq{x}$.
Both Forward and Viterbi have runtime $O(d^3 + d^2 n),$\resolved{\roy{You said in the previous footnote that $A^*$ is computed in $O(d^3)$, but that we use a first-order approximation. Does this mean complexity for $A^*$ is lower? let's say this explicitly if so, as this is currently a bit confusing}}
requiring just a single linear pass through the phrase. 
Using first-order approximate asteration, this runtime drops to $O(d^2 n)$.\footnote{In our case, we also use a sparse transition matrix (\subsecref{soft-patterns:as_wfsas}), which further reduces our runtime to $O(dn)$.}

Finally, we note that Forward scores are for \emph{exact matches}---the entire phrase must be consumed.
We show in \subsecref{soft-patterns:encoding} how phrase-level scores can be summarized into a document-level score.

%\royb{WFSAs and HMMS?} 

