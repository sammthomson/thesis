
\isectionb{Interpretability}
%We have shown so far that \SoftP has comparable performance to BiLSTMs on two out of three datasets, and can even outperform them on small datasets.
We turn to another key aspect of \SoftP---its interpretability. 
We start by demonstrating how we interpret a single \softp, and then  describe
how to interpret the decisions made by downstream classifiers that
rely on \SoftP---in this case, a sentence classifier.
Importantly, these visualization techniques are equally applicable to CNNs.

 \paragraph{Interpreting a single pattern.}
In order to visualize a pattern, we compute the pattern matching scores with each phrase in our training dataset, 
and select the $k$ phrases with the highest scores.
\resolved{\nascomment{I like all these examples.  Can we have A LOT MORE?  like
  a half page column figure? maybe show 5 neighbors instead of 3?}}
\tabref{patt-interpretability} shows examples of six patterns learned using the best \SoftP model on the SST dataset,
as represented by their five highest scoring phrases in the training set.
A few interesting trends can be observed from these examples. 
First, it seems our patterns encode semantically coherent expressions.
A large\resolved{\nascomment{two out of three is not ``significant''
  especially if you cherrypicked}} portion of them correspond to sentiment (the five top examples in the table),
but others capture different semantics, e.g., time expressions.

Second, it seems our patterns are relatively soft, and  allow lexical flexibility. 
While some patterns do seem to fix specific words, e.g., ``of'' in the first example or ``minutes'' in the last one, 
even in those cases some of the top matching spans replace these words with other, similar words (``with'' and ``half-hour'', respectively).
%In fact, in these examples there isn't even a single word that appears in the same slot in all examples.
%in general they are much softer than the more traditional hard-patterns.
Encouraging \SoftP to have more concrete words, e.g., by jointly learning the word vectors, might make \SoftP useful in other contexts, particularly as a decoder.
We defer this direction to future work.

\resolved{\nascomment{Also suggest learning word vectors w/in \SoftP?}}
Finally, \SoftP makes limited but non-negligible use of self-loops and epsilon steps.
Interestingly, the second example shows that one of the patterns had an \epstrans\ at the same place in every phrase.
This demonstrates a different function of \epstrans s than originally designed---they allow a \softp to %match spans shorter than the number of states,
effectively shorten itself, by learning a high \epstrans\ parameter for a certain state.
\resolved{\nascomment{Say something about parameter tying across related but different length patterns.}\roy{Not entirely sure what you mean by that}}

\begin{table}[t]
\ra{1.2}
\newcommand{\ep}{{\color{red}{$\epsilon$}} }
\newcommand{\sloop}[1]{{\color{blue}{#1$_{\textit{SL}}$}} }
\setlength{\tabcolsep}{1.5pt}
% \scriptsize
\begin{tabularx}{\linewidth}{@{}l c | lllll@{}}
  \toprule
  {\bf Pattern} & & \multicolumn{5}{c}{\bf Highest Scoring Phrases} \\
  \midrule
  \multirow{5}{*}{Pattern~1} 
  && ~thoughtful &     ,        &       reverent  &      portrait  &      of  \\
  && ~and  &           astonishingly&   articulate&      cast&            of     \\
  && ~entertaining&    ,          &     thought-provoking& film&            with       \\ 
  && ~gentle&        ,   &           mesmerizing&  portrait&  of \\
  && ~poignant&        and&             uplifting&       story&           in \\

  \midrule

  \multirow{5}{*}{Pattern~2}  
  && ~'s &           \ep &                uninspired &      story &           .  \\
  && ~this&           \ep&                bad&            on&             purpose \\ 
  && ~this&           \ep&                 leaden&          comedy&          .      \\
  && ~a&              \ep&                 half-assed&      film&            .         \\ 
  && ~is&             \ep&                 clumsy         \sloop{,}&               the&             writing      \\

  \midrule

 \multirow{5}{*}{Pattern~3} 
 && ~mesmerizing&     portrait    &    of&              a     &         \\ 
 && ~engrossing      &portrait    &    of    &          a     &         \\
 && ~clear-eyed      &portrait     &   of     &         an&             \\
 && ~fascinating    & portrait       & of        &      a&              \\ 
 && ~self-assured     & portrait&        of&              small& \\
  \midrule


\multirow{5}{*}{Pattern~4}
&& ~honest&          ,             & and&            enjoyable& \\
&& ~soulful&        ,         \sloop{scathing} &      and        &    joyous& \\
&& ~unpretentious&  ,          \sloop{charming} &      ,   &           quirky &        \\
&& ~forceful&  ,          &     and&  beautifully&\\
&& ~energetic& ,          &     and&            surprisingly& \\

  \midrule

\multirow{5}{*}{Pattern~5}
&& ~is &             deadly&          dull&& \\
&& ~a&               numbingly&       dull        &&   \\
&& ~is&             remarkably & dull&& \\
&& ~is   &           a             &  phlegmatic&& \\
&& ~an   &           utterly&         incompetent&& \\

  \midrule

  \multirow{5}{*}{Pattern~6}
  && ~five & minutes&&& \\
  && ~four &minutes&&& \\
  && ~final &          minutes&&& \\
  && ~first& half-hour&&& \\
  && ~fifteen&         minutes&&& \\

  \bottomrule
\end{tabularx}
\caption{\label{patt-interpretability}
Six patterns of different lengths learned by \SoftP on SST.
Each group represents a single pattern $p$, and shows the five phrases in
the training data that have the highest score for $p$.
Columns represent pattern states.
% {\bf Pattern Length} is the number of states of the pattern.
Words marked with \sloop{}are self-loops.
\ep{}symbols indicate \epstrans s.
All other words are from \happy\ transitions.}
\end{table}
%\vspace{-0.35cm}

 \paragraph{Interpreting a document.}
\SoftP provides an interpretable representation of a document---a vector of the maximal matching score of each pattern with any span in the document.
To visualize the decisions of our model for a given document, we can observe the patterns and corresponding phrases that score highly within it.


To understand which of the $k$ patterns contributes most to the classification decision, we apply a leave-one-out method.
We run the forward method of the MLP layer in \SoftP $k$ times, each time zeroing-out the score of a different pattern $p$.
The difference between the resulting score and the original model score is considered $p$'s contribution.
%, defined to be the difference between the probability given to the positive label when using all patterns vs.~when leaving $p$ out.
%This difference allows us to quantify the contribution of $p$ to the final decision.
Table~\ref{table:examples} shows example texts along with their most positive and negative contributing phrases.

\newcommand{\positive}[1]{{\textcolor{DarkGreen}{\textbf{#1}}}}
\newcommand{\negative}[1]{{\textcolor{orange}{\textit{#1}}}}

\begin{table}[t]
\ra{1.2}
\begin{tabularx}{\linewidth}{@{}p{\columnwidth}@{}}
\toprule

\textbf{Analyzed Documents} \\
\midrule%[\cmidrulewidth]
\negative{it 's dumb ,} \positive{but more importantly} , \negative{it 's just not scary}\\
\midrule[\cmidrulewidth]
% starting the sentence with posive affects the spacing above it for some reason
\positive{}though moonlight mile is replete with \positive{acclaimed actors and actresses} and tackles a subject that 's \positive{potentially moving} , the movie is \negative{too predictable} and \negative{too self-conscious to reach a} level of \positive{high drama} \\
\midrule[\cmidrulewidth]
\positive{}While \positive{its careful pace and} seemingly \negative{opaque story} may not satisfy every moviegoer 's appetite, the film 's final scene is \positive{soaringly , transparently moving} \\
\midrule[\cmidrulewidth]
\positive{unlike the speedy wham-bam} effect \negative{of most hollywood offerings} , character development -- and more \positive{importantly, character empathy} -- \positive{is at the heart of} italian for beginners~. \\
\midrule[\cmidrulewidth]
\positive{the band 's courage in} the face of official repression \positive{is inspiring} , \positive{especially for} aging \negative{hippies} ( this one included )~. \\
\bottomrule
\end{tabularx}
\caption{\label{table:examples}
Documents from the SST training data.
Phrases with the largest contribution toward a positive
sentiment classification are in {\positive{bold green}}, and
the most negative phrases are in {\negative{italic orange}}.
% \SoftP can be seen to focus on spans that are relevant to the classification decision.
}
\end{table}



\resolved{\nascomment{I like the interpretability section a lot.  in table 2,
  can we add some information about positive/negative valence?}
  \roy{These patterns are not computed with respect to any label, but give an absolute score to each document}}
\resolved{\nascomment{table
  3 is great, but can we use some additional cues -- not just color --
  to distinguish what's green and orange?  reviewers complain about
  using just color and it's in the style guide now.}}
  