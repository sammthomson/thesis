\section{Sequence Encoding: \SoftP}
\label{sec:kgen:sopa}

\astfootnote{\textit{%
The work described in Section~\ref{sec:kgen:sopa} was done with
Roy Schwartz and Noah A.~Smith, and published as \citet{schwartz-EtAl:2018:ACL}.}}%
Recurrent neural networks (RNNs; \citealp{Elman:1990}) and convolutional neural networks (CNNs; \citealp{lecun_gradient-based_1998}) 
are two of the most useful text representation methods in NLP \citep{Goldberg:2016}.
These methods are generally considered to be quite different: 
the former encodes an arbitrarily long sequence of text, and is highly expressive \citep{Siegelmann:1995}.
The latter is more local, encoding fixed length windows, and accordingly less expressive.
In this paper, we seek to bridge the  expressivity gap between RNNs and CNNs, presenting \term{\SoftP} (for \textbf{So}ft {\bf Pa}tterns), a model that lies in between them.

\SoftP is a neural version of a weighted finite-state automaton (WFSA), with a restricted set of transitions.
Linguistically, \SoftP is appealing as it is able to capture a soft notion of surface
patterns (e.g., \textit{``what a great \textbf{X} !''}, \citealp{Hearst:1992}), where some words may be dropped, inserted, or replaced with similar words (see \figref{WFSA}).
From a modeling perspective, \SoftP{} is interesting because WFSAs are well-studied, and  they come with efficient and flexible inference algorithms \interalia{mohri_finite_1997,eisner_parameter_2002}.

\input{encoder/fig/example-fsa}


\SoftP defines a set of soft patterns of different lengths, with each pattern represented as a WFSA  (\secref{soft-patterns}). 
While the number and length of the patterns are hyperparameters, the patterns themselves are learned end-to-end.
\SoftP then represents a document with a vector that is the aggregate of the scores computed by matching each of the  patterns with each span in the document.
\com{Importantly, due to its reliance on WFSAs, \SoftP is less expressive than a single-layer RNN, which is Turing complete \cite{Siegelmann:1995}.}
\com{Interestingly, our experiments show that our model uses far fewer parameters than a BiLSTM baseline, while reaching the same or better results. 
In particular, \SoftP is considerably better than a BiLSTM in small data settings.}

We then show that \SoftP is in fact a generalization of a one-layer CNN (\secref{CNN}).
Accordingly, \SoftP is more expressive than a one-layer CNN, and can encode flexible-length text spans.
We show that such CNNs can be thought of as a collection of linear-chain WFSAs, each of which can only match fixed-length spans.
% As a model that is more expressive than a CNN, \SoftP is a step toward paving the bridge from CNNs to RNNs.





To test the utility of \SoftP, we experiment with three text classification tasks (\secref{Experiments}).
We compare against four baselines, including both a bidirectional LSTM and a CNN. 
Our model performs on par or better than all baselines on all tasks (\secref{Results}). 
Moreover, when training with smaller datasets, \SoftP is particularly useful, outperforming all models by substantial margins.
\proposalonly{
In the full thesis, we also offer a new, simple method to interpret \SoftP, which we omit in this proposal for space.
}
\thesisonly{
Finally, building on the connections discovered in this paper, we offer a new, simple method for the interpretation of \SoftP (\secref{Interpretability}).
This method applies equally well to CNNs.\footnote{We release our code at \url{http://anonymous}}
}
