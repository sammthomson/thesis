\isectionb{Experiments}

\resolved{\nascomment{I don't think we ever said exactly how many patterns we're
  training with!  Would be really nice to show curves with varying
  number of patterns, too.}}

To evaluate \SoftP, we  apply it to text classification tasks. %, on which hard patterns have been shown to be useful in the past (see \secref{related}).
Below we describe our datasets and baselines.
\thesisonly{More details can be found in \appref{Experimental}.}

\paragraph{Datasets.}
We experiment with three binary classification datasets. 

\begin{itemize}%[itemsep=0pt,topsep=3pt,leftmargin=*]
\item {\bf SST}. The Stanford Sentiment Treebank \cite{Socher:2013}\camready{\footnote{\url{https://nlp.stanford.edu/sentiment/index.html}}} contains roughly 10K movie reviews from Rotten Tomatoes,\camready{\footnote{\url{http://www.rottentomatoes.com}}} labeled on a scale of 1--5.
We consider the binary task, which considers 1 and 2 as
negative, and 4 and 5 as positive (ignoring 3s).
It is worth noting that this dataset also contains
syntactic phrase level annotations, providing a sentiment label to parts of sentences. 
In order to experiment in a realistic setup, we only consider the complete sentences,
and ignore syntactic annotations at train or test time.
The number of training/development/test sentences in the dataset is 6,920/872/1,821.

\item{\bf Amazon}. The Amazon Review Corpus \cite{McAuley:2013}\camready{\footnote{\url{http://riejohnson.com/cnn_data.html}}} contains electronics product reviews\camready{,
a subset of a larger review dataset}.
Each document in the dataset contains a review and a summary. 
Following \citet{Yogatama:2015}, we only use the reviews part, focusing on positive and negative reviews.
The number of training/development/test samples is 20K/5K/25K.

\item{\bf ROC}. The ROC story cloze task \cite{Mostafazadeh:2016} is a story understanding task.\camready{\footnote{\url{http://cs.rochester.edu/nlp/rocstories/}}}
The task is composed of four-sentence story prefixes, followed by two competing endings: one that makes the joint five-sentence story coherent, and another that makes it incoherent. 
Following \citet{Schwartz:2017}, we treat it as a style detection task: %and rather than attempting to solve the task itself (i.e., decide between two endings to the same story),
we treat all ``right'' endings as positive samples and all ``wrong'' ones as negative, and we ignore the story prefix. 
We split the development set into train and development (of sizes 3,366 and 374 sentences, respectively), and take the test set as-is (3,742 sentences).
\end{itemize}

\paragraph{Reduced training data.}
In order to test our model's ability to learn from small datasets, we also randomly sample 100, 500, 1,000 and 2,500 SST training instances and 
 100, 500, 1,000, 2,500, 5,000, and 10,000 Amazon training instances.
Development and test sets remain the same.

\paragraph{Baselines.}
We compare to four baselines: 
a BiLSTM, a one-layer CNN, DAN (a simple alternative to RNNs) and a feature-based classifier trained with hard-pattern features.
%In all neural cases, the learned representation is fed to an MLP.
%We describe the baselines below.

\begin{itemize}%[itemsep=0pt,topsep=3pt,leftmargin=*]
\item{\bf BiLSTM.} 
Bidirectional LSTMs have been successfully used in the past for text classification tasks \cite{Zhou:2016}.
We learn a one-layer BiLSTM representation of the
document, and feed the average of all hidden states to an MLP. \resolved{\nascomment{say a bit more; who else has done this and how well
  has it worked?}}

\item{\bf CNN}.
CNNs are particularly useful for text classification \cite{kim_convolutional_2014}.
We train a one-layer CNN with max-pooling, and feed the resulting representation to an MLP.


\item{\bf DAN}. 
We learn a deep averaging network with word dropout \cite{Iyyer:2015}, a simple but strong text-classification baseline.

\item{\bf Hard}. 
We train a logistic regression classifier with hard-pattern features. 
Following \citet{Tsur:2010}, we replace low frequency words with a special wildcard symbol.
We learn sequences of 1--6 concrete words,
where any number of wildcards can come between two adjacent 
words. \camready{We consider words occurring with frequency of at least 0.01\% of our training set as concrete words, and words occurring in frequency 1\% or less as wildcards.\footnote{Some words may serve as both words and wildcards. See \citet{Davidov:2008} for discussion.}}

\end{itemize}


\paragraph{Number of patterns.}

\SoftP requires specifying the number of patterns to be learned, and their lengths.
Preliminary experiments showed that the model doesn't benefit from more than a few dozen patterns.
We experiment with several configurations of patterns of different lengths, generally considering 0, 10 or 20 patterns of each pattern length between 2--7.
The total number of patterns learned ranges between 30--70.\footnote{The number of patterns and their length are hyperparameters tuned on the development data\thesisonly{ (see \appref{Experimental})}.}

\isectionb{Results}

\tabref{tab:results} shows our main experimental results. 
In two of the cases (SST and ROC), \SoftP outperforms all models.
On Amazon, \SoftP performs within 0.3 points of CNN and BiLSTM, and outperforms the other two baselines.\resolved{\nascomment{it's not final in the table; maybe fix
 table order to reflect the text?}}
The table also shows the number of parameters used by each model for each task. 
Given enough data, models with more parameters should be expected to perform better.
However, \SoftP performs better or roughly the same as a BiLSTM, which has  $3$--$6$ times as many parameters.


\begin{table}[t]
\ra{1.2}
\centering
\begin{tabularx}{\linewidth}{@{}l X X X  @{}}
  \toprule
  {\bf Model} & {\bf ROC} & {\bf SST} & {\bf Amazon}   \\
  \midrule
  {\bf Hard} & 62.2 (4K) & 75.5 (6K) & 88.5 (67K)   \\
  {\bf DAN} & 64.3 (91K)& 83.1 (91K) & 85.4 (91K) \\
  {\bf BiLSTM} & 65.2 (844K)& 84.8 (1.5M) & {\bf 90.8} (844K) \\
  {\bf CNN} & 64.3 (155K) & 82.2  (62K) & 90.2 (305K) \\
  \midrule
  {\bf \SoftP} & {\bf 66.5} (255K)& {\bf 85.6} (255K) & 90.5 (256K)  \\
  \midrule
    {\bf ${ms}\setminus$$\{\sigma\}$ } & 64.4 & 84.8 & 90.0 \\
  {\bf ${ms}\setminus$$\{\sigma, sl\}$} & 63.2 & 84.6 & 89.8  \\
  {\bf ${ms}\setminus$$\{\sigma, \epsilon\}$} & 64.3 & 83.6 & 89.7 \\
  {\bf ${ms}\setminus$$\{\sigma, sl,\epsilon\}$} & 64.0 & 85.0 & 89.5\\
%  {\bf ${ms}^*\setminus$$\{\sigma, sl,\epsilon\}$} &  63.3 & 84.0 & 90.0\\

  \bottomrule
\end{tabularx}
\caption{\label{tab:results} \resolved{\nascomment{Drop percentage signs in table. Bold (significant) best numbers.}}Test classification accuracy (and the number of parameters used).
The bottom part shows our ablation results:  \SoftP{}: our full model. 
${ms}$: running with max-sum semiring (rather than max-times).
$sl$: self-loops, $\epsilon$: $\epsilon$ transitions, $\sigma$: sigmoid transition (see Equation~\ref{eqn:main-self-transitions}). 
The final row is equivalent to a one-layer CNN.}
\end{table}


\input{encoder/fig/small-data-plot}


Figure \ref{fig:training-size} shows a comparison of all models on the SST and Amazon 
datasets with varying training set sizes. 
\SoftP is substantially outperforming all baselines, in particular BiLSTM, on small datasets (100 samples).
This suggests that \SoftP, being less expressive than BiLSTM, is better fit to learn from small datasets.
%With more training samples, the other models improve.%, BiLSTM especially.
%With access to  a few thousand samples, BiLSTM is starting to
%outperform our model.
\resolved{\nascomment{maybe reiterate the comparison in \# parameters for the
  small data case?  I bet bilstm has 10x as many parameters here ...}\roy{Not the case unfortunately}}

\paragraph{Ablation analysis.}
%We seek to measure the contribution of the different components of \SoftP, in particular the elements that differentiate it from CNNs.
\tabref{tab:results} also shows an ablation of the differences between \SoftP and CNN:
max-times semiring with sigmoid vs.~max-sum semiring, self-loops, and \epstrans s.
The last line is equivalent to a CNN with multiple window sizes.\resolved{\nascomment{why are the numbers different than CNN?}}
Interestingly, the most notable difference between \SoftP and CNN is the semiring, while $\epsilon$ transitions and self-loops have little effect on performance.\footnote{Although \SoftP does make use of them\thesisonly{---see \secref{Interpretability}}.}
% We defer the further exploration of the impact of these components to future work.

