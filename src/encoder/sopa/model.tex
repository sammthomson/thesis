\isection{\SoftP: A Weighted Finite-state Automaton RNN}{soft-patterns}

We introduce \SoftP,  a WFSA-based RNN,
which is designed to represent text as collection of surface pattern occurrences. 
We start by showing how a single \softp can be represented as a WFSA\=/$\epsilon$ (\subsecref{soft-patterns:as_wfsas}).
Then we describe how to score a complete document using a \softp (\subsecref{soft-patterns:encoding}).
Finally, we describe how multiple \softps can be used to encode a document (\subsecref{soft-patterns:classifying}).



\isubsection{Patterns as WFSAs}{soft-patterns:as_wfsas}

We describe how a \softp can be represented as a WFSA\=/$\epsilon$.
We first assume a single \softp. 
A \softp \emph{is} a WFSA\=/$\epsilon$, but we impose hard constraints on its shape, and its transition weights are given by differentiable functions that have the power to capture concrete words, wildcards, and everything in between.
Our model is designed to behave similarly to  flexible hard
patterns (see \secref{background}), but to be learnable directly and ``end-to-end''
through backpropagation.
Importantly, it will still be interpretable as simple, almost linear-chain, WFSA\=/$\epsilon$.

Each pattern has a sequence of $d$ states (in our experiments we use patterns of varying lengths between 2 and 7).
Each state $i$ has exactly three possible outgoing transitions: a
\term{self-loop}, which allows the pattern to consume a word without
moving states, a \term{\happy}  transition to state $i+1$ which allows the
pattern to consume one token and move forward one state, and an
\term{\epstrans} to state $i+1$, which allows the pattern to move
forward one state without consuming a token.
All other transitions are given score 0.
When processing a sequence of text with a pattern $p$, we start with a special \StartState\ state, and only move forward (or stay put), until we reach the special \EndState\ state.\footnote{To ensure that we start in the \StartState\ state and end in the \EndState\ state, we fix $\pi=[1,0,\dots,0]$ and $\eta=[0,\dots,0,1]$.}
A pattern with $d$ states will tend to match token spans of length $d - 1$ (but possibly shorter spans due to \epstrans s, or longer spans due to self-loops).
See \figref{WFSA} for an illustration.


Our transition function, $\tensor{T}$, is a parameterized function that returns a $d \times d$ matrix.
For a word $x$:\begin{equation}
\label{eqn:main-self-transitions}
\left[\tensor{T}(x)\right]_{i,j} =
  \begin{cases}
    \sigma(\tensor{u}_{i} \cdot \tensor{v}_x + a_{i}), & \text{ if } j = i \textit{ (self-loop)} \\
    \sigma(\tensor{w}_{i} \cdot \tensor{v}_x + b_{i}), & \text{ if } j = i+1 \\ 
    0, & \text{ otherwise,}
  \end{cases}
\end{equation}
where $\tensor{u}_{i}$ and $\tensor{w}_{i}$ are vectors of parameters, $a_{i}$  and $b_i$ are scalar parameters, $\tensor{v}_x$ is a fixed pre-trained word vector for $x$, and $\sigma$ is the sigmoid function.
\epstrans s are also parameterized, but don't consume a token and depend only on the current state:\begin{equation}
\label{eqn:eps_transitions}
\left[\tensor{T}(\epsilon)\right]_{i,j} =
  \begin{cases}
    \sigma(c_{i}), & \text{ if } j = i+1 \\
    0, & \text{ otherwise,}
  \end{cases}
\end{equation}
where $c_i$ is a scalar parameter.\footnote{Adding \epstrans s to WFSAs does not increase their expressive power, and in fact slightly complicates the Forward equations.
We use them as they require fewer parameters, and make the modeling connection between (hard) flexible patterns and our (soft) \softps more direct and intuitive.
% Simulating \epstrans s could also be done by adding another non-zero diagonal to the  transition matrix ($\left[\tensor{T}(x)\right]_{i,i+2}$) at the expense of $(d - 1) |v_x|$ extra parameters.
}
As we have only three non-zero diagonals in total, the matrix multiplications in Equation~\ref{eqn:kgen:sopa:forward} can be implemented using vector operations, and the overall runtimes of Forward and Viterbi are reduced to $O(dn)$.\footnote{Our implementation is optimized to run on GPUs, so the observed runtime is even {\it sublinear} in $d$.}
%Lastly, to ensure that we start in the \StartState\ state and end in the \EndState\ state, we fix $\pi=[1,0,\dots,0]$ and $\eta=[0,\dots,0,1]$.

\paragraph{Words vs.~wildcards.}
Traditional  {\it hard} patterns distinguish between words and wildcards. 
Our model does not explicitly capture the notion of either, but the transition weight function is simple enough that it can be interpreted in those terms.

Each transition is a logistic regression over the next word vector $\tensor{v}_x$.
For example, for a \happy out of state $i$, $\tensor{T}$ has two parameters, $\tensor{w}_{i}$ and $b_{i}$.
If $\tensor{w}_{i}$ has large magnitude and is close to the word vector for some word $y$ (e.g., $\tensor{w}_i \approx 100 \tensor{v}_y$), and $b_i$ is a large negative bias (e.g., $b_i \approx -100$), then the transition is essentially matching the specific word $y$.
Whereas if $\tensor{w}_i$ has small magnitude ($\tensor{w}_i \approx
\tensor{0}$) and $b_i$ is a large positive bias (e.g., $b_i \approx
100$), then the transition is ignoring the current token and matching
 a wildcard.\footnote{A large bias increases the
eagerness to match \emph{any} word.}
The transition could also be something in between, for instance by
focusing on specific dimensions of a word's meaning encoded in the
vector, such as POS or semantic features like animacy or
concreteness \cite{\camready{Rubinstein:2015,}Tsvetkov:2015}.



\isubsection{Scoring Documents}{soft-patterns:encoding}

%We now describe how to score a complete document.
So far we described how to calculate how well a pattern matches a token span exactly (consuming the whole span).
To score a complete document, we prefer a score that aggregates over all matches on subspans of the document (similar to ``search'' instead of ``match'', in regular expression parlance).
We still assume a single \softp.

Either the Forward algorithm can be used to calculate 
$p_{\text{doc}}(\seq{x})$ = $\sum_{1\leq i \leq j \leq
  n}{p_{\text{span}}(\seq{x}_{i:j})}$,\footnote{With a probabilistic
  WFSA, this is the expected count of the pattern in the document.} or Viterbi to calculate
$s_{\text{doc}}(\seq{x})$ = $\max_{1\leq i \leq j \leq
  n}{s_{\text{span}}(\seq{x}_{i:j})}$.\footnote{This is the score of
  the highest-scoring match\com{, anywhere in the document}.}
In short documents, we expect patterns to typically occur at most once, 
% rather than repeat multiple times,
so we choose the Viterbi algorithm in our experiments.

\paragraph{Implementation details.}
%Here we describe the recurrence for calculating $s_\text{doc}$.
In our experiments we use the Viterbi inference in the probability semiring
(i.e., $\oplus=\max{},\otimes=\times$).
We give the specific recurrences we use to score documents in a single pass with this model.
We define:
\begin{equation}
\label{eqn:encoder:sopa:maxmul}
[\maxmul(\tensor{A}, \tensor{B})]_{i,j} =
  \max_{k}{
    A_{i,k} B_{k,j}
  }.
\end{equation}
We also define the following for taking zero or one \epstrans s:
\begin{equation}
\eps{(\tensor{v})} =
  \maxmul{\left(
    \tensor{v},
    \max(\tensor{I}, \tensor{T}(\epsilon))
  \right)}
\end{equation}
We keep a state vector $\tensor{h}_t$ at each token:
\begin{subequations}
\begin{align}
\label{eqn:state_eqns:start}
\tensor{h}_0 = &
  \eps(\tensor{\pi^\top}) \\
\label{eqn:state_eqns:next}
\tensor{h}_{t+1} = &
  \max{\left(
    \eps(\maxmul{(\tensor{h}_t, \tensor{T}(x_{t+1}))}),
    \tensor{h}_0
  \right)}\\
\label{eqn:state_eqns:score_t}
s_t = &
  \maxmul{(\tensor{h}_t, \tensor{\eta})} \\
\label{eqn:state_eqns:score_sum}
s_{\text{doc}} = &
  \max_{1 \leq t \leq n}{s_t}
\end{align}
\end{subequations}
$[\tensor{h}_t]_i$ represents the score of the best path through the pattern that ends in state $i$ after consuming $t$ tokens.
By including $\tensor{h}_0$ in Equation~\ref{eqn:state_eqns:next}, we are accounting for spans that start at time $t$.
$s_t$ is the maximum of the exact match scores for all spans ending at token $t$.
And $s_{\text{doc}}$ is the maximum score of any subspan in the document.
\resolved{\nascomment{it might be worth adding a point here (or later where we
  say we use Viterbi in the model itself) about the inductive
  bias we incorporate by choosing to focus the model on single
  matches.  the patterns we learn will be the ``kind of thing'' that
  happens around once in a document, not things that tend to repeat
  within one document.  that seems interesting to me.  but I'm tired.}}

\isubsection{Aggregating Multiple Patterns}{soft-patterns:classifying}

We describe how $k$ patterns are aggregated to score a document.
%We then traverse the document and assign a score to each subsequence of text with each of the patterns.
%For each pattern, we keep the highest score for any subsequence of the document. 
%Suppose we have $k$ patterns.
These $k$ patterns give $k$ different $s_{\text{doc}}$ scores for the document,
which are stacked into a vector $\tensor{z} \in \R^k$ and constitute the final document representation of \SoftP. 
This vector representation can be viewed as a feature vector.
In this paper, we feed it into a multilayer perceptron (MLP), culminating in a softmax to give a probability distribution over document labels.
We minimize cross-entropy, allowing the \SoftP and MLP parameters to be learned end-to-end.

\SoftP uses a total of $(2 e + 3) d k$ parameters\resolved{\nascomment{better notation for $|\tensor{v}_x|$?}},
where $e$ is the word embedding dimension, $d$ is the number of states and $k$ is the
number of patterns.
For comparison, an LSTM with a hidden dimension of $h$ has
$4 ((e + 1) h + h^2)$.
In \secref{Results} we show that \SoftP consistently uses fewer parameters than a BiLSTM baseline to
achieve its best result.


\begin{figure*}[t]
\begin{center}
\hspace{-0.3cm}
  \includegraphics[trim={0 0.1cm 0 0.18cm},clip,width=.95\textwidth]{encoder/fig/model.pdf}
\end{center}
%\setlength{\abovecaptionskip}{-.05cm}

\caption{
  \label{fig:model}
  State activations of two patterns as they score a document.
  pattern1 (length three) matches on \textit{`in years'}.
  pattern2  (length five) matches on \textit{`funniest and most likeable movie'}, using a self-loop to consume the token \textit{`most'}.
  Active states in the best match are marked with arrow cursors.
%   These best matching spans can be found using Viterbi with back-pointers.
}
\end{figure*}

\paragraph{\SoftP as an RNN.}
\SoftP can in fact also be considered as a simple RNN. 
As shown above, a pattern with $d$ states has a hidden state of size $d$. 
Stacking the $k$ hidden states of $k$ patterns into one vector of size $k \times d$ can be thought of as the hidden state of our model.
This hidden state is, like in any other RNN, dependent of the input and the previous state.
Using self-loops, the hidden state at time point $i$ can in theory depend on the entire document (see \figref{fig:model} for illustration).
Importantly, since all transitions pass through a sigmoid and so have scores strictly less than 1, 
our model is discouraged from following self-loops, only following them if it results in a better fit with the remainder of the pattern.\footnote{Using other semirings can result in different dynamics, potentially encouraging rather than discouraging self-loops.}

Although even single-layer RNNs are Turing complete~\cite{Siegelmann:1995},
\SoftP's expressive power is limited and determined by the semiring $\SR$ used during the Forward
algorithm.
When a WFSA is thought of as a function from finite sequences
of tokens to $\SR$, it is restricted to the class of functions known as
\term{rational series}~\cite{schutzenberger_definition_1961,droste_kleeneschutzenberger_1999,sakarovitch_rational_2009}.\footnote{%
Rational series generalize recognizers of \emph{regular languages}, which are
the special case where $\SR$ is the Boolean semiring.
}
It is unclear how limiting this theoretical restriction is in practice, especially when \SoftP
is used as a component in a larger network.
\com{
We note that while on its own, \SoftP{} is not able to represent every function
in $V^*\rightarrow \R$, it \emph{is} capable of losslessly encoding its
input~(see Appendix~\ref{sec:lossless-encode}).
When used as part of a larger network, if the remainder of the
network (a two-layer MLP in our experiments) is Turing complete, then the
entire network remains Turing complete.
}
% it is not as expressive as a one-layer rational-weight RNN, which is Turing complete \cite{Siegelmann:1995}.
% , which makes them less expressive than Turing complete systems.
Next, we show that \SoftP is a generalization of a one-layer CNN, thus more expressive.



\isection{CNN as \SoftP}{CNN}
A convolutional neural network \citep[\term{CNN};][]{lecun_gradient-based_1998} moves a fixed-size sliding window over the document, producing a vector representation for each window.
These representations are then often summed, averaged, or max-pooled to produce a document-level representation \interalia{kim_convolutional_2014,yin_multichannel_2015}.
In this section, we show that \SoftP is in fact a generalization of one-layer CNNs.

To recover a CNN from a soft pattern with $d+1$ states, we remove self-loops and \epstrans s, retaining only the main path transitions.
We also remove the sigmoid in the main path transition score (Equation~\ref{eqn:main-self-transitions}), and use the max-sum semiring when running Forward.
With only main path transitions, the network will %assign a score of $-\infty$\footnote{``0'' in the max-sum semiring.} to
not match any span that is not exactly $d$ tokens long.
Using max-sum, spans of length $d$ will be assigned the score:
\begin{subequations}
\begin{align}
\label{eqn:cnn-span}
  s_{\text{span}}(\seq{x}_{i:i+d})
    =& \sum_{j=0}^{d-1}{\tensor{w}_j \cdot \tensor{v}_{x_{i+j}} + b_j}, \\
    =& \tensor{w}_{0:d} \cdot \tensor{v}_{x_{i:i+d}}
      + \sum_{j=0}^{d-1}{b_j},
\end{align}
\end{subequations}
where $\tensor{w}_{0:d} = [ \tensor{w}_0^\top; \ldots ; \tensor{w}_{d-1}^\top ]^\top$,  $\tensor{v}_{x_{i:i+d}} = [ \tensor{v}_{x_i}^\top; \ldots; \tensor{v}_{x_{i+d-1}}^\top ]^\top$.
We can now recognize this as an affine transformation %with weights $[ \tensor{w}_i^\top; \ldots ; \tensor{w}_{i+d-1}^\top ]$ and bias term $\sum_{j=i}^{i+d-1}{b_j}$
of the concatenated word vectors $\tensor{v}_{x_{i:i+d}}$,
which is just the same as a linear convolutional filter with window size $d$.\footnote{This variant of \SoftP has $d$ bias parameters, which correspond to only a single bias parameter in a CNN.
The redundant biases may affect optimization but are an otherwise unimportant difference.} A single pattern's score for a document is:
\begin{equation}
\label{eqn:cnn-doc}
  s_\text{doc}(\seq{x}) = \max_{1 \leq i \leq n-d+1}{s_\text{span}(\seq{x}_{i:i+d})}.
\end{equation}
Using $k$ patterns with $d+1$ states corresponds to a CNN with window size $d$ and output dimension $k$.
The $\max{}$ in Equation~\ref{eqn:cnn-doc} is calculated for each pattern independently, corresponding to element-wise max-pooling of the CNN's output layer.
Based on the equivalence between this impoverished version of \SoftP and CNNs,
we conclude that one-layer CNNs are learning an even more restricted class of
WFSAs (linear-chain WFSAs) that capture only fixed-length \softps.

%%% Summarize the differences between CNNs and \SoftP.
% One implementation difference between \SoftP and CNNs is that \SoftP allows for the pre-specification of different pattern length size, while CNNs are typically used with only a single window size.\footnote{Although variants of CNN with multiple window sizes exist \cite{yin_multichannel_2015}.}
One notable difference between \SoftP and CNNs is that CNNs can use any subdifferentiable nonlinear filter (like an MLP over $\tensor{v}_{x_{i:i+d}}$, for example).
In contrast, in order to efficiently pool over flexible-length spans, \SoftP is restricted to operations that follow the semiring laws.\footnote{The max-sum semiring corresponds to a linear filter. Other semirings could potentially model more interesting interactions, but we leave this to future work.}
% calculate a value for each token in the span independently and combines values via the semiring times operator.
% So \SoftP can only simulate a CNN that has a single linear layer before document-level pooling.
% This restriction applies only when composing token representations into document representations;
% \SoftP may run an arbitrary model beforehand to get token representations, and afterward to turn document representations into final predictions.
% Note that while it is possible to run a biLSTM, for example, to get input representations to \SoftP, it would add interactions that are not accounted for in our interpretability analyses.
\resolved{\smtb{We could also cite Lingpeng Kong's Segmental RNNs \cite{Kong:2016} as related.
SegRNNs can calculate expressive span representations, but have $O(n^2)$ runtime.}}

As a model that is more flexible than a one-layer CNN, but less expressive than
an RNN, \SoftP lies somewhere on the continuum between these two approaches.
Continuing to study the bridge between CNNs and RNNs is an exciting direction for future research.


