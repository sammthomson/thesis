\chapter{Conclusion}

In this thesis we explored the dual goals of decoding and encoding
linguistic graphs, with experiments and motivating applications in
natural language processing, especially semantic analysis.
These two goals roughly map to the two questions:
``How do you automatically produce a graph from text?'',
and ``How do you use such a graph, once you have it?''.

In Chapters~\ref{chap:arc-factored-sdp} through \ref{chap:steiner},
we introduced methods for \emph{decoding} linguistic graphs---%
given natural language text, how to produce a graph representing
some aspect of its linguistic structure.
In Chapter~\ref{chap:arc-factored-sdp}, we experimented with
linguistically motivated features and constraints in a linear
model for semantic dependency parsing, achieving strong results
in a shared task.
In Chapter~\ref{chap:meurbo}, we built on the winning model of
that shared task, updating it to use deep neural networks.
We further showed how all three semantic dependency parsing formalisms
could be modeled and predicted jointly in a multitask learning
setup, leading to state-of-the-art performance.
In Chapter~\ref{chap:steiner}, we showed that jointly predicting the
nodes and edges of \emph{connected} graphs is an NP-hard problem, relating
it to the prize-collecting Steiner tree problem.
We introduced an extension of \PCST, \NEWCS, which is more suitable for
graph prediction.
We extended techniques for \PCST\ solving to \NEWCS\ solving, and showed
how these techniques could be used for semantic parsing and other related
NLP tasks.

In Chapter~\ref{chap:kgen}, we introduced new models for \emph{encoding}
sequences and graphs.
We first introduced a new, highly interpretable model for text encoding
called \SoftP.
\SoftP is made up of many small FSAs with neural weights, and we show
experimentally that despite its limited form, \SoftP is a capable model
for encoding sequences.
Then we extended \SoftP to encode graphs (Section~\ref{sec:kgen:kgen}),
making use of the fact
that inference in WFSAs follows the semiring laws, and in some cases, the
star semiring laws.
This allowed us to encode entire graphs using dynamic programming.
We tested our new model, the Kleene Graph Encoder Network (KGEN),
by encoding a syntactic graph and predicting semantic dependencies.
We compared to a strong Graph Convolutional Network baseline, and showed
that KGEN resulted in a bigger performance gains on the SDP task.

One question left unexplored in this thesis is: when predicting a graph $G$, how should one predict $G$'s node set?
In our experiments we only predict arcs, given a fixed set of nodes.
In fact, we show in Chapter~\ref{chap:steiner} that jointly modeling nodes and arcs is a hard problem, but we do give an approximate decoding algorithm for it (\BeasGraph,~\S\ref{algo:steiner:beasgraph}).
In future work, we hope to use this \BeasGraph, along with a KGEN for encoding syntax, in a NeurboParser for the abstract meaning representation.
This would be a robust demonstration of the power of the graph-to-graph pipeline described in Chapter~\ref{chap:intro}.

While we focused in this thesis on graphs representing syntactic and semantic
aspects of sentences, graph-based networks
have applications across
NLP~\interalia{yasunaga_graph-based_2017,cetoli_graph_2017,bastings_graph_2017},
and beyond~\interalia{zhou_graph_2017,defferrard_convolutional_2016,kipf_semi-supervised_2016,zellers_neural_2018}.



